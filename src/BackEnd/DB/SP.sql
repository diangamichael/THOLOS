/*
 * @Description: Defines the procedures for Tholos Survey applications
 * @Author: veshu
 * @Version: 1.0
 * @Copyright: Copyright (C) 2015, TopCoder, Inc. All rights reserved.
 */

IF OBJECT_ID('spCheckParticipantUsernamePassword', 'P') IS NOT NULL DROP PROC spCheckParticipantUsernamePassword
GO
IF OBJECT_ID('spCheckUserUsernamePassword', 'P') IS NOT NULL DROP PROC spCheckUserUsernamePassword
GO
IF OBJECT_ID('spGetUserByUsername', 'P') IS NOT NULL DROP PROC spGetUserByUsername
GO
IF OBJECT_ID('spCreateProfileAnswer', 'P') IS NOT NULL DROP PROC spCreateProfileAnswer
GO 
IF OBJECT_ID('spGetParticipantRoles', 'P') IS NOT NULL DROP PROC spGetParticipantRoles
GO 
IF OBJECT_ID('spGetUserRoles', 'P') IS NOT NULL DROP PROC spGetUserRoles
GO
IF OBJECT_ID('spGetUserRoleNames', 'P') IS NOT NULL DROP PROC spGetUserRoleNames
GO
-- survey services
IF OBJECT_ID('spGetPrototypeTestsByParticipantSurveyId', 'P') IS NOT NULL DROP PROC spGetPrototypeTestsByParticipantSurveyId
GO
IF OBJECT_ID('spGetParticipantSurveysBySurveyId', 'P') IS NOT NULL DROP PROC spGetParticipantSurveysBySurveyId
GO
IF OBJECT_ID('spGetParticipantCredentials', 'P') IS NOT NULL DROP PROC spGetParticipantCredentials
GO
IF OBJECT_ID('spCreateQuickAccessSurvey', 'P') IS NOT NULL DROP PROC spCreateQuickAccessSurvey
GO
IF OBJECT_ID('spDeleteQuickAccessSurveysByUserId', 'P') IS NOT NULL DROP PROC spDeleteQuickAccessSurveysByUserId
GO
IF OBJECT_ID('spGetQuickAccessSurveysByUserId', 'P') IS NOT NULL DROP PROC spGetQuickAccessSurveysByUserId
GO
IF OBJECT_ID('spGetUserSurveyStatus', 'P') IS NOT NULL DROP PROC spGetUserSurveyStatus
GO
IF OBJECT_ID('spGetAllSurveys', 'P') IS NOT NULL DROP PROC spGetAllSurveys
GO
IF OBJECT_ID('spSearchSurveys', 'P') IS NOT NULL DROP PROC spSearchSurveys
GO
IF OBJECT_ID('spGetPrototypeCodesBySurveyId', 'P') IS NOT NULL DROP PROC spGetPrototypeCodesBySurveyId
GO
IF OBJECT_ID('spGetSurveyPostTestingQuestionsBySurveyId', 'P') IS NOT NULL DROP PROC spGetSurveyPostTestingQuestionsBySurveyId
GO
IF OBJECT_ID('spGetSurveyPrototypeQuestionsBySurveyId', 'P') IS NOT NULL DROP PROC spGetSurveyPrototypeQuestionsBySurveyId
GO
IF OBJECT_ID('spGetSurvey', 'P') IS NOT NULL DROP PROC spGetSurvey
GO
IF OBJECT_ID('spDeletePrototypeCodesBySurveyId', 'P') IS NOT NULL DROP PROC spDeletePrototypeCodesBySurveyId
GO
IF OBJECT_ID('spDeleteSurveyPostTestingQuestionsBySurveyId', 'P') IS NOT NULL DROP PROC spDeleteSurveyPostTestingQuestionsBySurveyId
GO
IF OBJECT_ID('spDeleteSurveyPrototypeQuestionsBySurveyId', 'P') IS NOT NULL DROP PROC spDeleteSurveyPrototypeQuestionsBySurveyId
GO
IF OBJECT_ID('spUpdateSurvey', 'P') IS NOT NULL DROP PROC spUpdateSurvey
GO
IF OBJECT_ID('spUpdateSurveyStatus', 'P') IS NOT NULL DROP PROC spUpdateSurveyStatus
GO
IF OBJECT_ID('spCreateParticipantCredentials', 'P') IS NOT NULL DROP PROC spCreateParticipantCredentials
GO
IF OBJECT_ID('spCreatePrototypeCode', 'P') IS NOT NULL DROP PROC spCreatePrototypeCode
GO
IF OBJECT_ID('spCreateSurveyPostTestingQuestion', 'P') IS NOT NULL DROP PROC spCreateSurveyPostTestingQuestion
GO
IF OBJECT_ID('spCreateSurveyPrototypeQuestion', 'P') IS NOT NULL DROP PROC spCreateSurveyPrototypeQuestion
GO
IF OBJECT_ID('spCreateSurvey', 'P') IS NOT NULL DROP PROC spCreateSurvey
GO
IF OBJECT_ID('spGetAllPrototypeQuestions', 'P') IS NOT NULL DROP PROC spGetAllPrototypeQuestions
GO  
IF OBJECT_ID('spGetPrototypeAnswerOptionsByQuestionId', 'P') IS NOT NULL DROP PROC spGetPrototypeAnswerOptionsByQuestionId
GO 
IF OBJECT_ID('spGetAllPostTestingQuestions', 'P') IS NOT NULL DROP PROC spGetAllPostTestingQuestions
GO 
IF OBJECT_ID('spGetPostTestingAnswerOptionsByQuestionId', 'P') IS NOT NULL DROP PROC spGetPostTestingAnswerOptionsByQuestionId
GO 
-- participant services
IF OBJECT_ID('spGetProfileAnswerOptionsByQuestionId', 'P') IS NOT NULL DROP PROC spGetProfileAnswerOptionsByQuestionId
GO
IF OBJECT_ID('spGetAllProfileQuestions', 'P') IS NOT NULL DROP PROC spGetAllProfileQuestions
GO 
IF OBJECT_ID('spGetParticipantIdByUsername', 'P') IS NOT NULL DROP PROC spGetParticipantIdByUsername
GO
IF OBJECT_ID('spUpdateParticipantCredentialsAsUsedById', 'P') IS NOT NULL DROP PROC spUpdateParticipantCredentialsAsUsedById
GO
IF OBJECT_ID('spGetRoleIdByName', 'P') IS NOT NULL DROP PROC spGetRoleIdByName
GO
IF OBJECT_ID('spCreateParticipant', 'P') IS NOT NULL DROP PROC spCreateParticipant
GO 
IF OBJECT_ID('spCreateParticipantRole', 'P') IS NOT NULL DROP PROC spCreateParticipantRole
GO 
IF OBJECT_ID('spGetParticipantCredentialsByUsername', 'P') IS NOT NULL DROP PROC spGetParticipantCredentialsByUsername
GO 
IF OBJECT_ID('spGetParticipant', 'P') IS NOT NULL DROP PROC spGetParticipant
GO 
IF OBJECT_ID('spGetProfileAnswersByParticipantId', 'P') IS NOT NULL DROP PROC spGetProfileAnswersByParticipantId
GO
--- participant survey services
IF OBJECT_ID('spGetSurveyPostTestingQuestionsByParticipantSurveyId', 'P') IS NOT NULL DROP PROC spGetSurveyPostTestingQuestionsByParticipantSurveyId
GO
IF OBJECT_ID('spGetSurveyPrototypeQuestionsByParticipantSurveyId', 'P') IS NOT NULL DROP PROC spGetSurveyPrototypeQuestionsByParticipantSurveyId
GO
IF OBJECT_ID('spGetSurveyPrototypeQuestionsForParticipantSurveyId', 'P') IS NOT NULL DROP PROC spGetSurveyPrototypeQuestionsForParticipantSurveyId
GO
IF OBJECT_ID('spIncreaseCompetedSurveysNumber', 'P') IS NOT NULL DROP PROC spIncreaseCompetedSurveysNumber
GO
IF OBJECT_ID('spCreatePostTestingAnswer', 'P') IS NOT NULL DROP PROC spCreatePostTestingAnswer
GO
IF OBJECT_ID('spUpdatePrototypeTestWithCompleteData', 'P') IS NOT NULL DROP PROC spUpdatePrototypeTestWithCompleteData
GO
IF OBJECT_ID('spGetParticipantSurveyByPrototypeTestId', 'P') IS NOT NULL DROP PROC spGetParticipantSurveyByPrototypeTestId
GO
IF OBJECT_ID('spUpdatePrototypeAnswer', 'P') IS NOT NULL DROP PROC spUpdatePrototypeAnswer
GO
IF OBJECT_ID('spCreatePrototypeAnswer', 'P') IS NOT NULL DROP PROC spCreatePrototypeAnswer
GO
IF OBJECT_ID('spGetPrototypeAnswersByPrototypeTestId', 'P') IS NOT NULL DROP PROC spGetPrototypeAnswersByPrototypeTestId
GO
IF OBJECT_ID('spGetPrototypeTest', 'P') IS NOT NULL DROP PROC spGetPrototypeTest
GO
IF OBJECT_ID('spUpdateCurrentPrototypeTestId', 'P') IS NOT NULL DROP PROC spUpdateCurrentPrototypeTestId
GO
IF OBJECT_ID('spCreatePrototypeTest', 'P') IS NOT NULL DROP PROC spCreatePrototypeTest
GO
IF OBJECT_ID('spGetPostTestingAnswersByParticipantSurveyId', 'P') IS NOT NULL DROP PROC spGetPostTestingAnswersByParticipantSurveyId
GO
IF OBJECT_ID('spGetPrototypeTestIdsByParticipantSurveyId', 'P') IS NOT NULL DROP PROC spGetPrototypeTestIdsByParticipantSurveyId
GO
IF OBJECT_ID('spGetParticipantSurvey', 'P') IS NOT NULL DROP PROC spGetParticipantSurvey
GO

/**************************************************************************
** Stored Procedure: spGetSurveyPostTestingQuestionsByParticipantSurveyId
**
** In:
**    @ParticipantSurveyId	-	 The Participant Survey Id
**
** Description: 
**   Gets survey PostTesting questions by Participant Survey id
**************************************************************************/
CREATE PROCEDURE spGetSurveyPostTestingQuestionsByParticipantSurveyId
	 @ParticipantSurveyId	BIGINT      
AS  
	SELECT pq.[Id], [Text], [Conditional], [QuestionType], QuestionNumber, pqc.Id as [ConditionId], 
	ConditionalOnQuestionId, ConditionalOnAnswerId
	FROM [dbo].[PostTestingQuestion] pq
	LEFT JOIN [dbo].[PostTestingQuestionCondition] pqc ON pq.Id=pqc.QuestionId
	INNER JOIN [dbo].[SurveyPostTestingQuestion] spq ON spq.QuestionId=pq.Id
	INNER JOIN [dbo].[ParticipantSurvey] ps ON ps.SurveyId=spq.SurveyId
	WHERE ps.[Id]=@ParticipantSurveyId;
GO

/**************************************************************************
** Stored Procedure: spGetSurveyPrototypeQuestionsByParticipantSurveyId
**
** In:
**    @ParticipantSurveyId	-	 The Participant Survey Id
**
** Description: 
**   Gets survey prototype questions by Participant Survey id
**************************************************************************/
CREATE PROCEDURE spGetSurveyPrototypeQuestionsByParticipantSurveyId
	 @ParticipantSurveyId	BIGINT      
AS  
	SELECT pq.[Id], [Text], [Conditional], [QuestionType], QuestionNumber, pqc.Id as [ConditionId], 
	ConditionalOnQuestionId, ConditionalOnAnswerId
	FROM [dbo].[PrototypeQuestion] pq
	LEFT JOIN [dbo].[PrototypeQuestionCondition] pqc ON pq.Id=pqc.QuestionId
	INNER JOIN [dbo].[SurveyProrotypeQuestion] spq ON spq.QuestionId=pq.Id
	INNER JOIN [dbo].[ParticipantSurvey] ps ON ps.SurveyId=spq.SurveyId
	WHERE ps.[Id]=@ParticipantSurveyId;
GO

/**************************************************************************
 ** Stored Procedure: spIncreaseCompetedSurveysNumber
 ** In:
 **    @ParticipantSurveyId - the Participant Survey Id .
 ** Out:
 **    @Id - The Id
 ** Description:
 **    Increases the completed surveys number.
 **************************************************************************/
CREATE PROC [dbo].[spIncreaseCompetedSurveysNumber]
	@ParticipantSurveyId BIGINT
AS
	UPDATE s
	SET CompletedSurveysNumber= CompletedSurveysNumber+1
	FROM [dbo].[Survey] s
	INNER JOIN [dbo].[ParticipantSurvey] ps ON ps.SurveyId=s.Id
	WHERE ps.Id=@ParticipantSurveyId;
GO
/**************************************************************************
 ** Stored Procedure: spCreatePostTestingAnswer
 ** In:
 **    @ParticipantSurveyId - the Participant Survey Id .
 **    @AnswerType - the answer type.
 **    @QuestionId - the question id.
 **    @AnswerOptionId - the answer option id.
 **    @Input - the input.
 **    @Value - the value.
 ** Out:
 **    @Id - The Id
 ** Description:
 **    Inserts a new record into the PostTestingAnswer table and child tables depending on the @AnswerType.
 **************************************************************************/
CREATE PROC [dbo].spCreatePostTestingAnswer
@Id	BIGINT	OUTPUT,
@ParticipantSurveyId BIGINT,
@AnswerType VARCHAR(32),
@QuestionId BIGINT,
@AnswerOptionId BIGINT=null,
@Input VARCHAR(128)=null,
@Value INT=null
AS
	INSERT INTO [dbo].[PostTestingAnswer](QuestionId, AnswerType, ParticipantSurveyId)
	VALUES(@QuestionId,@AnswerType,@ParticipantSurveyId);

	SET @Id=SCOPE_IDENTITY();

	IF (@AnswerType='RangeAnswer')
	BEGIN
		INSERT INTO [dbo].[PostTestingRangeAnswer](Id, Value)
		VALUES(@Id,@value);
	END
	ELSE IF (@AnswerType='SingleAnswerInput' OR @AnswerType='SingleAnswer')
	BEGIN
			INSERT INTO [dbo].[PostTestingSingleAnswer](Id, AnswerOptionId)
			VALUES(@Id,@AnswerOptionId);

		IF (@AnswerType='SingleAnswerInput')
		BEGIN
			INSERT INTO [dbo].[PostTestingSingleInputAnswer](Id, Input)
			VALUES(@Id, @Input);
		END
	END;
GO

/**************************************************************************
** Stored Procedure: spUpdatePrototypeTestWithCompleteData
**
** In:
**   @Id - The Id
**	 @AfterWeight - The after weight 
**	 @AfterPhotoLocalPath - The  after photo local path
**	 @AfterPhotoUri - the after photo uri
** 
** Description: 
**   Inserts into prototype test table
**************************************************************************/
CREATE PROCEDURE spUpdatePrototypeTestWithCompleteData
	@Id					BIGINT,
	@AfterWeight			REAL,
	@AfterPhotoLocalPath	VARCHAR(1024),
	@AfterPhotoUri VARCHAR(1024)
AS
	UPDATE [dbo].[PrototypeTest] 
	SET [AfterWeight] = @AfterWeight,
		[AfterPhotoLocalPath] = @AfterPhotoLocalPath,
		[AfterPhotoUri]=@AfterPhotoUri,
		[Completed]=1
	WHERE [Id] = @Id;
	UPDATE [dbo].[ParticipantSurvey]
	SET CurrentPrototypeTestId = 0
	WHERE CurrentPrototypeTestId = @Id;
GO
/**************************************************************************
** Stored Procedure: spGetParticipantSurveyByPrototypeTestId
**
** In:
**	  @PrototypeTestId - The PrototypeTest Id
**
** Description: 
**   Gets the participant survey fields by PrototypeTest Id.
**************************************************************************/
CREATE PROCEDURE spGetParticipantSurveyByPrototypeTestId
	@PrototypeTestId	 BIGINT	
AS  
	SELECT ps.[Id]
      ,[ParticipantId]
      ,[SurveyId]
      ,[CurrentPrototypeTestId]
	FROM [dbo].[ParticipantSurvey] ps
	INNER JOIN [dbo].[PrototypeTest] pt ON ps.Id=pt.ParticipantSurveyId
	WHERE pt.Id=@PrototypeTestId;
GO
/**************************************************************************
 ** Stored Procedure: spUpdatePrototypeAnswer
 ** In:
 **    @AnswerType - the answer type.
 **    @AnswerOptionId - the answer option id.
 **    @Input - the input.
 **    @Value - the value.
 ** Out:
 **    @Id - The Id
 ** Description:
 **    Updates the PrototypeAnswer table and child tables depending on the @AnswerType.
 **************************************************************************/
CREATE PROC [dbo].spUpdatePrototypeAnswer
@Id	BIGINT,
@AnswerType VARCHAR(32),
@AnswerOptionId BIGINT=null,
@Input VARCHAR(128)=null,
@Value INT=null
AS
	IF (@AnswerType='RangeAnswer')
	BEGIN
		UPDATE [dbo].[PrototypeRangeAnswer]
		SET Value=@value
		WHERE ID=@Id;
	END
	ELSE IF (@AnswerType='SingleAnswerInput' OR @AnswerType='SingleAnswer')
	BEGIN
			UPDATE [dbo].[PrototypeSingleAnswer]
			SET AnswerOptionId=@AnswerOptionId
			WHERE ID=@Id;
		IF (@AnswerType='SingleAnswerInput')
		BEGIN
			UPDATE [dbo].[PrototypeSingleInputAnswer]
			SET Input=@Input
			WHERE ID=@Id;
		END
	END;
GO

/**************************************************************************
 ** Stored Procedure: spCreatePrototypeAnswer
 ** In:
 **    @PrototypeTestId - the profile answer id.
 **    @AnswerType - the answer type.
 **    @QuestionId - the question id.
 **    @AnswerOptionId - the answer option id.
 **    @Input - the input.
 **    @Value - the value.
 ** Out:
 **    @Id - The Id
 ** Description:
 **    Inserts a new record into the PrototypeAnswer table and child tables depending on the @AnswerType.
 **************************************************************************/
CREATE PROC [dbo].spCreatePrototypeAnswer
@Id	BIGINT	OUTPUT,
@PrototypeTestId BIGINT,
@AnswerType VARCHAR(32),
@QuestionId BIGINT,
@AnswerOptionId BIGINT=null,
@Input VARCHAR(128)=null,
@Value INT=null
AS
	INSERT INTO [dbo].[PrototypeAnswer](QuestionId, AnswerType, PrototypeTestId)
	VALUES(@QuestionId,@AnswerType,@PrototypeTestId);

	SET @Id=SCOPE_IDENTITY();

	IF (@AnswerType='RangeAnswer')
	BEGIN
		INSERT INTO [dbo].[PrototypeRangeAnswer](Id, Value)
		VALUES(@Id,@value);
	END
	ELSE IF (@AnswerType='SingleAnswerInput' OR @AnswerType='SingleAnswer')
	BEGIN
			INSERT INTO [dbo].[PrototypeSingleAnswer](Id, AnswerOptionId)
			VALUES(@Id,@AnswerOptionId);

		IF (@AnswerType='SingleAnswerInput')
		BEGIN
			INSERT INTO [dbo].[PrototypeSingleInputAnswer](Id, Input)
			VALUES(@Id, @Input);
		END
	END;
GO

/**************************************************************************
** Stored Procedure: spGetPrototypeAnswersByPrototypeTestId
**
** In:
**	 @PrototypeTestId - The Prototype Test Id
**
** Description: 
**   Gets prototype answers by prototype test id.
**************************************************************************/
CREATE PROCEDURE spGetPrototypeAnswersByPrototypeTestId
	@PrototypeTestId BIGINT
AS  
	SELECT pa.Id, QuestionId, AnswerType, pra.Value, psa.AnswerOptionId, psia.Input
	FROM [dbo].[PrototypeAnswer] pa
	LEFT JOIN [dbo].[PrototypeRangeAnswer] pra ON pa.Id = pra.Id
	LEFT JOIN [dbo].[PrototypeSingleAnswer] psa ON pa.Id= psa.Id
	LEFT JOIN [dbo].[PrototypeSingleInputAnswer] psia ON psia.Id = pa.Id
	WHERE PrototypeTestId=@PrototypeTestId; 
GO

/**************************************************************************
** Stored Procedure: spGetPrototypeTest
**
** In:
**	 @PrototypeTestId - The Prototype Test Id
**
** Description: 
**   Gets prototype test fields
**************************************************************************/
CREATE PROCEDURE spGetPrototypeTest
	@PrototypeTestId BIGINT
AS  
	SELECT [Id]
		  ,[PrototypeCode]
		  ,[Iteration]
		  ,[BeforeWeight]
		  ,[AfterWeight]
		  ,[BeforePhotoUri]
		  ,[AfterPhotoUri]
		   , BeforePhotoLocalPath
          , AfterPhotoLocalPath
          , Completed
	   FROM  [dbo].[PrototypeTest]
       WHERE [Id] = @PrototypeTestId
GO
/**************************************************************************
** Stored Procedure: spUpdateCurrentPrototypeTestId
**
** In:
**	 @ParticipantSurveyId - The Participant Survey Id
**	 @PrototypeTestId - The Prototype Test Id
**
** Description: 
**   Inserts into prototype test table
**************************************************************************/
CREATE PROCEDURE spUpdateCurrentPrototypeTestId
	@ParticipantSurveyId BIGINT ,
	@PrototypeTestId BIGINT
AS  
	UPDATE [dbo].[ParticipantSurvey]
	SET [CurrentPrototypeTestId] = @PrototypeTestId
	WHERE [Id] = @ParticipantSurveyId;
GO

/**************************************************************************
** Stored Procedure: spCreatePrototypeTest
**
** In:
**   @PrototypeCode - The Prototype Code
**	 @ParticipantSurveyId - The Participant Survey Id
**   @Iteration - the iteration
**	 @BeforeWeight - The Before Weight
**	 @BeforePhotoLocalPath - The  Before photo local path
**	 @BeforePhotoUri - the before photo uri
** Out:
**    @Id - The Id
**
** Description: 
**   Inserts into prototype test table
**************************************************************************/
CREATE PROCEDURE spCreatePrototypeTest
	@PrototypeCode  VARCHAR(64),
	@ParticipantSurveyId	 BIGINT	,
	@Iteration INT,
	@BeforePhotoLocalPath VARCHAR(1024),
	@BeforePhotoUri VARCHAR(1024),
	@Id	BIGINT OUTPUT
AS  
	DECLARE @Weight INT
	SELECT @Weight = [Weight] FROM [dbo].[Participant] p INNER JOIN [dbo].[ParticipantSurvey] s ON p.Id = s.ParticipantId WHERE s.Id = @ParticipantSurveyId;
	INSERT INTO [dbo].[PrototypeTest]([ParticipantSurveyId], [PrototypeCode],[Iteration], [BeforeWeight], [BeforePhotoLocalPath],[BeforePhotoUri])
    VALUES(@ParticipantSurveyId, @PrototypeCode,@Iteration, @Weight, @BeforePhotoLocalPath,@BeforePhotoUri);
	SET @Id = SCOPE_IDENTITY();
	UPDATE [dbo].[ParticipantSurvey] SET [CurrentPrototypeTestId] = @Id WHERE Id = @ParticipantSurveyId;
GO

/**************************************************************************
** Stored Procedure: spGetPostTestingAnswersByParticipantSurveyId
**
** In:
**	 @ParticipantSurveyId - The Participant Survey Id
**
** Description: 
**   Gets the id's from prototype test table
**************************************************************************/
CREATE PROCEDURE spGetPostTestingAnswersByParticipantSurveyId
	@ParticipantSurveyId	 BIGINT	
AS  
  SELECT pra.Id, QuestionId, AnswerType, pra.Value, psa.AnswerOptionId, psia.Input
	FROM [dbo].[PostTestingAnswer] pa
	LEFT JOIN [dbo].[PostTestingRangeAnswer] pra ON pa.Id = pra.Id
	LEFT JOIN [dbo].[PostTestingSingleAnswer] psa ON pa.Id= psa.Id
	LEFT JOIN [dbo].[PostTestingSingleInputAnswer] psia ON psia.Id = pa.Id
	WHERE ParticipantSurveyId=@ParticipantSurveyId; 
GO
/**************************************************************************
** Stored Procedure: spGetPrototypeTestIdsByParticipantSurveyId
**
** In:
**	  @ParticipantSurveyId - The participant survey Id
**
** Description: 
**   Gets the participant survey fields
**************************************************************************/
CREATE PROCEDURE spGetPrototypeTestIdsByParticipantSurveyId
	@ParticipantSurveyId	 BIGINT	
AS  
	SELECT [Id]
	FROM [dbo].[PrototypeTest]
	WHERE [ParticipantSurveyId]=@ParticipantSurveyId;
GO

/**************************************************************************
** Stored Procedure: spGetParticipantSurvey
**
** In:
**	  @ParticipantSurveyId - The participant survey Id
**
** Description: 
**   Gets the participant survey fields
**************************************************************************/
CREATE PROCEDURE spGetParticipantSurvey
	@ParticipantSurveyId	 BIGINT	
AS  
	SELECT [Id]
      ,[ParticipantId]
      ,[SurveyId]
      ,[CurrentPrototypeTestId]
	FROM [dbo].[ParticipantSurvey]
	WHERE  [Id]=@ParticipantSurveyId;
GO

/**************************************************************************
** Stored Procedure: spGetPrototypeTestsByParticipantSurveyId
**
** In:
**	 @ParticipantSurveyId - The participant survey Id
**
** Description: 
**   Gets the participant survey fields
**************************************************************************/
CREATE PROCEDURE spGetPrototypeTestsByParticipantSurveyId
	@ParticipantSurveyId	 BIGINT	
AS  
	SELECT [Id]
		  ,[PrototypeCode]
		  ,[Iteration]
		  ,[BeforeWeight]
		  ,[AfterWeight]
		  ,[BeforePhotoUri]
		  ,[AfterPhotoUri]
		  , BeforePhotoLocalPath
          , AfterPhotoLocalPath
          , Completed
	  FROM [dbo].[PrototypeTest]
	  WHERE  [ParticipantSurveyId]=@ParticipantSurveyId
GO

/**************************************************************************
** Stored Procedure: spGetParticipantSurveysBySurveyId
**
** In:
**	  @SurveyId - The Survey Id
**
** Description: 
**   Gets the participant survey fields
**************************************************************************/
CREATE PROCEDURE spGetParticipantSurveysBySurveyId
	@SurveyId	 BIGINT	
AS  
	SELECT [Id]
      ,[ParticipantId]
      ,[SurveyId]
      ,[CurrentPrototypeTestId]
	FROM [dbo].[ParticipantSurvey]
	WHERE  [SurveyId]=@SurveyId;
GO
/**************************************************************************
** Stored Procedure: spGetParticipantCredentials
**
** In:
**	  @SurveyId - The Survey Id
**
** Description: 
**   Gets the participant credentails for a survey.
**************************************************************************/
CREATE PROCEDURE spGetParticipantCredentials
	@SurveyId	 BIGINT
AS  
	SELECT [Id],
		 [SurveyId],
		 [Username],
		 [Password],
		 [Used]
	FROM [dbo].[ParticipantCredentials]
	WHERE [SurveyId]=@SurveyId;
GO
/**************************************************************************
** Stored Procedure: spCreateQuickAccessSurvey
**
** In:
**	  @SurveyId - The Survey Id
**    @UserId	- The  User Id
**	  @Position - The Position
**
** Description: 
**   Inserts quick access survey data
**************************************************************************/
CREATE PROCEDURE spCreateQuickAccessSurvey
	@SurveyId	 BIGINT,
	@UserId		 BIGINT,
	@Position	 INT,
	@Id			 BIGINT OUTPUT
AS  
	
	INSERT INTO [dbo].[QuickAccessSurvey]([UserId], [SurveyId], [Position])
    VALUES(@UserId, @SurveyId, @Position)
	SET @Id=SCOPE_IDENTITY();
GO

/**************************************************************************
** Stored Procedure: spDeleteQuickAccessSurveysByUserId
**
** In:
**    @UserId	-	 The  User Id
**
** Description: 
**   Removes quick access survey fields for given user id
**************************************************************************/
CREATE PROCEDURE spDeleteQuickAccessSurveysByUserId
	  @UserId		BIGINT  
AS  
	DELETE FROM [dbo].[QuickAccessSurvey]
    WHERE [UserId]=@UserId;
GO

/**************************************************************************
** Stored Procedure: spGetQuickAccessSurveysByUserId
**
** In:
**    @UserId	-	 The  User Id
**
** Description: 
**   Gets quick access survey fields and survery fields by user id
**************************************************************************/
CREATE PROCEDURE spGetQuickAccessSurveysByUserId
@UserId	BIGINT  
AS  
	SELECT qas.[SurveyId],s.[Name],s.[CellName], qas.[Position]      
	FROM [dbo].[QuickAccessSurvey] qas 
		INNER JOIN [dbo].[Survey] s ON qas.[SurveyId]=s.[Id]
	WHERE [UserId]= @UserId;
GO

/**************************************************************************
** Stored Procedure: spGetUserSurveyStatus
**
** In:
**    @UserId	-	 The  User Id
**
** Description: 
**   Gets survey status
**************************************************************************/
CREATE PROCEDURE spGetUserSurveyStatus
	  @UserId		BIGINT  
AS  
	SELECT [Status]      
	FROM [dbo].[Survey] 	
	WHERE [CreatedBy]= @UserId 
GO

/**************************************************************************
** Stored Procedure: spGetPrototypeCodesBySurveyId
**
** In:
**    @SurveyId	-	 The  Survey Id
**    @Status	-	 The expected survey status
**
** Description: 
**   Gets prototype codes by survey id
**************************************************************************/
CREATE PROCEDURE spGetPrototypeCodesBySurveyId
	  @SurveyId		 BIGINT,
	  @Status		 VARCHAR(32) = NULL
AS  	 	
     SELECT pc.[Id], pc.[Code], pc.[PrototypesPerCode]
	 FROM [dbo].[PrototypeCode] pc JOIN [dbo].[Survey] s ON pc.SurveyId = s.Id
	 WHERE [SurveyId]=@SurveyId
	 AND (@Status IS NULL OR s.Status = @Status)
GO

/**************************************************************************
** Stored Procedure: spGetSurveyPostTestingQuestionsBySurveyId
**
** In:
**    @SurveyId	-	 The  Survey Id
**
** Description: 
**   Gets survey post testing questions by survey id
**************************************************************************/
CREATE PROCEDURE spGetSurveyPostTestingQuestionsBySurveyId
	  @SurveyId		 BIGINT      
AS  	 	
    SELECT ptq.[Id], [Text], [Conditional], [QuestionType], QuestionNumber, ptqc.Id as [ConditionId], 
		ConditionalOnQuestionId, ConditionalOnAnswerId
	FROM [dbo].[PostTestingQuestion] ptq
		LEFT JOIN [dbo].[PostTestingQuestionCondition] ptqc ON ptq.Id=ptqc.QuestionId
		INNER JOIN	 [dbo].[SurveyPostTestingQuestion] sptq	ON sptq.[QuestionId]= ptq.[Id]	
	WHERE sptq.[SurveyId]=@SurveyId
GO
/**************************************************************************
** Stored Procedure: spGetSurveyPrototypeQuestionsBySurveyId
**
** In:
**    @SurveyId	-	 The  Id
**
** Description: 
**   Gets survey prototype questions by survey id
**************************************************************************/
CREATE PROCEDURE spGetSurveyPrototypeQuestionsBySurveyId
	 @SurveyId	BIGINT      
AS  
	SELECT pq.[Id], [Text], [Conditional], [QuestionType], QuestionNumber, pqc.Id as [ConditionId], 
	ConditionalOnQuestionId, ConditionalOnAnswerId
	FROM [dbo].[PrototypeQuestion] pq
	LEFT JOIN [dbo].[PrototypeQuestionCondition] pqc ON pq.Id=pqc.QuestionId
	INNER JOIN [dbo].[SurveyProrotypeQuestion] spq ON spq.QuestionId=pq.Id
	WHERE spq.[SurveyId]=@SurveyId;
GO

/**************************************************************************
** Stored Procedure: spGetSurvey
**
** In:
**    @SurveyId	-	 The Id
**
** Description: 
**   Gets survey fields
**************************************************************************/
CREATE PROCEDURE spGetSurvey
	  @SurveyId		 BIGINT      
AS  	 	
    SELECT [Id] ,[Name], [CellName], [ParticipantsNumber], [Status], [DateCreated], [CompletedSurveysNumber]
		,[Draft], [CreatedBy]
	FROM [dbo].[Survey] 
	WHERE [Id] = @SurveyId	
GO

/**************************************************************************
** Stored Procedure: spDeletePrototypeCodesBySurveyId
**
** In:
**    @SurveyId	-	The  Survey Id
**
** Description: 
**   Deletes prototype codes by surveyd id
**************************************************************************/
CREATE PROCEDURE spDeletePrototypeCodesBySurveyId
	  @SurveyId		 BIGINT      

AS  	 	
     DELETE FROM [dbo].[PrototypeCode]  
			WHERE [SurveyId]= @SurveyId; 	
GO

/**************************************************************************
** Stored Procedure: spDeleteSurveyPostTestingQuestionsBySurveyId
**
** In:
**    @SurveyId	-	 The  Survey Id
**
** Description: 
**   Deletes survey post testing questions by surveyd id
**************************************************************************/
CREATE PROCEDURE spDeleteSurveyPostTestingQuestionsBySurveyId
	  @SurveyId		 BIGINT      

  AS  	 	
     DELETE FROM [dbo].[SurveyPostTestingQuestion]      
			WHERE [SurveyId]= @SurveyId  	
GO

/**************************************************************************
** Stored Procedure: spDeleteSurveyPrototypeQuestionsBySurveyId
**
** In:
**    @SurveyId	-	 The  Survey Id
**
** Description: 
**   Deletes survey prototype questions by survey id
**************************************************************************/
CREATE PROCEDURE spDeleteSurveyPrototypeQuestionsBySurveyId
	  @SurveyId		 BIGINT      
AS  	 	
    DELETE FROM [dbo].[SurveyProrotypeQuestion]      
	WHERE [SurveyId]= @SurveyId;	
GO
/**************************************************************************
** Stored Procedure: spUpdateSurvey
**
** In:
**    @Id		-				 The  Id
**    @Name		-				 The  Name
**    @CellName -				 The Cell Name
**    @Status	-				 The Status
**    @CompletedSurveysNumber -	 The Completed Survery Number
**    @Draft	 -				 The Draft
**
** Description: 
**   Updates the survey
**************************************************************************/
CREATE PROCEDURE spUpdateSurvey
	  @Id						 BIGINT,
      @Name						 VARCHAR(64),
      @CellName					 VARCHAR(64),
      @Status					 VARCHAR(32),
      @CompletedSurveysNumber	 INT,
      @Draft					 BIT
  AS  	  	
	UPDATE [dbo].[Survey]
		SET  [Name] = @Name
			,[CellName] = @CellName			
			,[Status] = @Status
			,[CompletedSurveysNumber] = @CompletedSurveysNumber
			,[Draft] = @Draft		
	WHERE [Id]= @Id  	
GO
/**************************************************************************
** Stored Procedure: spUpdateSurveyStatus
**
** In:
**    @Id		-		 The  Id
**    @Status	-		 The Status
**    @Draft	-		 The Draft
**
** Description: 
**   Updates survey status
**************************************************************************/
CREATE PROCEDURE spUpdateSurveyStatus
	  @Id						 BIGINT,
      @Status					 VARCHAR(32),
      @Draft					 BIT
AS  	  	     
	UPDATE [dbo].[Survey]
	SET [Status] = @Status,
		[Draft] = @Draft	
	WHERE [Id]= @Id  
  GO
/**************************************************************************
 ** Stored Procedure: spCreateParticipantCredentials
 **
 ** In:
 **   @SurveyId	 - The Survey Id
 **   @Username	- The User Name
 **   @Password - The Password
 ** Out:
 **    @Id - The Id
 **
 ** Description:
 **    Creates participant's credentials
 **************************************************************************/
CREATE PROCEDURE spCreateParticipantCredentials	
	@SurveyId BIGINT,
    @Username VARCHAR(64),
	@Password VARCHAR(64),	
   	@Id	BIGINT	OUTPUT
AS
	
	INSERT INTO [dbo].[ParticipantCredentials] ([SurveyId], [Username], [Password], [Used])          
    VALUES(@SurveyId, @Username, @Password, '0')

	SET @Id=SCOPE_IDENTITY();
GO
/**************************************************************************
 ** Stored Procedure: spCreatePrototypeCode
 **
 ** In:
 **    @Code	 - The Code
 **    @PrototypesPerCode	- Prototypes Per Code
 **    @SurveyId - The Survery Id
 ** Out:
 **    @Id - The Id
 **
 ** Description:
 **    Creates survey
 **************************************************************************/
CREATE PROCEDURE spCreatePrototypeCode	
	@Code						VARCHAR(64),
	@PrototypesPerCode			VARCHAR(64),   
    @SurveyId					BIGINT,
   	@Id							BIGINT	OUTPUT
AS
	
	INSERT INTO [dbo].[PrototypeCode] ([Code] ,[PrototypesPerCode],[SurveyId])
    VALUES (@Code, @PrototypesPerCode, @SurveyId);

	SET @Id=SCOPE_IDENTITY();
GO
/**************************************************************************
 ** Stored Procedure: spCreateSurveyPostTestingQuestion
 **
 ** In:
 **   @SurveyId	 - The Survey Id
 **   @QuestionId	- Question Id
 **   @QuestionNumber - Question Number
 ** Out:
 **    @Id - The Id
 **
 ** Description:
 **    Creates survey post testing question
 **************************************************************************/
CREATE PROCEDURE spCreateSurveyPostTestingQuestion	
	@SurveyId BIGINT,
    @QuestionId BIGINT,
	@QuestionNumber INT,
   	@Id	BIGINT	OUTPUT
AS
	
	INSERT INTO [dbo].[SurveyPostTestingQuestion]([SurveyId], [QuestionId], [QuestionNumber])
    VALUES ( @SurveyId, @QuestionId, @QuestionNumber)

	SET @Id=SCOPE_IDENTITY();
GO
/**************************************************************************
 ** Stored Procedure: spCreateSurveyPrototypeQuestion
 **
 ** In:
 **   @SurveyId	 - The Survey Id
 **   @QuestionId	- Question Id
 **   @QuestionNumber - The QuestionNumber
 ** Out:
 **    @Id - The Id
 **
 ** Description:
 **    Creates survey prototype question
 **************************************************************************/
CREATE PROCEDURE spCreateSurveyPrototypeQuestion	
	@SurveyId BIGINT,
    @QuestionId BIGINT,
    @QuestionNumber INT,
   	@Id	BIGINT	OUTPUT
AS
	
	INSERT INTO [dbo].[SurveyProrotypeQuestion]([SurveyId], [QuestionId], [QuestionNumber])
    VALUES (@SurveyId, @QuestionId, @QuestionNumber)

	SET @Id=SCOPE_IDENTITY();
GO
/**************************************************************************
 ** Stored Procedure: spCreateSurvey
 **
 ** In:
 **    @Name - The Name
 **    @CellName- The Cell Name
 **    @ParticipantsNumber - Participants Number
 **    @Status - The Status.
 **    @CompletedSurveysNumber - Completed Surveys Number
 **    @Draft - The Draft
 **    @CreatedBy - Created By
 ** Out:
 **    @Id - The Id
 **
 ** Description:
 **    Creates survey
 **************************************************************************/
CREATE PROCEDURE spCreateSurvey	
	@Name						VARCHAR(64),
	@CellName					VARCHAR(64),
    @ParticipantsNumber			INT,
    @Status						VARCHAR(32),
    @CompletedSurveysNumber		INT,
    @Draft						BIT,
    @CreatedBy					BIGINT,
	@Id							BIGINT	OUTPUT
AS
	
	INSERT INTO [dbo].[Survey] ([Name],[CellName],[ParticipantsNumber],[Status],[CompletedSurveysNumber],[Draft],[CreatedBy],DateCreated)
	VALUES(@Name, @CellName, @ParticipantsNumber, @Status, @CompletedSurveysNumber,@Draft, @CreatedBy,GETDATE());

	SET @Id=SCOPE_IDENTITY();
GO

/**************************************************************************
** Stored Procedure: spGetPrototypeAnswerOptionsByQuestionId
**
** Description: 
**   Gets all Prototype question answer options
**************************************************************************/
CREATE PROCEDURE spGetPrototypeAnswerOptionsByQuestionId 
	@QuestionId BIGINT
AS 
	SELECT pa.[Id], [QuestionId], [AnswerType], Value, Label, FromValueLabel, ToValueLabel, FromValue, ToValue, Increment
	FROM [dbo].[PrototypeAnswerOption] pa
	LEFT JOIN [dbo].[PrototypeRangeAnswerOption] pra ON pa.Id = pra.Id
	LEFT JOIN [dbo].[PrototypeSingleAnswerOption] psa ON pa.Id= psa.Id
	WHERE pa.[QuestionId]=@QuestionId; 
GO

/**************************************************************************
** Stored Procedure: spGetAllPrototypeQuestions
**
** Description: 
**   Gets all Prototype questions
**************************************************************************/
CREATE PROCEDURE spGetAllPrototypeQuestions
AS 
	SELECT pq.[Id], [Text], [Conditional], [QuestionType], pqc.Id as [ConditionId], 
	ConditionalOnQuestionId, ConditionalOnAnswerId
	FROM [dbo].[PrototypeQuestion] pq
	LEFT JOIN [dbo].[PrototypeQuestionCondition] pqc ON pq.Id=pqc.QuestionId;
GO

/**************************************************************************
** Stored Procedure: spGetPostTestingAnswerOptionsByQuestionId
**
** Description: 
**   Gets all PostTesting question answer options
**************************************************************************/
CREATE PROCEDURE spGetPostTestingAnswerOptionsByQuestionId 
	@QuestionId BIGINT
AS 
	SELECT pa.[Id], [QuestionId], [AnswerType], Value, Label, FromValueLabel, ToValueLabel, FromValue, ToValue, Increment
	FROM [dbo].[PostTestingAnswerOption] pa
	LEFT JOIN [dbo].[PostTestingRangeAnswerOption] pra ON pa.Id = pra.Id
	LEFT JOIN [dbo].[PostTestingSingleAnswerOption] psa ON pa.Id= psa.Id
	WHERE pa.[QuestionId]=@QuestionId; 
GO

/**************************************************************************
** Stored Procedure: spGetAllPostTestingQuestions
**
** Description: 
**   Gets all PostTesting questions
**************************************************************************/
CREATE PROCEDURE spGetAllPostTestingQuestions
AS 
	SELECT pq.[Id], [Text], [Conditional], [QuestionType], pqc.Id as [ConditionId], 
	ConditionalOnQuestionId, ConditionalOnAnswerId
	FROM [dbo].[PostTestingQuestion] pq
	LEFT JOIN [dbo].[PostTestingQuestionCondition] pqc ON pq.Id=pqc.QuestionId;
GO


/**************************************************************************
** Stored Procedure: spGetProfileAnswerOptionsByQuestionId
**
** Description: 
**   Gets all profile question answer options
**************************************************************************/
CREATE PROCEDURE spGetProfileAnswerOptionsByQuestionId 
	@QuestionId BIGINT
AS 
	SELECT pa.[Id], [QuestionId], [AnswerType], Value, Label, FromValueLabel, ToValueLabel, FromValue, ToValue, Increment
	FROM [dbo].[ProfileAnswerOption] pa
	LEFT JOIN [dbo].[ProfileRangeAnswerOption] pra ON pa.Id = pra.Id
	LEFT JOIN [dbo].[ProfileSingleAnswerOption] psa ON pa.Id= psa.Id
	WHERE pa.[QuestionId]=@QuestionId; 
GO

/**************************************************************************
** Stored Procedure: spGetAllProfileQuestions
**
** Description: 
**   Gets all profile questions
**************************************************************************/
CREATE PROCEDURE spGetAllProfileQuestions
AS 
	SELECT pq.[Id], [Text], [Conditional], [QuestionType], pqc.Id as [ConditionId], 
	ConditionalOnQuestionId, ConditionalOnAnswerId
	FROM [dbo].[ProfileQuestion] pq
	LEFT JOIN [dbo].[ProfileQuestionCondition] pqc ON pq.Id=pqc.QuestionId;

GO

/**************************************************************************
** Stored Procedure: spGetParticipantIdByUsername
**
** In:
**   @Username - The user name
**
** Description: 
**   Gets participant id by user name
**************************************************************************/
CREATE PROCEDURE spGetParticipantIdByUsername
@Username VARCHAR(64)
AS 
	SELECT [Id]
	FROM [dbo].[Participant]
	WHERE [Username]=@Username
GO

/**************************************************************************
** Stored Procedure: spUpdateParticipantCredentialsAsUsedById
**
** In:
**   @Username - The usern name
**
** Description: 
		Updates participant credential as used by id
**************************************************************************/
CREATE PROCEDURE spUpdateParticipantCredentialsAsUsedById
@Id BIGINT
AS 
	UPDATE [dbo].[ParticipantCredentials]
	SET  [Used] = 1
	WHERE [Id]=@Id;
GO
/**************************************************************************
** Stored Procedure: spGetRoleIdByName
**
** In:
**   @Name - The role name
**
** Description: 
		Selects the role id by name
**************************************************************************/
CREATE PROCEDURE spGetRoleIdByName
@Name VARCHAR(64)		
AS 
	SELECT [Id],[Name]
    FROM [dbo].[Role]
	WHERE  [Name]=@Name;
GO

/**************************************************************************
** Stored Procedure: spCreateParticipant
**
** In:
**   @Username - The user name
**	 @SurveyId - The survery id
**	 @BirthDate - the birth date
**	 @Weight - The weight
**	 @HeightFeet - The height in feet
**	 @HeightInches - The height in inches
 ** Out:
 **    @Id - The Id
** Description: 
**		Creates participant 
**************************************************************************/
CREATE PROCEDURE spCreateParticipant
@Id BIGINT OUT,
@Username VARCHAR(45),
@SurveyId BIGINT,
@Weight INT,
@HeightFeet INT,
@HeightInches INT,
@BirthDate DATETIME=NULL
AS 
	INSERT INTO [dbo].[Participant]([Username],[SurveyId],[BirthDate],[Weight],[HeightFeet],[HeightInches])
    VALUES (@Username, @SurveyId, @BirthDate, @Weight, @HeightFeet, @HeightInches);
	SET @Id = SCOPE_IDENTITY();
	INSERT INTO [dbo].[ParticipantSurvey]([Id],[ParticipantId],[SurveyId],[CurrentPrototypeTestId])
    VALUES (@Id, @Id, @SurveyId, 0);
GO

/**************************************************************************
** Stored Procedure: spCreateParticipantRole
**
** In:
**   @Username - The user name
**	 @RoleId - The role id
**
** Description: 
		Creates participant role
**************************************************************************/
CREATE PROCEDURE spCreateParticipantRole
	@ParticipantId			BIGINT,
	@RoleId					BIGINT
AS 
	INSERT INTO [dbo].[ParticipantRole]([ParticipantId],[RoleId])
    VALUES (@ParticipantId, @RoleId);
GO
/**************************************************************************
** Stored Procedure: spGetParticipantCredentialsByUsername
**
** In:
**   @Username - The user name
**
** Description: 
		Selects participant credentials by username
**************************************************************************/
CREATE PROCEDURE spGetParticipantCredentialsByUsername
@Username VARCHAR(64)
AS 
	SELECT TOP 1 [Id],
		 [SurveyId],
		 [Username],
		 [Password],
		 [Used]
	  FROM [dbo].[ParticipantCredentials]
	  WHERE	  [Username]=@Username AND [Used]=0;
GO
/**************************************************************************
** Stored Procedure: spGetParticipant
**
** In:
**   @Participantd - The participant id
**
** Description: 
**   Gets participants
**************************************************************************/
CREATE PROCEDURE spGetParticipant
@ParticipantId	BIGINT
AS 
	SELECT [Id],
      [Username],
      [SurveyId],
	  [BirthDate],
      [Weight],
      [HeightFeet],
      [HeightInches]
	FROM [dbo].[Participant]
	WHERE [Id]=@ParticipantId
GO
/**************************************************************************
** Stored Procedure: spGetUserRoles
**
** In:
**   @Username - The user name
**
** Description: 
**   Gets user roles
**************************************************************************/
CREATE PROCEDURE spGetUserRoles
@Username VARCHAR(64)
AS
	SELECT r.[Id], r.[Name]
	FROM [dbo].[Role] r
	INNER JOIN [dbo].[UserRole] ur ON r.[Id]=ur.[RoleId]
	INNER JOIN [dbo].[Users] u  ON u.Id=ur.UserId
	WHERE u.username=@username;
GO
/**************************************************************************
** Stored Procedure: spGetUserRoleNames
**
** In:
**   @Username - The user name
**
** Description: 
**   Gets user role names
**************************************************************************/
CREATE PROCEDURE spGetUserRoleNames
@Username VARCHAR(64)
AS
	SELECT r.[Name]
	FROM [dbo].[Role] r
	INNER JOIN [dbo].[UserRole] ur ON r.[Id]=ur.[RoleId]
	INNER JOIN [dbo].[Users] u  ON u.Id=ur.UserId
	WHERE u.username=@username;
GO
/**************************************************************************
** Stored Procedure: spGetParticipantRoles
**
** In:
**   @Username - The user name
**
** Description: 
**   Gets participant roles
**************************************************************************/
CREATE PROCEDURE spGetParticipantRoles
@Username VARCHAR(64)
AS
	SELECT r.[Name]r
	FROM [dbo].[Role] r
	INNER JOIN [dbo].[ParticipantRole] pr ON r.[Id]=pr.[RoleId]
	INNER JOIN [dbo].[Participant] p ON p.Id=pr.ParticipantId
	WHERE [Username]=@Username
GO
/**************************************************************************
 ** Stored Procedure: spGetProfileAnswersByParticipantId
 ** In:
 **    @ParticipantId - the participant id.
 **
 ** Description:
 **    Gets a record from the ProfileAnswer table and child tables depending on the @AnswerType.
 **************************************************************************/
CREATE PROC [dbo].[spGetProfileAnswersByParticipantId]
@ParticipantId BIGINT
AS
	SELECT pa.Id, QuestionId, AnswerType, pra.Value, psa.AnswerOptionId, psia.Input
	FROM [dbo].[ProfileAnswer] pa
	LEFT JOIN [dbo].[ProfileRangeAnswer] pra ON pa.Id = pra.Id
	LEFT JOIN [dbo].[ProfileSingleAnswer] psa ON pa.Id= psa.Id
	LEFT JOIN [dbo].[ProfileSingleInputAnswer] psia ON psia.Id = pa.Id
	WHERE ParticipantId=@ParticipantId; 
GO
/**************************************************************************
 ** Stored Procedure: spCreateProfileAnswer
 ** In:
 **    @ParticipantId - the participant id.
 **    @AnswerType - the answer type.
 **    @QuestionId - the question id.
 **    @AnswerOptionId - the answer option id.
 **    @Input - the input.
 **    @Value - the value.
 ** Out:
 **    @Id - The Id
 ** Description:
 **    Inserts a new record into the ProfileAnswer table and child tables depending on the @AnswerType.
 **************************************************************************/
CREATE PROC [dbo].[spCreateProfileAnswer]
@Id	BIGINT	OUTPUT,
@ParticipantId BIGINT,
@AnswerType VARCHAR(32),
@QuestionId BIGINT,
@AnswerOptionId BIGINT=null,
@Input VARCHAR(128)=null,
@Value INT=null
AS
	INSERT INTO [dbo].[ProfileAnswer](QuestionId, AnswerType, ParticipantId)
	VALUES(@QuestionId,@AnswerType,@ParticipantId);

	SET @Id=SCOPE_IDENTITY();

	IF (@AnswerType='RangeAnswer')
	BEGIN
		INSERT INTO [dbo].[ProfileRangeAnswer](Id, Value)
		VALUES(@Id,@value);
	END
	ELSE IF (@AnswerType='SingleAnswerInput' OR @AnswerType='SingleAnswer')
	BEGIN
			INSERT INTO [dbo].[ProfileSingleAnswer](Id, AnswerOptionId)
			VALUES(@Id,@AnswerOptionId);

		IF (@AnswerType='SingleAnswerInput')
		BEGIN
			INSERT INTO [dbo].[ProfileSingleInputAnswer](Id, Input)
			VALUES(@Id, @Input);
		END
	END;
	
GO
/**************************************************************************
 ** Stored Procedure: spGetUserByUsername
 ** In:
 **    @Username	- The username
 **
 ** Description:
 **    Gets the user by username.
 **************************************************************************/
CREATE PROC [dbo].[spGetUserByUsername]
@Username VARCHAR(64)
AS
	SELECT [Id], [Name],[UserPicPath] FROM [dbo].[Users] WHERE [Username]=@username;
GO
/**************************************************************************
 ** Stored Procedure: spCheckParticipantUsernamePassword
 ** In:
 **    @Username	- The username
 **    @Password	- The password
 **
 ** Description:
 **    Tries to see if there is a row in the ParticipantCredentials table that matches the parameter
 **************************************************************************/
CREATE PROC [dbo].[spCheckParticipantUsernamePassword]
@Username VARCHAR(64),
@Password VARCHAR(64)
AS
 IF EXISTS(SELECT * FROM [dbo].[ParticipantCredentials] WHERE [username]=@Username AND [Password]=@Password)
 BEGIN
  SELECT 1 AS [IsExist];
 END
 ELSE
 BEGIN 
	SELECT 0 AS [IsExist];
 END;
GO
/**************************************************************************
 ** Stored Procedure: spCheckUserUsernamePassword
 ** In:
 **    @Username	- The username
 **    @Password	- The password
 **
 ** Description:
 **    Tries to see if there is a row in the User table that matches the parameter
 **************************************************************************/
CREATE PROC [dbo].[spCheckUserUsernamePassword]
@Username VARCHAR(64),
@Password VARCHAR(256)
AS
 IF EXISTS(SELECT * FROM [dbo].[Users] WHERE [username]=@Username AND [Password]=@Password)
 BEGIN
	SELECT 1 AS [IsExist];
 END
 ELSE
 BEGIN 
	SELECT 0  AS [IsExist];
 END;
GO
/**************************************************************************
 ** Stored Procedure: spSearchSurveys
 **
 ** In:
 **		@Name - the survey name.
 **		@PageSize - the page size.
 **		@PageNumber - the 1-based page number (0 - to retrieve all records).
 **		@SortColumn - the sort column.
 **		@SortAscending - the sort order (1 - ascending, 0 - descending).
 ** Out:
 **    @total - The total records
 **
 ** Description:
 **    Searches surveys.
 **************************************************************************/
CREATE PROCEDURE [dbo].spSearchSurveys 
	@Name VARCHAR(100) = NULL,
	@PageSize INT = 0,
	@PageNumber INT = 0,
	@SortColumn VARCHAR(50) = 'Id',
	@SortAscending BIT = 1,
	@total BIGINT OUT
AS
BEGIN
	DECLARE @startCount INT;
    DECLARE @endCount INT;

	IF @PageNumber <= 0
    BEGIN
        SET @startCount = 1;
        SET @endCount = 2147483647;
    END
    ELSE BEGIN
        SET @startCount = (@PageNumber - 1) * @PageSize + 1;
        SET @endCount = @PageNumber * @PageSize;
    END;
	IF (@SortColumn=null OR @SortColumn='')
		SET @SortColumn='Id';

	IF @Name IS NULL
		SET @Name='';

	DECLARE  @temp TABLE(
	  [Id] bigint ,
	  [Name] varchar(64) ,
	  [CellName] varchar(64) ,
	  [ParticipantsNumber] int,
	  [Status] varchar(32) ,
	  [DateCreated] DATETIME ,
	  [CompletedSurveysNumber] int,
	  [Draft] bit ,
	  [RowNum] INT 
	)
	-- Insert filtered results into temporary table
	   INSERT INTO @temp
	   SELECT
			[Id] ,
			[Name],
			[CellName] ,
			[ParticipantsNumber],
			[Status] ,
			[DateCreated] ,
			[CompletedSurveysNumber] ,
			[Draft],
			CASE WHEN @SortAscending = 1 THEN 
					CASE @SortColumn
						WHEN 'Id' THEN ROW_NUMBER() OVER (ORDER BY Id ASC)
						WHEN 'Name' THEN ROW_NUMBER() OVER (ORDER BY Name ASC)
						WHEN 'CellName' THEN ROW_NUMBER() OVER (ORDER BY CellName ASC)
						WHEN 'ParticipantsNumber' THEN ROW_NUMBER() OVER (ORDER BY ParticipantsNumber ASC)
						WHEN 'Status' THEN ROW_NUMBER() OVER (ORDER BY Status ASC)
						WHEN 'DateCreated' THEN ROW_NUMBER() OVER (ORDER BY DateCreated ASC)
						WHEN 'CompletedSurveysNumber' THEN ROW_NUMBER() OVER (ORDER BY CompletedSurveysNumber ASC)
						WHEN 'Draft' THEN ROW_NUMBER() OVER (ORDER BY Draft ASC)
					END 
				ELSE  
					CASE @SortColumn
						WHEN 'Id' THEN ROW_NUMBER() OVER (ORDER BY Id DESC)
						WHEN 'Name' THEN ROW_NUMBER() OVER (ORDER BY Name DESC)
						WHEN 'CellName' THEN ROW_NUMBER() OVER (ORDER BY CellName DESC)
						WHEN 'ParticipantsNumber' THEN ROW_NUMBER() OVER (ORDER BY ParticipantsNumber DESC)
						WHEN 'Status' THEN ROW_NUMBER() OVER (ORDER BY Status DESC)
						WHEN 'DateCreated' THEN ROW_NUMBER() OVER (ORDER BY DateCreated DESC)
						WHEN 'CompletedSurveysNumber' THEN ROW_NUMBER() OVER (ORDER BY CompletedSurveysNumber DESC)
						WHEN 'Draft' THEN ROW_NUMBER() OVER (ORDER BY Draft ASC)
					END 
			END as [RowNum] 
		FROM [dbo].[Survey]
		WHERE (@Name IS NULL OR (@Name IS NOT NULL AND Name LIKE '%'+@Name+'%'))

	-- Select records for required page
	SELECT
			[Id] ,
			[Name],
			[CellName] ,
			[ParticipantsNumber],
			[Status] ,
			[DateCreated] ,
			[CompletedSurveysNumber] ,
			[Draft]
	FROM @temp
	WHERE @PageNumber = 0 OR [RowNum] BETWEEN @startCount AND @endCount
	ORDER BY [RowNum]
		
	SELECT @total=COUNT(1) FROM @temp;
	
END
GO
/**************************************************************************
 ** Stored Procedure: spGetAllSurveys
 **
 ** In:
 **		@PageSize - the page size.
 **		@PageNumber - the 1-based page number (0 - to retrieve all records).
 **		@SortColumn - the sort column.
 **		@SortAscending - the sort order (1 - ascending, 0 - descending).
 ** Out:
 **    @total - The total records
 **
 ** Description:
 **    Get All Surveys.
 **************************************************************************/
CREATE PROCEDURE [dbo].spGetAllSurveys 
	@PageSize INT = 0,
	@PageNumber INT = 0,
	@SortColumn VARCHAR(50) = 'Id',
	@SortAscending BIT = 1,
	@total BIGINT OUT
AS
BEGIN
	DECLARE @startCount INT;
    DECLARE @endCount INT;

	IF @PageNumber <= 0
    BEGIN
        SET @startCount = 1;
        SET @endCount = 2147483647;
    END
    ELSE BEGIN
        SET @startCount = (@PageNumber - 1) * @PageSize + 1;
        SET @endCount = @PageNumber * @PageSize;
    END;

	-- Insert filtered results into temporary table
	   SELECT
			[Id] ,
			[Name],
			[CellName] ,
			[ParticipantsNumber],
			[Status] ,
			[DateCreated] ,
			[CompletedSurveysNumber] ,
			[Draft],
			CASE WHEN @SortAscending = 1 THEN 
					CASE @SortColumn
						WHEN 'Id' THEN ROW_NUMBER() OVER (ORDER BY Id ASC)
						WHEN 'Name' THEN ROW_NUMBER() OVER (ORDER BY Name ASC)
						WHEN 'CellName' THEN ROW_NUMBER() OVER (ORDER BY CellName ASC)
						WHEN 'ParticipantsNumber' THEN ROW_NUMBER() OVER (ORDER BY ParticipantsNumber ASC)
						WHEN 'Status' THEN ROW_NUMBER() OVER (ORDER BY Status ASC)
						WHEN 'DateCreated' THEN ROW_NUMBER() OVER (ORDER BY DateCreated ASC)
						WHEN 'CompletedSurveysNumber' THEN ROW_NUMBER() OVER (ORDER BY CompletedSurveysNumber ASC)
						WHEN 'Draft' THEN ROW_NUMBER() OVER (ORDER BY Draft ASC)
					END 
				ELSE  
					CASE @SortColumn
						WHEN 'Id' THEN ROW_NUMBER() OVER (ORDER BY Id DESC)
						WHEN 'Name' THEN ROW_NUMBER() OVER (ORDER BY Name DESC)
						WHEN 'CellName' THEN ROW_NUMBER() OVER (ORDER BY CellName DESC)
						WHEN 'ParticipantsNumber' THEN ROW_NUMBER() OVER (ORDER BY ParticipantsNumber DESC)
						WHEN 'Status' THEN ROW_NUMBER() OVER (ORDER BY Status DESC)
						WHEN 'DateCreated' THEN ROW_NUMBER() OVER (ORDER BY DateCreated DESC)
						WHEN 'CompletedSurveysNumber' THEN ROW_NUMBER() OVER (ORDER BY CompletedSurveysNumber DESC)
						WHEN 'Draft' THEN ROW_NUMBER() OVER (ORDER BY Draft ASC)
					END 
			END as [RowNum] 
		INTO #temp
		FROM [dbo].[Survey]
		

	-- Select records for required page
	SELECT
			[Id] ,
			[Name],
			[CellName] ,
			[ParticipantsNumber],
			[Status] ,
			[DateCreated] ,
			[CompletedSurveysNumber] ,
			[Draft]
			
	FROM #temp
	WHERE @PageNumber = 0 OR [RowNum] BETWEEN @startCount AND @endCount
	ORDER BY [RowNum]
		
	SELECT @total=COUNT(*) FROM #temp;
	
END
GO