/*
 * @Description: Defines the tables for Tholos Survey applications
 * @Author: veshu
 * @Version: 1.0
 * @Copyright: Copyright (C) 2015, TopCoder, Inc. All rights reserved.
 */

IF OBJECT_ID('[dbo].[UserRole]', 'U') IS NOT NULL  DROP TABLE [dbo].[UserRole];
IF OBJECT_ID('[dbo].[ParticipantRole]', 'U') IS NOT NULL  DROP TABLE [dbo].[ParticipantRole];
IF OBJECT_ID('[dbo].[Role]', 'U') IS NOT NULL  DROP TABLE [dbo].[Role];
IF OBJECT_ID('[dbo].[Users]', 'U') IS NOT NULL  DROP TABLE [dbo].[Users];
IF OBJECT_ID('[dbo].[ParticipantCredentials]', 'U') IS NOT NULL  DROP TABLE [dbo].[ParticipantCredentials];
IF OBJECT_ID('[dbo].[SurveyProrotypeQuestion]', 'U') IS NOT NULL  DROP TABLE [dbo].[SurveyProrotypeQuestion];
IF OBJECT_ID('[dbo].[PrototypeCode]', 'U') IS NOT NULL  DROP TABLE [dbo].[PrototypeCode];
IF OBJECT_ID('[dbo].[SurveyPostTestingQuestion]', 'U') IS NOT NULL  DROP TABLE [dbo].[SurveyPostTestingQuestion];

IF OBJECT_ID('[dbo].[ProfileRangeAnswer]', 'U') IS NOT NULL  DROP TABLE [dbo].[ProfileRangeAnswer];
IF OBJECT_ID('[dbo].[ProfileSingleInputAnswer]', 'U') IS NOT NULL  DROP TABLE [dbo].[ProfileSingleInputAnswer];
IF OBJECT_ID('[dbo].[ProfileSingleAnswer]', 'U') IS NOT NULL  DROP TABLE [dbo].[ProfileSingleAnswer];
IF OBJECT_ID('[dbo].[ProfileAnswer]', 'U') IS NOT NULL  DROP TABLE [dbo].[ProfileAnswer];
IF OBJECT_ID('[dbo].[ProfileRangeAnswerOption]', 'U') IS NOT NULL  DROP TABLE [dbo].[ProfileRangeAnswerOption];
IF OBJECT_ID('[dbo].[ProfileSingleAnswerOption]', 'U') IS NOT NULL  DROP TABLE [dbo].[ProfileSingleAnswerOption];
IF OBJECT_ID('[dbo].[ProfileAnswerOption]', 'U') IS NOT NULL  DROP TABLE [dbo].[ProfileAnswerOption];
IF OBJECT_ID('[dbo].[ProfileQuestionCondition]', 'U') IS NOT NULL  DROP TABLE [dbo].[ProfileQuestionCondition];
IF OBJECT_ID('[dbo].[ProfileQuestion]', 'U') IS NOT NULL  DROP TABLE [dbo].[ProfileQuestion];

IF OBJECT_ID('[dbo].[PostTestingRangeAnswer]', 'U') IS NOT NULL  DROP TABLE [dbo].[PostTestingRangeAnswer];
IF OBJECT_ID('[dbo].[PostTestingSingleInputAnswer]', 'U') IS NOT NULL  DROP TABLE [dbo].[PostTestingSingleInputAnswer];
IF OBJECT_ID('[dbo].[PostTestingSingleAnswer]', 'U') IS NOT NULL  DROP TABLE [dbo].[PostTestingSingleAnswer];
IF OBJECT_ID('[dbo].[PostTestingAnswer]', 'U') IS NOT NULL  DROP TABLE [dbo].[PostTestingAnswer];
IF OBJECT_ID('[dbo].[PostTestingSingleAnswerOption]', 'U') IS NOT NULL  DROP TABLE [dbo].[PostTestingSingleAnswerOption];
IF OBJECT_ID('[dbo].[PostTestingRangeAnswerOption]', 'U') IS NOT NULL  DROP TABLE [dbo].[PostTestingRangeAnswerOption];
IF OBJECT_ID('[dbo].[PostTestingAnswerOption]', 'U') IS NOT NULL  DROP TABLE [dbo].[PostTestingAnswerOption];
IF OBJECT_ID('[dbo].[PostTestingQuestionCondition]', 'U') IS NOT NULL  DROP TABLE [dbo].[PostTestingQuestionCondition];
IF OBJECT_ID('[dbo].[PostTestingQuestion]', 'U') IS NOT NULL  DROP TABLE [dbo].[PostTestingQuestion];

IF OBJECT_ID('[dbo].[PrototypeRangeAnswer]', 'U') IS NOT NULL  DROP TABLE [dbo].[PrototypeRangeAnswer];
IF OBJECT_ID('[dbo].[PrototypeSingleInputAnswer]', 'U') IS NOT NULL  DROP TABLE [dbo].[PrototypeSingleInputAnswer];
IF OBJECT_ID('[dbo].[PrototypeSingleAnswer]', 'U') IS NOT NULL  DROP TABLE [dbo].[PrototypeSingleAnswer];
IF OBJECT_ID('[dbo].[PrototypeAnswer]', 'U') IS NOT NULL  DROP TABLE [dbo].[PrototypeAnswer];
IF OBJECT_ID('[dbo].[PrototypeSingleAnswerOption]', 'U') IS NOT NULL  DROP TABLE [dbo].[PrototypeSingleAnswerOption];
IF OBJECT_ID('[dbo].[PrototypeRangeAnswerOption]', 'U') IS NOT NULL  DROP TABLE [dbo].[PrototypeRangeAnswerOption];
IF OBJECT_ID('[dbo].[PrototypeAnswerOption]', 'U') IS NOT NULL  DROP TABLE [dbo].[PrototypeAnswerOption];
IF OBJECT_ID('[dbo].[PrototypeQuestionCondition]', 'U') IS NOT NULL  DROP TABLE [dbo].[PrototypeQuestionCondition];
IF OBJECT_ID('[dbo].[PrototypeQuestion]', 'U') IS NOT NULL  DROP TABLE [dbo].[PrototypeQuestion];

IF OBJECT_ID('[dbo].[PrototypeTest]', 'U') IS NOT NULL  DROP TABLE [dbo].[PrototypeTest];
IF OBJECT_ID('[dbo].[QuickAccessSurvey]', 'U') IS NOT NULL  DROP TABLE [dbo].[QuickAccessSurvey];
IF OBJECT_ID('[dbo].[ParticipantSurvey]', 'U') IS NOT NULL  DROP TABLE [dbo].[ParticipantSurvey];
IF OBJECT_ID('[dbo].[Survey]', 'U') IS NOT NULL  DROP TABLE [dbo].[Survey];
IF OBJECT_ID('[dbo].[Participant]', 'U') IS NOT NULL  DROP TABLE [dbo].[Participant];

-- -----------------------------------------------------
-- Table [dbo].[Participant]
-- -----------------------------------------------------
CREATE TABLE [dbo].[Participant] (
  [Id] bigint IDENTITY NOT NULL,
  [Username] varchar(64) NOT NULL,
  [SurveyId] bigint NOT NULL,
  [BirthDate] DateTime NULL,
  [Weight] int NOT NULL,
  [HeightFeet] int NOT NULL,
  [HeightInches] int NOT NULL,
  PRIMARY KEY ([Id])
)
CREATE UNIQUE NONCLUSTERED INDEX IX_Participant_Username_SurveyId   ON [dbo].[Participant] (SurveyId, Username);
-- -----------------------------------------------------
-- Table [dbo].[ParticipantSurvey]
-- -----------------------------------------------------
CREATE TABLE [dbo].[ParticipantSurvey] (
  [Id] bigint NOT NULL,
  [ParticipantId] bigint NOT NULL,
  [SurveyId] bigint NOT NULL,
  [CurrentPrototypeTestId] bigint NOT NULL,
  PRIMARY KEY ([Id])
)

-- -----------------------------------------------------
-- Table [dbo].[Survey]
-- -----------------------------------------------------
CREATE TABLE [dbo].[Survey] (
  [Id] bigint IDENTITY NOT NULL,
  [Name] varchar(64) NOT NULL,
  [CellName] varchar(64) NOT NULL,
  [ParticipantsNumber] int NOT NULL,
  [Status] varchar(32) NOT NULL,
  [DateCreated] DATETIME NOT NULL default GETDATE(),
  [CompletedSurveysNumber] int NOT NULL,
  [Draft] bit NOT NULL,
  [CreatedBy] bigint NOT NULL,
  PRIMARY KEY ([Id])
)

-- -----------------------------------------------------
-- Table [dbo].[PrototypeQuestion]
-- -----------------------------------------------------
CREATE TABLE [dbo].[PrototypeQuestion] (
  [Id] bigint NOT NULL,
  [Text] varchar(1024) NOT NULL,
  [QuestionType] varchar(32) NOT NULL,
  [Conditional] bit NOT NULL,
  PRIMARY KEY ([Id])
)

-- -----------------------------------------------------
-- Table [dbo].[PrototypeAnswerOption]
-- -----------------------------------------------------
CREATE TABLE [dbo].[PrototypeAnswerOption] (
  [Id] bigint NOT NULL,
  [QuestionId] bigint NOT NULL,
  [AnswerType] varchar(32) NOT NULL,
  PRIMARY KEY ([Id]),
  CONSTRAINT [PrototypeAnswerOption_PrototypeQuestion_fk]
  FOREIGN KEY ([QuestionId])
  REFERENCES [dbo].[PrototypeQuestion] ([Id])
  ON DELETE NO ACTION
  ON UPDATE NO ACTION
)
CREATE INDEX [PrototypeAnswerOption_PrototypeQuestion_fk_idx] ON [dbo].[PrototypeAnswerOption] ([QuestionId] ASC);

-- -----------------------------------------------------
-- Table [dbo].[PrototypeSingleAnswerOption]
-- -----------------------------------------------------
CREATE TABLE [dbo].[PrototypeSingleAnswerOption] (
  [Id] bigint NOT NULL,
  [Value] int NOT NULL,
  [Label] varchar(64) NOT NULL,
  [ConditionalQuestionId] bigint NULL,
  PRIMARY KEY ([Id]),
  CONSTRAINT [PrototypeSingleAnswerOption_PrototypeAnswerOption_fk]
  FOREIGN KEY ([Id])
  REFERENCES [dbo].[PrototypeAnswerOption] ([Id])
  ON DELETE NO ACTION
  ON UPDATE NO ACTION
)

-- -----------------------------------------------------
-- Table [dbo].[PrototypeRangeAnswerOption]
-- -----------------------------------------------------
CREATE TABLE [dbo].[PrototypeRangeAnswerOption] (
  [Id] bigint NOT NULL,
  [FromValueLabel] varchar(64) NOT NULL,
  [ToValueLabel] varchar(64) NOT NULL,
  [FromValue] int NOT NULL,
  [ToValue] int NOT NULL,
  [Increment] int NULL,
  PRIMARY KEY ([Id]),
  CONSTRAINT [PrototypeRangeAnswerOption_PrototypeAnswerOption_FK]
  FOREIGN KEY ([Id])
  REFERENCES [dbo].[PrototypeAnswerOption] ([Id])
  ON DELETE NO ACTION
  ON UPDATE NO ACTION
)

-- -----------------------------------------------------
-- Table [dbo].[SurveyProrotypeQuestion]
-- -----------------------------------------------------
CREATE TABLE [dbo].[SurveyProrotypeQuestion] (
  [Id] bigint IDENTITY NOT NULL,
  [SurveyId] bigint NOT NULL,
  [QuestionId] bigint NOT NULL,
  [QuestionNumber] int NOT NULL,
  PRIMARY KEY ([Id]),
  CONSTRAINT [SurveyProrotypeQuestion_Survey_FK]
  FOREIGN KEY ([SurveyId])
  REFERENCES [dbo].[Survey] ([Id])
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
  CONSTRAINT [SurveyProrotypeQuestion_PrototypeQuestion_FK]
  FOREIGN KEY ([QuestionId])
  REFERENCES [dbo].[PrototypeQuestion] ([Id])
  ON DELETE NO ACTION
  ON UPDATE NO ACTION
)
CREATE INDEX [SurveyProrotypeQuestion_Survey_FK_idx] ON [dbo].[SurveyProrotypeQuestion] ([SurveyId] ASC);
CREATE INDEX [SurveyProrotypeQuestion_PrototypeQuestion_FK_idx] ON [dbo].[SurveyProrotypeQuestion] ([QuestionId] ASC);

-- -----------------------------------------------------
-- Table [dbo].[PostTestingQuestion]
-- -----------------------------------------------------
CREATE TABLE [dbo].[PostTestingQuestion] (
  [Id] bigint NOT NULL,
  [Text] varchar(1024) NOT NULL,
  [Conditional] bit NOT NULL,
  [QuestionType] varchar(32) NOT NULL,
  PRIMARY KEY ([Id])
)

-- -----------------------------------------------------
-- Table [dbo].[PostTestingAnswerOption]
-- -----------------------------------------------------
CREATE TABLE [dbo].[PostTestingAnswerOption] (
  [Id] bigint NOT NULL,
  [QuestionId] bigint NOT NULL,
  [AnswerType] varchar(32) NOT NULL,
  PRIMARY KEY ([Id]),
  CONSTRAINT [PostTestingAnswerOption_PostTestingQuestion_fk0]
  FOREIGN KEY ([QuestionId])
  REFERENCES [dbo].[PostTestingQuestion] ([Id])
  ON DELETE NO ACTION
  ON UPDATE NO ACTION
)
CREATE INDEX [PostTestingAnswerOption_PostTestingQuestion_fk_idx] ON [dbo].[PostTestingAnswerOption] ([QuestionId] ASC);

-- -----------------------------------------------------
-- Table [dbo].[PostTestingRangeAnswerOption]
-- -----------------------------------------------------
CREATE TABLE [dbo].[PostTestingRangeAnswerOption] (
  [Id] bigint NOT NULL,
  [FromValueLabel] varchar(64) NOT NULL,
  [ToValueLabel] varchar(64) NOT NULL,
  [FromValue] int NOT NULL,
  [ToValue] int NOT NULL,
  [Increment] int NULL,
  PRIMARY KEY ([Id]),
  CONSTRAINT [PrototypeRangeAnswerOption_PostTestingAnswerOption_FK0]
  FOREIGN KEY ([Id])
  REFERENCES [dbo].[PostTestingAnswerOption] ([Id])
  ON DELETE NO ACTION
  ON UPDATE NO ACTION
)

-- -----------------------------------------------------
-- Table [dbo].[PostTestingSingleAnswerOption]
-- -----------------------------------------------------
CREATE TABLE [dbo].[PostTestingSingleAnswerOption] (
  [Id] bigint NOT NULL,
  [Value] int NOT NULL,
  [Label] varchar(64) NOT NULL,
  [ConditionalQuestionId] bigint NULL,
  PRIMARY KEY ([Id]),
  CONSTRAINT [PostTestingSingleAnswerOption_PostTestingAnswerOption_fk0]
  FOREIGN KEY ([Id])
  REFERENCES [dbo].[PostTestingAnswerOption] ([Id])
  ON DELETE NO ACTION
  ON UPDATE NO ACTION
)

-- -----------------------------------------------------
-- Table [dbo].[SurveyPostTestingQuestion]
-- -----------------------------------------------------
CREATE TABLE [dbo].[SurveyPostTestingQuestion] (
  [Id] bigint IDENTITY NOT NULL,
  [SurveyId] bigint NOT NULL,
  [QuestionId] bigint NOT NULL,
  [QuestionNumber] int NOT NULL,
  PRIMARY KEY ([Id]),
  CONSTRAINT [SurveyPostTestingQuestion_Survey_FK]
  FOREIGN KEY ([SurveyId])
  REFERENCES [dbo].[Survey] ([Id])
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
  CONSTRAINT [SurveyPostTestingQuestion_PostTestingQuestion_FK]
  FOREIGN KEY ([QuestionId])
  REFERENCES [dbo].[PostTestingQuestion] ([Id])
  ON DELETE NO ACTION
  ON UPDATE NO ACTION
)
CREATE INDEX [SurveyPostTestingQuestion_Survey_FK_idx] ON [dbo].[SurveyPostTestingQuestion] ([SurveyId] ASC);
CREATE INDEX [SurveyPostTestingQuestion_PostTestingQuestion_FK_idx] ON [dbo].[SurveyPostTestingQuestion] ([QuestionId] ASC);

-- -----------------------------------------------------
-- Table [dbo].[PrototypeCode]
-- -----------------------------------------------------
CREATE TABLE [dbo].[PrototypeCode] (
  [Id] bigint IDENTITY NOT NULL,
  [Code] varchar(64) NOT NULL,
  [PrototypesPerCode] int NOT NULL,
  [SurveyId] bigint NOT NULL,
  PRIMARY KEY ([Id]),
  CONSTRAINT [PrototypeCode_Survey_FK]
  FOREIGN KEY ([SurveyId])
  REFERENCES [dbo].[Survey] ([Id])
  ON DELETE NO ACTION
  ON UPDATE NO ACTION
)
CREATE INDEX [PrototypeCode_Survey_FK_idx] ON [dbo].[PrototypeCode] ([SurveyId] ASC);

-- -----------------------------------------------------
-- Table [dbo].[Role]
-- -----------------------------------------------------
CREATE TABLE [dbo].[Role] (
  [Id] bigint NOT NULL,
  [Name] varchar(64) NOT NULL,
  PRIMARY KEY ([Id])
)

-- -----------------------------------------------------
-- Table [dbo].[ParticipantRole]
-- -----------------------------------------------------
CREATE TABLE [dbo].[ParticipantRole] (
  [ParticipantId] bigint NOT NULL,
  [RoleId] bigint NOT NULL,
  PRIMARY KEY ([ParticipantId], [RoleId]),
  CONSTRAINT [ParticipantRole_Role_FK]
  FOREIGN KEY ([RoleId])
  REFERENCES [dbo].[Role] ([Id])
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
  CONSTRAINT [ParticipantRole_Participant_FK]
  FOREIGN KEY ([ParticipantId])
  REFERENCES [dbo].[Participant] ([Id])
  ON DELETE NO ACTION
  ON UPDATE NO ACTION
)
CREATE INDEX [ParticipantRole_Role_FK_idx] ON [dbo].[ParticipantRole] ([RoleId] ASC);

-- -----------------------------------------------------
-- Table [dbo].[ParticipantCredentials]
-- -----------------------------------------------------
CREATE TABLE [dbo].[ParticipantCredentials] (
  [Id] bigint IDENTITY NOT NULL,
  [SurveyId] bigint NOT NULL,
  [Username] varchar(64) NOT NULL,
  [Password] varchar(64) NOT NULL,
  [Used] bit NOT NULL,
  PRIMARY KEY ([Id]),
  CONSTRAINT [SurveyParticipantCredentials_Survey_FK]
  FOREIGN KEY ([SurveyId])
  REFERENCES [dbo].[Survey] ([Id])
  ON DELETE NO ACTION
  ON UPDATE NO ACTION
)
CREATE INDEX [SurveyParticipantCredentials_Survey_FK_idx] ON [dbo].[ParticipantCredentials] ([SurveyId] ASC);
CREATE UNIQUE NONCLUSTERED INDEX IX_SurveyParticipantCredentials_Username_SurveyId   ON [dbo].[ParticipantCredentials] (SurveyId, Username);
-- -----------------------------------------------------
-- Table [dbo].[PrototypeTest]
-- -----------------------------------------------------
CREATE TABLE [dbo].[PrototypeTest] (
  [Id] bigint IDENTITY NOT NULL,
  [ParticipantSurveyId] bigint NOT NULL,
  [PrototypeCode] varchar(64) NOT NULL,
  [Iteration] int NOT NULL,
  [BeforeWeight] real NOT NULL,
  [AfterWeight] real NULL,
  [BeforePhotoLocalPath] varchar(1024) NOT NULL,
  [AfterPhotoLocalPath] varchar(1024) NULL,
  [BeforePhotoUri] varchar(1024) NOT NULL,
  [AfterPhotoUri] varchar(1024) NULL,
  [Completed] bit not null default 0,
  PRIMARY KEY ([Id]),
  CONSTRAINT [PrototypeTest_ParticipantSurvey_FK]
  FOREIGN KEY ([ParticipantSurveyId])
  REFERENCES [dbo].[ParticipantSurvey] ([Id])
  ON DELETE NO ACTION
  ON UPDATE NO ACTION
)
CREATE INDEX [PrototypeTest_ParticipantSurvey_FK_idx] ON [dbo].[PrototypeTest] ([ParticipantSurveyId] ASC);

-- -----------------------------------------------------
-- Table [dbo].[PrototypeAnswer]
-- -----------------------------------------------------
CREATE TABLE [dbo].[PrototypeAnswer] (
  [Id] bigint IDENTITY NOT NULL,
  [QuestionId] bigint NOT NULL,
  [AnswerType] varchar(32) NOT NULL,
  [PrototypeTestId] bigint NOT NULL,
  PRIMARY KEY ([Id]),
  CONSTRAINT [PrototypeAnswer_Question_FK]
  FOREIGN KEY ([QuestionId])
  REFERENCES [dbo].[PrototypeQuestion] ([Id])
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
  CONSTRAINT [PrototypeAnswer_PrototypeTest_FK]
  FOREIGN KEY ([PrototypeTestId])
  REFERENCES [dbo].[PrototypeTest] ([Id])
  ON DELETE NO ACTION
  ON UPDATE NO ACTION
)
CREATE INDEX [PrototypeAnswer_Question_FK_idx] ON [dbo].[PrototypeAnswer] ([QuestionId] ASC);
CREATE INDEX [PrototypeAnswer_PrototypeTest_FK_idx] ON [dbo].[PrototypeAnswer] ([PrototypeTestId] ASC);

-- -----------------------------------------------------
-- Table [dbo].[PrototypeSingleAnswer]
-- -----------------------------------------------------
CREATE TABLE [dbo].[PrototypeSingleAnswer] (
  [Id] bigint NOT NULL,
  [AnswerOptionId] bigint NOT NULL,
  PRIMARY KEY ([Id]),
  CONSTRAINT [prototopesingleanswerinputoption_PrototypeAnswer_fk1]
  FOREIGN KEY ([Id])
  REFERENCES [dbo].[PrototypeAnswer] ([Id])
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
  CONSTRAINT [PrototypeSingleAnswer_PrototypeAnswerOption]
  FOREIGN KEY ([AnswerOptionId])
  REFERENCES [dbo].[PrototypeAnswerOption] ([Id])
  ON DELETE NO ACTION
  ON UPDATE NO ACTION
)
CREATE INDEX [PrototypeSingleAnswer_PrototypeAnswerOption_idx] ON [dbo].[PrototypeSingleAnswer] ([AnswerOptionId] ASC);

-- -----------------------------------------------------
-- Table [dbo].[PrototypeRangeAnswer]
-- -----------------------------------------------------
CREATE TABLE [dbo].[PrototypeRangeAnswer] (
  [Id] bigint NOT NULL,
  [Value] int NOT NULL,
  PRIMARY KEY ([Id]),
  CONSTRAINT [PrototypeRangeAnswerOption_PrototypeAnswer_FK1]
  FOREIGN KEY ([Id])
  REFERENCES [dbo].[PrototypeAnswer] ([Id])
  ON DELETE NO ACTION
  ON UPDATE NO ACTION
)

-- -----------------------------------------------------
-- Table [dbo].[PrototypeSingleInputAnswer]
-- -----------------------------------------------------
CREATE TABLE [dbo].[PrototypeSingleInputAnswer] (
  [Id] bigint NOT NULL,
  [Input] varchar(128) NOT NULL,
  PRIMARY KEY ([Id]),
  CONSTRAINT [PrototypeSingleInputAnswer_PrototypeSingleAnswer_FK]
  FOREIGN KEY ([Id])
  REFERENCES [dbo].[PrototypeSingleAnswer] ([Id])
  ON DELETE NO ACTION
  ON UPDATE NO ACTION
)

-- -----------------------------------------------------
-- Table [dbo].[ProfileQuestion]
-- -----------------------------------------------------
CREATE TABLE [dbo].[ProfileQuestion] (
  [Id] bigint NOT NULL,
  [Text] varchar(1024) NOT NULL,
  [Conditional] bit NOT NULL,
  [QuestionType] varchar(32) NOT NULL,
  PRIMARY KEY ([Id])
)

-- -----------------------------------------------------
-- Table [dbo].[ProfileAnswerOption]
-- -----------------------------------------------------
CREATE TABLE [dbo].[ProfileAnswerOption] (
  [Id] bigint NOT NULL,
  [QuestionId] bigint NOT NULL,
  [AnswerType] varchar(32) NOT NULL,
  PRIMARY KEY ([Id]),
  CONSTRAINT [ProfileAnswerOption_ProfileQuestion_fk1]
  FOREIGN KEY ([QuestionId])
  REFERENCES [dbo].[ProfileQuestion] ([Id])
  ON DELETE NO ACTION
  ON UPDATE NO ACTION
)
CREATE INDEX [ProfileAnswerOption_ProfileQuestion_fk_idx] ON [dbo].[ProfileAnswerOption] ([QuestionId] ASC);

-- -----------------------------------------------------
-- Table [dbo].[ProfileRangeAnswerOption]
-- -----------------------------------------------------
CREATE TABLE [dbo].[ProfileRangeAnswerOption] (
  [Id] bigint NOT NULL,
  [FromValueLabel] varchar(64) NOT NULL,
  [ToValueLabel] varchar(64) NOT NULL,
  [FromValue] int NOT NULL,
  [ToValue] int NOT NULL,
  [Increment] int NULL,
  PRIMARY KEY ([Id]),
  CONSTRAINT [ProfileRangeAnswerOption_ProfileAnswerOption_FK2]
  FOREIGN KEY ([Id])
  REFERENCES [dbo].[ProfileAnswerOption] ([Id])
  ON DELETE NO ACTION
  ON UPDATE NO ACTION
)

-- -----------------------------------------------------
-- Table [dbo].[ProfileSingleAnswerOption]
-- -----------------------------------------------------
CREATE TABLE [dbo].[ProfileSingleAnswerOption] (
  [Id] bigint NOT NULL,
  [Value] int NOT NULL,
  [Label] varchar(64) NOT NULL,
  [ConditionalQuestionId] bigint NULL,
  PRIMARY KEY ([Id]),
  CONSTRAINT [ProfileSingleAnswerOption_ProfileAnswerOption_FK2]
  FOREIGN KEY ([Id])
  REFERENCES [dbo].[ProfileAnswerOption] ([Id])
  ON DELETE NO ACTION
  ON UPDATE NO ACTION
)

-- -----------------------------------------------------
-- Table [dbo].[PostTestingAnswer]
-- -----------------------------------------------------
CREATE TABLE [dbo].[PostTestingAnswer] (
  [Id] bigint IDENTITY NOT NULL,
  [QuestionId] bigint NOT NULL,
  [AnswerType] varchar(32) NOT NULL,
  [ParticipantSurveyId] bigint NOT NULL,
  PRIMARY KEY ([Id]),
  CONSTRAINT [PrototypeAnswer_PostTestingQuestion_FK0]
  FOREIGN KEY ([QuestionId])
  REFERENCES [dbo].[PostTestingQuestion] ([Id])
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
  CONSTRAINT [PostTestingAnswer_ParticipantSurvey_FK]
  FOREIGN KEY ([ParticipantSurveyId])
  REFERENCES [dbo].[ParticipantSurvey] ([Id])
  ON DELETE NO ACTION
  ON UPDATE NO ACTION
)
CREATE INDEX [PrototypeAnswer_PostTestingQuestion_FK0_idx] ON [dbo].[PostTestingAnswer] ([QuestionId] ASC);
CREATE INDEX [PostTestingAnswer_ParticipantSurvey_FK_idx] ON [dbo].[PostTestingAnswer] ([ParticipantSurveyId] ASC);

-- -----------------------------------------------------
-- Table [dbo].[PostTestingSingleAnswer]
-- -----------------------------------------------------
CREATE TABLE [dbo].[PostTestingSingleAnswer] (
  [Id] bigint NOT NULL,
  [AnswerOptionId] bigint NOT NULL,
  PRIMARY KEY ([Id]),
  CONSTRAINT [PostTestingSingleAnswer_PostTestingAnswer_FK10]
  FOREIGN KEY ([Id])
  REFERENCES [dbo].[PostTestingAnswer] ([Id])
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
  CONSTRAINT [PrototypeSingleAnswer_PostTestingAnswerOption]
  FOREIGN KEY ([AnswerOptionId])
  REFERENCES [dbo].[PostTestingAnswerOption] ([Id])
  ON DELETE NO ACTION
  ON UPDATE NO ACTION
)

-- -----------------------------------------------------
-- Table [dbo].[PostTestingSingleInputAnswer]
-- -----------------------------------------------------
CREATE TABLE [dbo].[PostTestingSingleInputAnswer] (
  [Id] bigint NOT NULL,
  [Input] varchar(128) NOT NULL,
  PRIMARY KEY ([Id]),
  CONSTRAINT [PrototypeSingleInputAnswer_PostTestingSingleAnswer_FK0]
  FOREIGN KEY ([Id])
  REFERENCES [dbo].[PostTestingSingleAnswer] ([Id])
  ON DELETE NO ACTION
  ON UPDATE NO ACTION
)

-- -----------------------------------------------------
-- Table [dbo].[PostTestingRangeAnswer]
-- -----------------------------------------------------
CREATE TABLE [dbo].[PostTestingRangeAnswer] (
  [Id] bigint NOT NULL,
  [Value] int NOT NULL,
  PRIMARY KEY ([Id]),
  CONSTRAINT [PrototypeRangeAnswerOption_PostTestingAnswer_FK10]
  FOREIGN KEY ([Id])
  REFERENCES [dbo].[PostTestingAnswer] ([Id])
  ON DELETE NO ACTION
  ON UPDATE NO ACTION
)

-- -----------------------------------------------------
-- Table [dbo].[ProfileAnswer]
-- -----------------------------------------------------
CREATE TABLE [dbo].[ProfileAnswer] (
  [Id] bigint IDENTITY NOT NULL,
  [QuestionId] bigint NOT NULL,
  [AnswerType] varchar(32) NOT NULL,
  [ParticipantId] bigint NOT NULL,
  PRIMARY KEY ([Id]),
  CONSTRAINT [ProfileAnswer_ProfileQuestion_FK00]
  FOREIGN KEY ([QuestionId])
  REFERENCES [dbo].[ProfileQuestion] ([Id])
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
  CONSTRAINT [ProfileAnswer_Participant]
  FOREIGN KEY ([ParticipantId])
  REFERENCES [dbo].[Participant] ([Id])
  ON DELETE NO ACTION
  ON UPDATE NO ACTION
)
CREATE INDEX [ProfileAnswer_ProfileQuestion_FK0_idx] ON [dbo].[ProfileAnswer] ([QuestionId] ASC);
CREATE INDEX [ProfileAnswer_Participant_idx] ON [dbo].[ProfileAnswer] ([ParticipantId] ASC);

-- -----------------------------------------------------
-- Table [dbo].[ProfileSingleAnswer]
-- -----------------------------------------------------
CREATE TABLE [dbo].[ProfileSingleAnswer] (
  [Id] bigint NOT NULL,
  [AnswerOptionId] bigint NOT NULL,
  PRIMARY KEY ([Id]),
  CONSTRAINT [ProfileSingleAnswer_ProfileAnswer_FK100]
  FOREIGN KEY ([Id])
  REFERENCES [dbo].[ProfileAnswer] ([Id])
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
  CONSTRAINT [PrototypeSingleAnswer_ProfileAnswerOption_FK101]
  FOREIGN KEY ([AnswerOptionId])
  REFERENCES [dbo].[ProfileAnswerOption] ([Id])
  ON DELETE NO ACTION
  ON UPDATE NO ACTION
)

-- -----------------------------------------------------
-- Table [dbo].[ProfileSingleInputAnswer]
-- -----------------------------------------------------
CREATE TABLE [dbo].[ProfileSingleInputAnswer] (
  [Id] bigint NOT NULL,
  [Input] varchar(128) NOT NULL,
  PRIMARY KEY ([Id]),
  CONSTRAINT [PrototypeSingleInputAnswer_ProfileSingleAnswer_FK00]
  FOREIGN KEY ([Id])
  REFERENCES [dbo].[ProfileSingleAnswer] ([Id])
  ON DELETE NO ACTION
  ON UPDATE NO ACTION
)

-- -----------------------------------------------------
-- Table [dbo].[ProfileRangeAnswer]
-- -----------------------------------------------------
CREATE TABLE [dbo].[ProfileRangeAnswer] (
  [Id] bigint NOT NULL,
  [Value] int NOT NULL,
  PRIMARY KEY ([Id]),
  CONSTRAINT [PrototypeRangeAnswerOption_ProfileAnswer_FK100]
  FOREIGN KEY ([Id])
  REFERENCES [dbo].[ProfileAnswer] ([Id])
  ON DELETE NO ACTION
  ON UPDATE NO ACTION
)

-- -----------------------------------------------------
-- Table [dbo].[User]
-- -----------------------------------------------------
CREATE TABLE [dbo].[Users] (
  [Id] bigint NOT NULL,
  [Username] varchar(64) NOT NULL,
  [UserpicPath] varchar(1024) NOT NULL,
  [Password] varchar(256) NOT NULL,
  [Name] varchar(500) NULL,
  PRIMARY KEY ([Id])
)

-- -----------------------------------------------------
-- Table [dbo].[UserRole]
-- -----------------------------------------------------
CREATE TABLE [dbo].[UserRole] (
  [UserId] bigint NOT NULL,
  [RoleId] bigint NOT NULL,
  PRIMARY KEY ([UserId], [RoleId]),
  CONSTRAINT [UserRole_Role_FK0]
  FOREIGN KEY ([RoleId])
  REFERENCES [dbo].[Role] ([Id])
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
  CONSTRAINT [UserRole_User_FK0]
  FOREIGN KEY ([UserId])
  REFERENCES [dbo].[Users] ([Id])
  ON DELETE NO ACTION
  ON UPDATE NO ACTION
)
CREATE INDEX [ParticipantRole_Role_FK_idx] ON [dbo].[UserRole] ([RoleId] ASC);

-- -----------------------------------------------------
-- Table [dbo].[QuickAccessSurvey]
-- -----------------------------------------------------
CREATE TABLE [dbo].[QuickAccessSurvey] (
  [Id] bigint IDENTITY NOT NULL,
  [UserId] bigint NOT NULL,
  [SurveyId] bigint NOT NULL,
  [Position] int NOT NULL,
  PRIMARY KEY ([Id])
)

-- -----------------------------------------------------
-- Table [dbo].[PrototypeQuestionCondition]
-- -----------------------------------------------------
CREATE TABLE [dbo].[PrototypeQuestionCondition] (
  [Id] bigint NOT NULL,
  [QuestionId] bigint NOT NULL,
  [ConditionalOnQuestionId] bigint NOT NULL,
  [ConditionalOnAnswerId] bigint NOT NULL,
  PRIMARY KEY ([Id])
)

-- -----------------------------------------------------
-- Table [dbo].[PostTestingQuestionCondition]
-- -----------------------------------------------------
CREATE TABLE [dbo].[PostTestingQuestionCondition] (
  [Id] bigint NOT NULL,
  [QuestionId] bigint NOT NULL,
  [ConditionalOnQuestionId] bigint NOT NULL,
  [ConditionalOnAnswerId] bigint NOT NULL,
  PRIMARY KEY ([Id])
)

-- -----------------------------------------------------
-- Table [dbo].[ProfileQuestionCondition]
-- -----------------------------------------------------
CREATE TABLE [dbo].[ProfileQuestionCondition] (
  [Id] bigint NOT NULL,
  [QuestionId] bigint NOT NULL,
  [ConditionalOnQuestionId] bigint NOT NULL,
  [ConditionalOnAnswerId] bigint NOT NULL,
  PRIMARY KEY ([Id]),
  CONSTRAINT [ProfileQuestionCondition_ProfileQuestionId]
  FOREIGN KEY ([QuestionId])
  REFERENCES [dbo].[ProfileQuestion] ([Id])
  ON DELETE NO ACTION
  ON UPDATE NO ACTION
)
CREATE INDEX [ProfileQuestionCondition_ProfileQuestionId_idx] ON [dbo].[ProfileQuestionCondition] ([QuestionId] ASC);