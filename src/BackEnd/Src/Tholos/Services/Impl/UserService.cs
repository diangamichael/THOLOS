/*
*  Copyright (c) 2015, TopCoder, Inc. All rights reserved.
*
*/

using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Web;
using Tholos.Entities;

namespace Tholos.Services.Impl
{
    /// <summary>
    /// <para>
    /// This class is the realization of the <see cref="IUserService"/> service contract.
    /// It handles all the user related operations.
    /// </para>
    /// <para>
    /// Changes in version 1.1 (THOLOS - Integrate Prototype with Backend APIs):
    /// <list type="bullet">
    /// <item>
    /// <description>Changed to returns roles when getting user.</description>
    /// </item>
    /// </list>
    /// </para>
    /// </summary>
    ///
    /// <threadsafety>
    /// This class is mutable (base class is mutable) but effectively thread-safe.
    /// </threadsafety>
    /// 
    /// <author>veshu</author>
    /// <author>duxiaoyang</author>
    ///
    /// <version>1.1</version>
    /// <since>1.0</since>
    /// <copyright>Copyright (c) 2015, TopCoder, Inc. All rights reserved.</copyright>
    [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple,
        InstanceContextMode = InstanceContextMode.PerSession)]
    public class UserService : BasePersistenceService, IUserService
    {
        /// <summary>
        /// Represents the name of the stored procedure to retrieve user by Username.
        /// </summary>
        private const string GetUserByUsernameSpName = "spGetUserByUsername";

        /// <summary>
        /// Represents the name of the stored procedure to retrieve user roles.
        /// </summary>
        private const string GetUserRolesSpName = "spGetUserRoles";

        /// <summary>
        /// <para>
        /// Initializes a new instance of the <see cref="UserService"/> class.
        /// </para>
        /// </summary>
        public UserService()
        {
        }

        /// <summary>
        /// <para>
        /// Get Users by given user name.
        /// </para>
        /// </summary>
        /// <param name="username">The username.</param>
        /// <returns>The user.</returns>
        /// 
        /// <exception cref="WebFaultException{T}">
        /// If any error occurred <see cref="ServiceFaultDetail"/> will be created from:
        /// <para>
        /// <list type="bullet">
        /// <item><see cref="ArgumentNullException"/> If <paramref name="username"/> is <c>null</c>.</item>
        /// <item><see cref="ArgumentException"/> If <paramref name="username"/> is empty.</item>
        /// <item><see cref="PersistenceException"/> If a DB-based error occurs.</item>
        /// <item><see cref="TholosException"/> If any other errors occur
        /// while performing this operation.</item>
        /// </list>
        /// </para>
        /// </exception>
        [OperationBehavior(TransactionScopeRequired = false)]
        public User GetUser(string username)
        {
            return Helper.LoggingWrapper(Logger,
                () =>
                {
                    Helper.CheckNotNullOrEmpty(username, "username");
                    using (var conn = GetConnection())
                    {
                        var user = Helper.ExecuteRetrieveSp(conn, GetUserByUsernameSpName,
                            Helper.ReaderUser, new Dictionary<string, object>
                            {
                                {"Username", username}
                            });
                        if (user != null)
                        {
                            user.Roles = Helper.ExecuteRetrieveSp(conn, GetUserRolesSpName,
                                Helper.ReaderRoles, new Dictionary<string, object>
                                {
                                    {"Username", username}
                                });
                        }
                        return user;
                    }
                }, username);
        }
    }
}

