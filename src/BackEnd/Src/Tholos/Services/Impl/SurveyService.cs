/*
*  Copyright (c) 2015, TopCoder, Inc. All rights reserved.
*
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Security.Permissions;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Web.Security;
using Microsoft.Practices.Unity;
using Tholos.Entities;
using IsolationLevel = System.Transactions.IsolationLevel;

namespace Tholos.Services.Impl
{

    /// <summary>
    /// <para>
    /// This class is the realization of the <see cref="ISurveyService"/> service contract.
    /// It handles all the survey related manager operations.
    /// </para>
    /// <para>
    /// Changes in version 1.1 (THOLOS - Integrate Prototype with Backend APIs):
    /// <list type="bullet">
    /// <item>
    /// <description>Added a new parameter when getting prototype codes.</description>
    /// </item>
    /// </list>
    /// </para>
    /// </summary>
    ///
    /// <threadsafety>
    /// This class is mutable (base class is mutable) but effectively thread-safe.
    /// </threadsafety>
    /// 
    /// <author>veshu</author>
    /// <author>duxiaoyang</author>
    ///
    /// <version>1.1</version>
    /// <since>1.0</since>
    /// <copyright>Copyright (c) 2015, TopCoder, Inc. All rights reserved.</copyright>
    [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple,
        InstanceContextMode = InstanceContextMode.PerSession,
        ReleaseServiceInstanceOnTransactionComplete = false,
        TransactionIsolationLevel = IsolationLevel.RepeatableRead)]
    public class SurveyService : BasePersistenceService, ISurveyService
    {
        #region used stored procedure names
        /// <summary>
        /// Represents the name of the stored procedure to retrieve all prototype questions.
        /// </summary>
        private const string GetAllPrototypeQuestionsSpName = "spGetAllPrototypeQuestions";

        /// <summary>
        /// Represents the name of the stored procedure to retrieve all post testing questions.
        /// </summary>
        private const string GetAllPostTestingQuestionsSpName = "spGetAllPostTestingQuestions";

        /// <summary>
        /// Represents the name of the stored procedure to create survey.
        /// </summary>
        private const string CreateSurveySpName = "spCreateSurvey";

        /// <summary>
        /// Represents the name of the stored procedure to create prototype questions.
        /// </summary>
        private const string CreateSurveyPrototypeQuestionSpName = "spCreateSurveyPrototypeQuestion";

        /// <summary>
        /// Represents the name of the stored procedure to create PostTesting questions.
        /// </summary>
        private const string CreateSurveyPostTestingQuestionSpName = "spCreateSurveyPostTestingQuestion";

        /// <summary>
        /// Represents the name of the stored procedure to create prototype code.
        /// </summary>
        private const string CreatePrototypeCodeSpName = "spCreatePrototypeCode";

        /// <summary>
        /// Represents the name of the stored procedure to create participant credentials.
        /// </summary>
        private const string CreateParticipantCredentialsSpName = "spCreateParticipantCredentials";

        /// <summary>
        /// Represents the name of the stored procedure to update survey status name.
        /// </summary>
        private const string UpdateSurveyStatusSpName = "spUpdateSurveyStatus";

        /// <summary>
        /// Represents the name of the stored procedure to delete survey prototype questions by survey id.
        /// </summary>
        private const string DeleteSurveyPrototypeQuestionsBySurveyIdSpName =
            "spDeleteSurveyPrototypeQuestionsBySurveyId";

        /// <summary>
        /// Represents the name of the stored procedure to delete survey PostTesting questions by survey id.
        /// </summary>
        private const string DeleteSurveyPostTestingQuestionsBySurveyIdSpName
            = "spDeleteSurveyPostTestingQuestionsBySurveyId";

        /// <summary>
        /// Represents the name of the stored procedure to delete PrototypeCodes by survey id.
        /// </summary>
        private const string DeletePrototypeCodesBySurveyIdSpName = "spDeletePrototypeCodesBySurveyId";

        /// <summary>
        /// Represents the name of the stored procedure to update survey.
        /// </summary>
        private const string UpdateSurveySpName = "spUpdateSurvey";

        /// <summary>
        /// Represents the name of the stored procedure to get survey.
        /// </summary>
        private const string GetSurveySpName = "spGetSurvey";

        /// <summary>
        /// Represents the name of the stored procedure to get survey Prototype Questions by survery id.
        /// </summary>
        private const string GetSurveyPrototypeQuestionsBySurveyIdSpName = "spGetSurveyPrototypeQuestionsBySurveyId";

        /// <summary>
        /// Represents the name of the stored procedure to get survey PostTesting Questions by survery id.
        /// </summary>
        private const string GetSurveyPostTestingQuestionsBySurveyIdSpName =
            "spGetSurveyPostTestingQuestionsBySurveyId";

        /// <summary>
        /// Represents the name of the stored procedure to search survey.
        /// </summary>
        private const string SearchSurveysSpName = "spSearchSurveys";

        /// <summary>
        /// Represents the name of the stored procedure to get all survey.
        /// </summary>
        private const string GetAllSurveysSpName = "spGetAllSurveys";

        /// <summary>
        /// Represents the name of the stored procedure to get user survey status.
        /// </summary>
        private const string GetUserSurveyStatusSpName = "spGetUserSurveyStatus";

        /// <summary>
        /// Represents the name of the stored procedure to get quick access survey by user id.
        /// </summary>
        private const string GetQuickAccessSurveysByUserIdSpName = "spGetQuickAccessSurveysByUserId";

        /// <summary>
        /// Represents the name of the stored procedure to deletes quick access survey by user id.
        /// </summary>
        private const string DeleteQuickAccessSurveysByUserIdSpName = "spDeleteQuickAccessSurveysByUserId";

        /// <summary>
        /// Represents the name of the stored procedure to creates quick access survey.
        /// </summary>
        private const string CreateQuickAccessSurveySpName = "spCreateQuickAccessSurvey";

        /// <summary>
        /// Represents the name of the stored procedure to get participant credentials.
        /// </summary>
        private const string GetParticipantCredentialsSpName = "spGetParticipantCredentials";

        /// <summary>
        /// Represents the name of the stored procedure to get participant surveys by survey id.
        /// </summary>
        private const string GetParticipantSurveysBySurveyIdSpName = "spGetParticipantSurveysBySurveyId";

        /// <summary>
        /// Represents the name of the stored procedure to get prototype tests by participant survey id.
        /// </summary>
        private const string GetPrototypeTestsByParticipantSurveyIdSpName
            = "spGetPrototypeTestsByParticipantSurveyId";
        #endregion

        #region properties
        /// <summary>
        /// <para>
        /// Represents the participant username length.
        /// </para>
        /// </summary>
        /// <value>The participant username length. It should be positive, defaults to 8.</value>
        private int _participantUsernameLength = 8;

        /// <summary>
        /// <para>
        /// Represents the participant password length.
        /// </para>
        /// </summary>
        /// <value>The participant password length. It should be positive, defaults to 8.</value>
        private int _participantPasswordLength = 16;

        /// <summary>
        /// Represents the valid sort column for survey.
        /// </summary>
        private readonly string[] _validSortColumn =
                  {
                      "Id", "Name", "CellName", "ParticipantsNumber", "Status", "DateCreated",
                      "CompletedSurveysNumber", "Draft"
                  };

        /// <summary>
        /// <para>
        /// Gets or sets the participant username length.
        /// </para>
        /// </summary>
        /// <remarks>It is used to generate the credentials.</remarks>
        /// <value>The participant username length. It should be positive, defaults to 8.</value>
        [Dependency("ParticipantUsernameLength")]
        public int ParticipantUsernameLength
        {
            get { return _participantUsernameLength; }
            set { _participantUsernameLength = value; }
        }

        /// <summary>
        /// <para>
        /// Gets or sets the participant password length.
        /// </para>
        /// </summary>
        /// <remarks>It is used to generate the credentials.</remarks>
        /// <value>The participant password length. It should be positive, defaults to 16.</value>
        [Dependency("ParticipantPasswordLength")]
        public int ParticipantPasswordLength
        {

            get { return _participantPasswordLength; }
            set { _participantPasswordLength = value; }
        }

        /// <summary>
        /// <para>
        /// Gets or sets the export service.
        /// </para>
        /// </summary>
        /// <value>
        /// The export service instance. It should not be null after initialization through Unity injection. 
        /// </value>
        [Dependency]
        public IExportService ExportService
        {
            get;
            set;
        }

        #endregion

        /// <summary>
        /// <para>
        /// Initializes a new instance of the <see cref="SurveyService"/> class.
        /// </para>
        /// </summary>
        public SurveyService()
        {
        }

        /// <summary>
        /// <para>
        /// Gets the prototype questions fully populated with the answer options.
        /// </para>
        /// </summary>
        /// <returns>The prototype questions.</returns>
        /// 
        /// <exception cref="WebFaultException{T}">
        /// If any error occurred <see cref="ServiceFaultDetail"/> will be created from:
        /// <para>
        /// <list type="bullet">
        /// <item><see cref="PersistenceException"/> If a DB-based error occurs.</item>
        /// <item><see cref="TholosException"/> If any other errors occur while performing this operation.</item>
        /// </list>
        /// </para>
        /// </exception>
        [OperationBehavior(TransactionScopeRequired = false)]
        [PrincipalPermission(SecurityAction.Demand, Role = "Manager")]
        public IList<Question> GetAllPrototypeQuestions()
        {
            return Helper.LoggingWrapper(Logger, () =>
            {
                var conn = GetConnection();
                var questions = Helper.ExecuteRetrieveSp(conn, GetAllPrototypeQuestionsSpName,
                      Helper.ReaderQuestionsList);

                foreach (var question in questions)
                {
                    question.AnswerOptions = Helper.GetAnswerOptionsByQuestionId(conn,
                       Helper.GetPrototypeAnswerOptionsByQuestionIdSpName, question.Id);
                }
                
                if (conn != null)
                {
                    CloseConnection(conn);
                }
                return questions;
            });
        }
        
        /// <summary>
        /// <para>
        /// Gets the post testing questions fully populated with the answer options.
        /// </para>
        /// </summary>
        /// <returns>The post testing questions.</returns>
        /// 
        /// <exception cref="WebFaultException{ServiceFaultDetail}">
        /// If any error occurred <see cref="ServiceFaultDetail"/> will be created from:
        /// <para>
        /// <list type="bullet">
        /// <item><see cref="PersistenceException"/> If a DB-based error occurs.</item>
        /// <item><see cref="TholosException"/> If any other errors occur while performing this operation.</item>
        /// </list>
        /// </para>
        /// </exception>
        [OperationBehavior(TransactionScopeRequired = false)]
        [PrincipalPermission(SecurityAction.Demand, Role = "Manager")]
        public IList<Question> GetAllPostTestingQuestions()
        {
            return Helper.LoggingWrapper(Logger, () =>
            {
                var conn = GetConnection();
                var questions = Helper.ExecuteRetrieveSp(conn, GetAllPostTestingQuestionsSpName,
                      Helper.ReaderQuestionsList);

                foreach (var question in questions)
                {
                    question.AnswerOptions = Helper.GetAnswerOptionsByQuestionId(conn,
                        Helper.GetPostTestingAnswerOptionsByQuestionIdSpName, question.Id);
                }
                
                if (conn != null)
                {
                    CloseConnection(conn);
                }
                return questions;
            });
        }

        /// <summary>
        /// <para>
        /// Creates the survey.
        /// </para>
        /// </summary>
        /// <remarks><para>Generates the credentials for the parcipants and persist it.
        /// It is supposed that only question ids are passed for the survey's prototype and post testing questions.
        /// </para></remarks>
        /// 
        /// <param name="survey">The survey.</param>
        /// <param name="userId">The id of the manager who creates the survey.</param>
        /// <returns>The id of created survey.</returns>
        /// 
        /// <exception cref="WebFaultException{ServiceFaultDetail}">
        /// If any error occurred <see cref="ServiceFaultDetail"/> will be created from:
        /// <para>
        /// <list type="bullet">
        /// <item><see cref="ArgumentNullException"/> If <paramref name="survey"/> is <c>null</c>.</item>
        /// <item><see cref="ArgumentException"/> If <paramref name="userId"/> is negative.</item>
        /// <item><see cref="PersistenceException"/> If a DB-based error occurs.</item>
        /// <item><see cref="TholosException"/> If any other errors occur while performing this operation.</item>
        /// </list>
        /// </para>
        /// </exception>
        [OperationBehavior(TransactionScopeRequired = true, TransactionAutoComplete = true)]
        [PrincipalPermission(SecurityAction.Demand, Role = "Manager")]
        public long CreateSurvey(Survey survey, long userId)
        {
            return Helper.LoggingWrapper(Logger, () =>
            {
                Helper.CheckPositive(userId, "userId");
                using (var conn = GetConnection())
                {
                    Helper.CheckExistence("Users", userId, conn);

                    Helper.ValidateSurveyEntity(survey, true);
                    var dic = BuildCommonSurveyParameter(survey);
                    dic.Add(new KeyValuePair<string, object>("ParticipantsNumber", survey.ParticipantsNumber));
                    dic.Add(new KeyValuePair<string, object>("CreatedBy", userId));

                    var surveyId = Helper.ExecuteSpWithReturnOutParam(conn, CreateSurveySpName, dic, "Id");

                    if (surveyId <= 0)
                    {
                        throw new Exception("Error occurred while creating survey.");
                    }
                    if (survey.PrototypeQuestions != null)
                    {
                        // save prototype questions
                        CreateQuestions(conn, surveyId, CreateSurveyPrototypeQuestionSpName,
                            survey.PrototypeQuestions);
                    }
                    if (survey.PostTestingQuestions != null)
                    {
                        // save post testing questions
                        CreateQuestions(conn, surveyId, CreateSurveyPostTestingQuestionSpName,
                            survey.PostTestingQuestions);
                    }
                    if (survey.PrototypeCodes != null)
                    {
                        // save prototype codes
                        CreatePrototypeCodes(conn, survey.PrototypeCodes, surveyId);
                    }
                    // generate participant credentials
                    var credentials = GenerareCredentials(survey.ParticipantsNumber);

                    // save credentials
                    foreach (var credential in credentials)
                    {
                        Helper.ExecuteSpWithReturnOutParam(conn, CreateParticipantCredentialsSpName,
                            new Dictionary<string, object>
                            {
                                {"SurveyId", surveyId},
                                {"Username", credential.Username},
                                {"Password", credential.Password}
                            }, "Id");
                    }
                    return surveyId;
                }

            }, survey, userId);
        }

        /// <summary>
        /// <para>
        /// Publishes the survey. Updates the survey status and draft flag.
        /// </para>
        /// </summary>
        /// 
        /// <param name="surveyId">The survey id.</param>
        /// 
        /// <exception cref="WebFaultException{ServiceFaultDetail}">
        /// If any error occurred <see cref="ServiceFaultDetail"/> will be created from:
        /// <para>
        /// <list type="bullet">
        /// <item><see cref="ArgumentException"/> If <paramref name="surveyId"/> is not valid positive number.</item>
        /// <item><see cref="EntityNotFoundException"/>
        /// If If the entity does not exist with given <paramref name="surveyId"/>.</item>
        /// <item><see cref="PersistenceException"/> If a DB-based error occurs.</item>
        /// <item><see cref="TholosException"/> If any other errors occur while performing this operation.</item>
        /// </list>
        /// </para>
        /// </exception>
        [OperationBehavior(TransactionScopeRequired = true, TransactionAutoComplete = true)]
        [PrincipalPermission(SecurityAction.Demand, Role = "Manager")]
        public void PublishSurvey(string surveyId)
        {
            Helper.LoggingWrapper(Logger, () =>
            {
                var id = Helper.CheckAndGetValidNumberAndPositive(surveyId, "surveyId");

                UpdateSurveyStatusOnly(id, SurveyStatus.Published);

            }, surveyId);
        }

        /// <summary>
        /// <para>
        /// Updates the survey. All dependent entities are updated.
        /// </para>
        /// </summary>
        /// 
        /// <param name="survey">The survey.</param>
        /// 
        /// <exception cref="WebFaultException{ServiceFaultDetail}">
        /// If any error occurred <see cref="ServiceFaultDetail"/> will be created from:
        /// <para>
        /// <list type="bullet">
        /// <item><see cref="ArgumentNullException"/> If <paramref name="survey"/> is <c>null</c>.</item>
        /// <item><see cref="EntityNotFoundException"/>
        /// If If the entity does not exist with given <paramref name="survey"/>.</item>
        /// <item><see cref="PersistenceException"/> If a DB-based error occurs.</item>
        /// <item><see cref="TholosException"/> If any other errors occur while performing this operation.</item>
        /// </list>
        /// </para>
        /// </exception>
        [OperationBehavior(TransactionScopeRequired = true, TransactionAutoComplete = true)]
        [PrincipalPermission(SecurityAction.Demand, Role = "Manager")]
        public void UpdateSurvey(Survey survey)
        {
            Helper.LoggingWrapper(Logger, () =>
            {
                Helper.ValidateSurveyEntity(survey, true);

                using (var conn = GetConnection())
                {
                    var dic = BuildCommonSurveyParameter(survey);
                    dic.Add(new KeyValuePair<string, object>("Id", survey.Id));
                    // update main entity
                    Helper.ExecuteUpdateOrDeleteSp<Survey>(conn, UpdateSurveySpName, "Id=" + survey.Id
                        , dic);

                    // delete child table entity
                    var param = BuildParameteDicrForSurveyId(survey.Id);

                    Helper.ExecuteSp(conn, DeleteSurveyPrototypeQuestionsBySurveyIdSpName, param);

                    Helper.ExecuteSp(conn, DeleteSurveyPostTestingQuestionsBySurveyIdSpName, param);

                    Helper.ExecuteSp(conn, DeletePrototypeCodesBySurveyIdSpName, param);

                    // save prototype questions
                    CreateQuestions(conn, survey.Id, CreateSurveyPrototypeQuestionSpName,
                        survey.PrototypeQuestions);

                    // save post testing questions
                    CreateQuestions(conn, survey.Id, CreateSurveyPostTestingQuestionSpName,
                        survey.PostTestingQuestions);

                    // save prototype codes
                    CreatePrototypeCodes(conn, survey.PrototypeCodes, survey.Id);
                }

            }, survey);
        }

        /// <summary>
        /// <para>
        /// Gets the survey fully populated with the questions and answer options.
        /// </para>
        /// </summary>
        /// 
        /// <param name="surveyId">The survey id.</param>
        /// <returns>The survey.</returns>
        /// 
        /// <exception cref="WebFaultException{ServiceFaultDetail}">
        /// If any error occurred <see cref="ServiceFaultDetail"/> will be created from:
        /// <para>
        /// <list type="bullet">
        /// <item><see cref="ArgumentException"/> If <paramref name="surveyId"/> is not valid positive number.</item>
        /// <item><see cref="PersistenceException"/> If a DB-based error occurs.</item>
        /// <item><see cref="TholosException"/> If any other errors occur while performing this operation.</item>
        /// </list>
        /// </para>
        /// </exception>
        [OperationBehavior(TransactionScopeRequired = false)]
        [PrincipalPermission(SecurityAction.Demand, Role = "Manager")]
        public Survey GetSurvey(string surveyId)
        {
            return Helper.LoggingWrapper(Logger, () =>
            {
                var id = Helper.CheckAndGetValidNumberAndPositive(surveyId, "surveyId");
                using (var conn = GetConnection())
                {
                    var parameters = BuildParameteDicrForSurveyId(id);

                    var survey = Helper.ExecuteRetrieveSp(conn, GetSurveySpName, Helper.ReaderSurvey, parameters);

                    if (survey != null)
                    {
                        survey.PrototypeQuestions = Helper.ExecuteRetrieveSp(conn
                            , GetSurveyPrototypeQuestionsBySurveyIdSpName, Helper.ReaderSurveyQuestionsList,
                            parameters);

                        survey.PostTestingQuestions = Helper.ExecuteRetrieveSp(conn
                            , GetSurveyPostTestingQuestionsBySurveyIdSpName, Helper.ReaderSurveyQuestionsList,
                            parameters);

                        var parameters1 = new Dictionary<string, object>(parameters);
                        parameters1.Add("Status", null);
                        survey.PrototypeCodes = Helper.ExecuteRetrieveSp(conn
                            , Helper.GetPrototypeCodesBySurveyIdSpName, Helper.ReaderPrototypeCode, parameters1);

                        foreach (var question in survey.PrototypeQuestions)
                        {
                            question.Question.AnswerOptions = Helper.GetAnswerOptionsByQuestionId(conn,
                                Helper.GetPrototypeAnswerOptionsByQuestionIdSpName, question.Question.Id);
                        }
                        foreach (var question in survey.PostTestingQuestions)
                        {
                            question.Question.AnswerOptions = Helper.GetAnswerOptionsByQuestionId(conn,
                                Helper.GetPostTestingAnswerOptionsByQuestionIdSpName, question.Question.Id);
                        }

                        IList<Question> sortedPostTestingQuestions = Helper.LabelAndSort(survey.PostTestingQuestions.Select(item => item.Question).ToList()).ToList();
                        IList<Question> sortedPrototypeQuestions = Helper.LabelAndSort(survey.PrototypeQuestions.Select(item => item.Question).ToList()).ToList();

                        survey.PostTestingQuestions = new List<IndexedQuestion>();
                        for (int i = 0; i < sortedPostTestingQuestions.Count; ++i)
                        {
                            survey.PostTestingQuestions.Add(new IndexedQuestion()
                            {
                                Position = i + 1,
                                Question = sortedPostTestingQuestions[i]
                            });
                        }

                        survey.PrototypeQuestions = new List<IndexedQuestion>();
                        for (int i = 0; i < sortedPrototypeQuestions.Count; ++i)
                        {
                            survey.PrototypeQuestions.Add(new IndexedQuestion()
                            {
                                Position = i + 1,
                                Question = sortedPrototypeQuestions[i]
                            });
                        }
                    }
                    return survey;
                }
            }, surveyId);
        }

        /// <summary>
        /// <para>
        /// Updates the survey status.
        /// </para>
        /// </summary>
        /// 
        /// <param name="surveyId">The survey id.</param>
        /// <param name="status">The survey status.</param>
        /// 
        /// <exception cref="WebFaultException{ServiceFaultDetail}">
        /// If any error occurred <see cref="ServiceFaultDetail"/> will be created from:
        /// <para>
        /// <list type="bullet">
        /// <item><see cref="ArgumentNullException"/> If <paramref name="status"/> or <paramref name="surveyId"/>
        ///  is <c>null</c>.</item>
        /// <item><see cref="ArgumentException"/> If <paramref name="surveyId"/> is not valid positive number.</item>
        /// <item><see cref="EntityNotFoundException"/>
        /// If If the entity does not exist with given <paramref name="surveyId"/>.</item>
        /// <item><see cref="PersistenceException"/> If a DB-based error occurs.</item>
        /// <item><see cref="TholosException"/> If any other errors occur while performing this operation.</item>
        /// </list>
        /// </para>
        /// </exception>
        [OperationBehavior(TransactionScopeRequired = true, TransactionAutoComplete = true)]
        [PrincipalPermission(SecurityAction.Demand, Role = "Manager")]
        public void UpdateSurveyStatus(string surveyId, SurveyStatus status)
        {
            Helper.LoggingWrapper(Logger, () =>
            {
                var id = Helper.CheckAndGetValidNumberAndPositive(surveyId, "surveyId");
                Helper.CheckNotNull(status, "status");
                UpdateSurveyStatusOnly(id, status, status == SurveyStatus.Draft ? true : false);

            }, surveyId, status);
        }

        /// <summary>
        /// <para>
        /// Searches the surveys. Survey quetsions are not loaded.
        /// </para>
        /// </summary>
        /// 
        /// <param name="name">The survey name.</param>
        /// <param name="filter">The search criteria.</param>
        /// <returns>The survery search results.</returns>
        /// 
        /// <exception cref="WebFaultException{ServiceFaultDetail}">
        /// If any error occurred <see cref="ServiceFaultDetail"/> will be created from:
        /// <para>
        /// <list type="bullet">
        /// <item><see cref="ArgumentNullException"/> If <paramref name="name"/> or <paramref name="filter"/> 
        /// is <c>null</c>.</item>
        /// <item><see cref="ArgumentException"/> If <paramref name="name"/> is empty.</item>
        /// <item><see cref="PersistenceException"/> If a DB-based error occurs.</item>
        /// <item><see cref="TholosException"/> If any other errors occur while performing this operation.</item>
        /// </list>
        /// </para>
        /// </exception>
        [OperationBehavior(TransactionScopeRequired = false)]
        [PrincipalPermission(SecurityAction.Demand, Role = "Manager")]
        public SurveySearchResult SearchSurvey(string name, SearchFilter filter)
        {
            return Helper.LoggingWrapper(Logger, () =>
              {
                  if (string.IsNullOrEmpty(name))
                  {
                      name = "";
                  }



                  Helper.CheckSearchCriteria(filter, _validSortColumn);

                  var filterParameter = new Dictionary<string, object>
                {
                    {"Name", name},
                    {"PageSize", filter.PageSize},
                    {"PageNumber", filter.PageNumber},
                    {"SortColumn", filter.SortColumn},
                    {"SortAscending", filter.SortAscending == true}
                };
                  int totalRecords;
                  var items = GetSearchSurveys(filterParameter, SearchSurveysSpName, out totalRecords);

                  return PrepareSearchResult(totalRecords, filter, items);
              }, name, filter);
        }

        /// <summary>
        /// <para>
        /// Get all surveys. Only data from the Survey table is loaded, questions are not loaded.
        /// </para>
        /// </summary>
        /// 
        /// <param name="filter">The search criteria.</param>
        /// <returns>The surveys.</returns>
        /// 
        /// <exception cref="WebFaultException{ServiceFaultDetail}">
        /// If any error occurred <see cref="ServiceFaultDetail"/> will be created from:
        /// <para>
        /// <list type="bullet">
        /// <item><see cref="ArgumentNullException"/> If <paramref name="filter"/>  is <c>null</c>.</item>
        /// <item><see cref="ArgumentException"/> If <paramref name="filter"/> is invalid.</item>
        /// <item><see cref="PersistenceException"/> If a DB-based error occurs.</item>
        /// <item><see cref="TholosException"/> If any other errors occur while performing this operation.</item>
        /// </list>
        /// </para>
        /// </exception>
        [OperationBehavior(TransactionScopeRequired = false)]
        [PrincipalPermission(SecurityAction.Demand, Role = "Manager")]
        public AllSurveysSearchResult GetAllSurveys(SearchFilter filter)
        {
            return Helper.LoggingWrapper(Logger, () =>
            {
                Helper.CheckSearchCriteria(filter, _validSortColumn);

                var filterParameter = new Dictionary<string, object>
                {
                    {"PageSize", filter.PageSize},
                    {"PageNumber", filter.PageNumber},
                    {"SortColumn", filter.SortColumn},
                    {"SortAscending", filter.SortAscending == true}
                };

                int totalRecords;
                var items = GetSearchSurveys(filterParameter, GetAllSurveysSpName, out totalRecords);

                int totalPages = 0;
                if (totalRecords > 0)
                {
                    totalPages = filter.PageNumber == 0 ? 1 : (totalRecords - 1) / filter.PageSize + 1;
                }
                return new AllSurveysSearchResult
                {
                    TotalRecordCount = totalRecords,
                    TotalPageCount = totalPages,
                    Records = items.GroupBy(x => x.Status)
                        .ToDictionary(item => item.Key, item => item.ToList())
                };
            }, filter);
        }

        /// <summary>
        /// <para>
        /// Gets the number of surveys for each survey status for the specific user.
        /// </para>
        /// </summary>
        /// 
        /// <param name="userId">The userId.</param>
        /// <returns>The user survey status.</returns>
        /// 
        /// <exception cref="WebFaultException{ServiceFaultDetail}">
        /// If any error occurred <see cref="ServiceFaultDetail"/> will be created from:
        /// <para>
        /// <list type="bullet">
        /// <item><see cref="ArgumentException"/> If <paramref name="userId"/> is not valid positive number.</item>
        /// <item><see cref="EntityNotFoundException"/> If the user does not exist.</item>
        /// <item><see cref="PersistenceException"/> If a DB-based error occurs.</item>
        /// <item><see cref="TholosException"/> If any other errors occur while performing this operation.</item>
        /// </list>
        /// </para>
        /// </exception>
        [OperationBehavior(TransactionScopeRequired = false)]
        [PrincipalPermission(SecurityAction.Demand, Role = "Manager")]
        public IDictionary<SurveyStatus, int> GetUserSurveyStatus(string userId)
        {
            return Helper.LoggingWrapper(Logger, () =>
            {
                var id = Helper.CheckAndGetValidNumberAndPositive(userId, "userId");
                using (var conn = GetConnection())
                {
                    Helper.CheckExistence("Users", id, conn);
                    var statuses = Helper.ExecuteRetrieveSp(conn, GetUserSurveyStatusSpName,
                        reader => Helper.ReadStringList(reader),
                        new Dictionary<string, object>
                        {
                            {"UserId", id}
                        });
                    return
                        statuses.GroupBy(status => status)
                            .ToDictionary(
                                item => (SurveyStatus)Enum.Parse(typeof(SurveyStatus), item.First(), true),
                                item => item.Count());
                }
            }, userId);
        }

        /// <summary>
        /// <para>
        /// Gets the quick access surveys. Surveys are not fully populated, only id, name and cell name are fetched.
        /// </para>
        /// </summary>
        /// 
        /// <param name="userId">The userId.</param>
        /// <returns>
        /// The quick access surveys in the dictionary where the keys are the positions in the quick access list.
        /// </returns>
        /// 
        /// <exception cref="WebFaultException{ServiceFaultDetail}">
        /// If any error occurred <see cref="ServiceFaultDetail"/> will be created from:
        /// <para>
        /// <list type="bullet">
        /// <item><see cref="ArgumentException"/> If <paramref name="userId"/> is not valid positive number.</item>
        /// <item><see cref="EntityNotFoundException"/> If the user does not exist.</item>
        /// <item><see cref="PersistenceException"/> If a DB-based error occurs.</item>
        /// <item><see cref="TholosException"/> If any other errors occur while performing this operation.</item>
        /// </list>
        /// </para>
        /// </exception>
        [OperationBehavior(TransactionScopeRequired = false)]
        [PrincipalPermission(SecurityAction.Demand, Role = "Manager")]
        public IList<QuickAccessSurvey> GetQuickAccessSurveys(string userId)
        {
            return Helper.LoggingWrapper(Logger, () =>
            {
                var id = Helper.CheckAndGetValidNumberAndPositive(userId, "userId");

                var conn = GetConnection();
                Helper.CheckExistence("Users", id, conn);

                var results = Helper.ExecuteRetrieveSp(conn, GetQuickAccessSurveysByUserIdSpName,
                      Helper.ReaderQuickAccessSurveyList, new Dictionary<string, object>
                      {
                          {"userId", userId}
                      });
                if (conn != null)
                {
                    CloseConnection(conn);
                }
                return results;
            }, userId);
        }

        /// <summary>
        /// <para>
        /// Updates the quick access survey.
        /// </para>
        /// </summary>
        /// <param name="userId">The userId.</param>
        /// <param name="surveys">The mapping between position in the list and survey id.</param>
        /// <exception cref="WebFaultException{ServiceFaultDetail}">
        /// If any error occurred <see cref="ServiceFaultDetail"/> will be created from:
        /// <para>
        /// <list type="bullet">
        /// <item><see cref="ArgumentNullException"/> If <paramref name="surveys"/> or <paramref name="userId"/> 
        /// is <c>null</c>.</item>
        /// <item><see cref="ArgumentException"/> If <paramref name="userId"/> is not valid positive number.</item>
        /// <item><see cref="EntityNotFoundException"/> If the user does not exist.</item>
        /// <item><see cref="PersistenceException"/> If a DB-based error occurs.</item>
        /// <item><see cref="TholosException"/> If any other errors occur while performing this operation.</item>
        /// </list> </para> </exception>
        [OperationBehavior(TransactionScopeRequired = true, TransactionAutoComplete = true)]
        [PrincipalPermission(SecurityAction.Demand, Role = "Manager")]
        public void UpdateQuickAccessSurveys(string userId, IList<QuickAccessSurvey> surveys)
        {
            Helper.LoggingWrapper(Logger, () =>
            {
                var id = Helper.CheckAndGetValidNumberAndPositive(userId, "userId");
                Helper.CheckNotNull(surveys, "surveys");
                foreach (var survey in surveys)
                {
                    Helper.CheckPositive(survey.SurveyId, "survey.SurveyId");
                    Helper.CheckPositive(survey.Position, "survey.Position");
                }
                using (var conn = GetConnection())
                {
                    Helper.CheckExistence("Users", id, conn);

                    var dic = new Dictionary<string, object>
                    {
                        {"userId", id}
                    };
                    Helper.ExecuteSp(conn, DeleteQuickAccessSurveysByUserIdSpName, dic);

                    foreach (var survey in surveys)
                    {
                        Helper.ExecuteSpWithReturnOutParam(conn, CreateQuickAccessSurveySpName,
                            new Dictionary<string, object>
                            {
                                {"SurveyId", survey.SurveyId},
                                {"UserId", id},
                                {"Position", survey.Position}
                            }, "Id");
                    }
                }
            }, userId, surveys);
        }

        /// <summary>
        /// <para>
        /// Export the survey participant credentials to the CSV file.
        /// </para>
        /// </summary>
        /// 
        /// <param name="surveyId">The survey Id.</param>
        /// <returns>
        /// The file stream with the credentials.
        /// </returns>
        /// 
        /// <exception cref="WebFaultException{ServiceFaultDetail}">
        /// If any error occurred <see cref="ServiceFaultDetail"/> will be created from:
        /// <para>
        /// <list type="bullet">
        /// <item><see cref="ArgumentException"/> If <paramref name="surveyId"/> is not valid positive number.</item>
        /// <item><see cref="EntityNotFoundException"/> If the entity does not exist.</item>
        /// <item><see cref="PersistenceException"/> If a DB-based error occurs.</item>
        /// <item><see cref="TholosException"/> If any other errors occur while performing this operation.</item>
        /// </list>
        /// </para>
        /// </exception>
        [OperationBehavior(TransactionScopeRequired = false)]
        [PrincipalPermission(SecurityAction.Demand, Role = "Manager")]
        public Stream ExportParticipantCredentials(string surveyId)
        {
            return Helper.LoggingWrapper(Logger, () =>
             {
                 var id = Helper.CheckAndGetValidNumberAndPositive(surveyId, "surveyId");

                 var conn = GetConnection();
                 Helper.CheckExistence("Survey", id, conn);

                 var credentials = Helper.ExecuteRetrieveSp(conn, GetParticipantCredentialsSpName,
                     Helper.ReaderParticipantCredentialsList, BuildParameteDicrForSurveyId(id));

                 foreach (var credential in credentials)
                 {
                     credential.Password = SecurityHelper.Decrypt(credential.Password);
                 }
                 var result = ExportService.ExportParticipantCredentials(credentials);
                 if (conn != null)
                 {
                     CloseConnection(conn);
                 }

                 return ReturnFile(result, "ParticipantCredentials.csv");

             }, surveyId);
        }

        /// <summary>
        /// <para>
        /// Export the survey results to the CSV file.
        /// </para>
        /// </summary>
        /// 
        /// <param name="surveyId">The survey Id.</param>
        /// <returns>
        /// The file stream with the results.
        /// </returns>
        /// 
        /// <exception cref="WebFaultException{ServiceFaultDetail}">
        /// If any error occurred <see cref="ServiceFaultDetail"/> will be created from:
        /// <para>
        /// <list type="bullet">
        /// <item><see cref="ArgumentException"/> If <paramref name="surveyId"/> is not valid positive number.</item>
        /// <item><see cref="EntityNotFoundException"/> If the entity does not exist.</item>
        /// <item><see cref="PersistenceException"/> If a DB-based error occurs.</item>
        /// <item><see cref="TholosException"/> If any other errors occur while performing this operation.</item>
        /// </list>
        /// </para>
        /// </exception>
        [OperationBehavior(TransactionScopeRequired = false)]
        [PrincipalPermission(SecurityAction.Demand, Role = "Manager")]
        public Stream ExportSurveyResults(string surveyId)
        {
            return Helper.LoggingWrapper(Logger, () =>
            {
                var participantSurveys = GetParticipantSurveys(surveyId, false);
                var result = ExportService.ExportSurveyResults(participantSurveys);
                return ReturnFile(result, "SurveyResults.csv");
            }, surveyId);
        }

        /// <summary>
        /// <para>
        /// Export the before and after product images for the specific survey. 
        /// </para>
        /// </summary>
        /// <remarks><para>
        /// The result zip file contains the CSV file with the participant username, prototype code 
        /// and the names of the before/after photos.
        /// </para></remarks>
        /// <param name="surveyId">The survey Id.</param>
        /// <returns>
        /// The file stream with the results.
        /// </returns>
        /// 
        /// <exception cref="WebFaultException{ServiceFaultDetail}">
        /// If any error occurred <see cref="ServiceFaultDetail"/> will be created from:
        /// <para>
        /// <list type="bullet">
        /// <item><see cref="ArgumentException"/> If <paramref name="surveyId"/> is not valid positive number.</item>
        /// <item><see cref="EntityNotFoundException"/> If the entity does not exist.</item>
        /// <item><see cref="PersistenceException"/> If a DB-based error occurs.</item>
        /// <item><see cref="TholosException"/> If any other errors occur while performing this operation.</item>
        /// </list>
        /// </para>
        /// </exception>
        [OperationBehavior(TransactionScopeRequired = false)]
        [PrincipalPermission(SecurityAction.Demand, Role = "Manager")]
        public Stream ExportProductImages(string surveyId)
        {
            return Helper.LoggingWrapper(Logger, () =>
            {
                var participantSurveys = GetParticipantSurveys(surveyId);
                var result = ExportService.ExportProductImages(participantSurveys);
                return ReturnFile(result, "ProductImages.zip");
            }, surveyId);
        }

        /// <summary>
        /// <para>
        /// Checks whether this instance was properly configured.
        /// </para>
        /// </summary>
        ///
        /// <exception cref="ConfigurationException ">
        /// If it is not configured.
        /// </exception>
        public override void CheckConfiguration()
        {
            base.CheckConfiguration();
            Helper.CheckConfiguration(ParticipantUsernameLength, "ParticipantUsernameLength");
            Helper.CheckConfiguration(ParticipantPasswordLength, "ParticipantPasswordLength");
            Helper.CheckConfiguration(ExportService, "ExportService");
        }

        #region private methods

        /// <summary>
        /// Returns the file.
        /// </summary>
        /// <param name="data">The data in byte[]</param>
        /// <param name="fileName">The file name.</param>
        /// <returns></returns>
        private Stream ReturnFile(byte[] data, string fileName)
        {
            if (WebOperationContext.Current != null)
            {
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/octet-stream";
                WebOperationContext.Current.OutgoingResponse.Headers.Add("content-disposition",
                    "inline; filename=" + fileName);
            }
            return new MemoryStream(data);
        }

        /// <summary>
        /// Helper method to generate the participant credentials. Username contains only alphanumeric chars.
        /// </summary>
        /// <param name="credentialsNumber">The number of credentials to generate.</param>
        /// <returns>The credentials.</returns>
        /// <exception>Exception will be propagated to caller.</exception>
        private IList<ParticipantCredentials> GenerareCredentials(int credentialsNumber)
        {
            String chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            char[] stringChars = new char[ParticipantUsernameLength];
            Random random = new Random();
            IList<ParticipantCredentials> result = new List<ParticipantCredentials>();

            // create number of credentials
            for (int j = 0; j < credentialsNumber; j++)
            {
                ParticipantCredentials credentials = new ParticipantCredentials();
                for (int i = 0; i < stringChars.Length; i++)
                {
                    stringChars[i] = chars[random.Next(chars.Length)];
                }
                credentials.Username = new String(stringChars);
                credentials.Password = SecurityHelper.Encrypt(Membership.GeneratePassword(
                    ParticipantPasswordLength, 0));
                result.Add(credentials);
            }
            return result;
        }

        /// <summary>
        /// Builds the parameter for survery update and create.
        /// </summary>
        /// <param name="survey">The survey.</param>
        /// <returns>The parameters.</returns>
        private static IDictionary<string, object> BuildCommonSurveyParameter(Survey survey)
        {
            return new Dictionary<string, object>
            {
                {"Name",survey.Name},
                {"CellName", survey.CellName},
                {"Status", survey.Status.ToString()},
                {"CompletedSurveysNumber", survey.CompletedSurveysNumber},
                {"Draft", survey.Draft},
            };
        }

        /// <summary>
        /// Creates prototype codes.
        /// </summary>
        /// <param name="conn">The open connection to database.</param>
        /// <param name="codes">The prototype codes.</param>
        /// <param name="surveyId">The survey id.</param>
        /// <exception>Exception will be propagated to caller.</exception>
        private static void CreatePrototypeCodes(IDbConnection conn, IList<PrototypeCode> codes, long surveyId)
        {
            if (codes != null)
            {
                foreach (var prototypeCode in codes)
                {
                    Helper.ExecuteSpWithReturnOutParam(conn, CreatePrototypeCodeSpName,
                        new Dictionary<string, object>
                        {
                            {"Code", prototypeCode.Code},
                            {"PrototypesPerCode", prototypeCode.PrototypesPerCode},
                            {"SurveyId", surveyId}
                        }, "Id");
                }
            }
        }

        /// <summary>
        /// Creates prototype and post testing questions for a survey
        /// </summary>
        /// <param name="conn">The open connection to database.</param>
        /// <param name="surveyId">The survey id.</param>
        /// <param name="spName">The stored procedure name.</param>
        /// <param name="questions">The questions.</param>
        /// <exception>Exception will be propagated to caller.</exception>
        private static void CreateQuestions(IDbConnection conn, long surveyId, string spName
            , IList<IndexedQuestion> questions)
        {
            if (questions != null)
            {
                foreach (var question in questions)
                {
                    Helper.ExecuteSpWithReturnOutParam(conn, spName,
                        new Dictionary<string, object>
                        {
                            {"SurveyId", surveyId},
                            {"QuestionId", question.Question.Id},
                            {"QuestionNumber", question.Position}
                        }, "Id");
                }
            }
        }

        /// <summary>
        /// Updates the status of survey.
        /// </summary>
        /// <param name="surveyId">The survey id.</param>
        /// <param name="status">The status to update.</param>
        /// <param name="isDraft">The flage whether to set as draft or not.</param>
        /// <exception cref="EntityNotFoundException">
        /// If If the entity does not exist with given <paramref name="surveyId"/>.</exception>
        /// <exception cref="PersistenceException"> If a DB-based error occurs.></exception>
        /// <exception cref="TholosException"> If any other errors occur while performing this operation.</exception>
        private void UpdateSurveyStatusOnly(long surveyId, SurveyStatus status, bool isDraft = false)
        {
            using (var conn = GetConnection())
            {
                Helper.ExecuteUpdateOrDeleteSp<Survey>(conn, UpdateSurveyStatusSpName, "Id=" + surveyId,
                    new Dictionary<string, object>
                    {
                        {"Id", surveyId},
                        {"Status", status.ToString()},
                        {"Draft", isDraft}
                    });
            }
        }

        /// <summary>
        /// Creates parameter dictionary for procedure which have only SurveyId as parameter.
        /// </summary>
        /// <param name="surveyId">The survey Id.</param>
        /// <returns>The parameters dictionary.</returns>
        private static Dictionary<string, object> BuildParameteDicrForSurveyId(long surveyId)
        {
            return new Dictionary<string, object>
            {
                {"SurveyId", surveyId}
            };
        }

        /// <summary>
        /// Wraps the search result items and returns the SurveySearchResult.
        /// </summary>
        /// <param name="totalRecords">The total records.</param>
        /// <param name="filter">The filter.</param>
        /// <param name="items">The records.</param>
        /// <returns>The survey search result.</returns>
        private SurveySearchResult PrepareSearchResult(int totalRecords, SearchFilter filter, IList<Survey> items)
        {
            int totalPages = 0;
            if (totalRecords > 0)
            {
                totalPages = filter.PageNumber == 0 ? 1 : (totalRecords - 1) / filter.PageSize + 1;
            }

            return new SurveySearchResult
            {
                TotalPageCount = totalPages,
                Records = items,
                TotalRecordCount = totalRecords
            };
        }

        /// <summary>
        /// Gets the participant surveys by survey id.
        /// </summary>
        /// <param name="surveyId">The survey id.</param>
        /// <param name="isPrototypeTestRequire">The flag when true, prototype test is required.</param>
        /// <returns>The participant surveys.</returns>
        /// <exception>Any exception will be propagated to the caller.</exception>
        private IList<ParticipantSurvey> GetParticipantSurveys(string surveyId, bool isPrototypeTestRequire = true)
        {
            var id = Helper.CheckAndGetValidNumberAndPositive(surveyId, "surveyId");

            using (var conn = GetConnection())
            {
                Helper.CheckExistence("Survey", id, conn);

                var participantSurveys = Helper.ExecuteRetrieveSp(conn, GetParticipantSurveysBySurveyIdSpName,
                    Helper.ReaderParticipantSurveyList, BuildParameteDicrForSurveyId(id));
                if (isPrototypeTestRequire)
                {
                    foreach (var participantSurvey in participantSurveys)
                    {
                        participantSurvey.PrototypeTests = Helper.ExecuteRetrieveSp(conn
                            , GetPrototypeTestsByParticipantSurveyIdSpName, Helper.ReaderPrototypeTestsList
                            , new Dictionary<string, object>
                            {
                                {"ParticipantSurveyId", participantSurvey.Id}
                            });
                    }
                }
                return participantSurveys;
            }
        }


        /// <summary>
        /// Searches the survey with given filter parameter and stored procedure.
        /// </summary>
        /// <param name="filterParameter">The filter parameter.</param>
        /// <param name="spName">The stored procedure name.</param>
        /// <param name="totalRecords">The total records matching the filter criteria.</param>
        /// <returns>The matched surveys.</returns>
        private IList<Survey> GetSearchSurveys(Dictionary<string, object> filterParameter, string spName, out int totalRecords)
        {
            IList<Survey> items;
            var conn = GetConnection();
            var command = Helper.CreateCommand(conn, spName, filterParameter);

            var totalParameter = command.CreateParameter();

            totalParameter.ParameterName = "@total";
            totalParameter.DbType = DbType.Int16;
            totalParameter.Direction = ParameterDirection.Output;
            command.Parameters.Add(totalParameter);
            using (IDataReader reader = command.ExecuteReader())
            {
                items = Helper.ReaderSurveyList(reader);
            }
            totalRecords = Convert.ToInt32(totalParameter.Value);
            if (conn != null)
            {
                CloseConnection(conn);
            }
            return items;
        }
        #endregion
    }
}

