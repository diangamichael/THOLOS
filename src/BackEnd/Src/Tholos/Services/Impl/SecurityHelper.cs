/*
*  Copyright (c) 2015, TopCoder, Inc. All rights reserved.
*/

using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace Tholos.Services.Impl
{
    /// <summary>
    /// <para>
    /// This class is the helper class used to hash the password.
    /// </para>
    /// </summary>
    ///
    /// <threadsafety>
    /// <para>
    /// This class is mutable but effectively thread-safe.
    /// </para>
    /// </threadsafety>
    /// 
    /// <author>veshu</author>
    ///
    /// <version>1.0</version>
    /// <copyright>Copyright (c) 2015, TopCoder, Inc. All rights reserved.</copyright>
    public class SecurityHelper
    {
        /// <summary>
        /// Represents the password hash.
        /// </summary>
        public static readonly string PasswordHash = "P@@Sw0rd";

        /// <summary>
        /// Represents the salt key.
        /// </summary>
        public static readonly string SaltKey = "S@LT&KEY";

        /// <summary>
        /// Represents the VI key.
        /// </summary>
        public static readonly string VIKey = "@1B2c3D4e5F6g7H8";

        /// <summary>
        /// <para>
        /// Initializes a new instance of the <see cref="SecurityHelper"/> class.
        /// </para>
        /// </summary>
        private SecurityHelper()
        {
        }

        /// <summary>
        /// <para>
        /// Helper method to hash the string with SHA1.
        /// </para>
        /// </summary>
        /// <param name="value">The string to hash.</param>
        /// <returns>The hashed string.</returns>
        /// <exception cref="ArgumentException">If value is empty.</exception>
        /// <exception cref="ArgumentNullException">If value is null.</exception>
        public static string HashSHA1(string value)
        {
            Helper.CheckNotNullOrEmpty(value, "value");
            var sha1 = SHA1.Create();
            var inputBytes = Encoding.ASCII.GetBytes(value);
            var hash = sha1.ComputeHash(inputBytes);

            var sb = new StringBuilder();
            for (var i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2"));
            }
            return sb.ToString();
        }

        /// <summary>
        /// <para>
        /// Helper method to encrypt the password.
        /// </para>
        /// </summary>
        /// <param name="password">The password to encrypt.</param>
        /// <returns>The encrypted string.</returns>
        /// <exception cref="ArgumentException">If password is empty.</exception>
        /// <exception cref="ArgumentNullException">If password is null.</exception>
        public static string Encrypt(string password)
        {
            Helper.CheckNotNullOrEmpty(password, "password");
            byte[] plainTextBytes = Encoding.UTF8.GetBytes(password);

            var keyBytes = new Rfc2898DeriveBytes(PasswordHash, Encoding.ASCII.GetBytes(SaltKey)).GetBytes(256 / 8);
            var symmetricKey = new RijndaelManaged() { Mode = CipherMode.CBC, Padding = PaddingMode.Zeros };
            var encryptor = symmetricKey.CreateEncryptor(keyBytes, Encoding.ASCII.GetBytes(VIKey));

            byte[] cipherTextBytes;

            using (var memoryStream = new MemoryStream())
            {
                using (var cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write))
                {
                    cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
                    cryptoStream.FlushFinalBlock();
                    cipherTextBytes = memoryStream.ToArray();
                    cryptoStream.Close();
                }
                memoryStream.Close();
            }
            return Convert.ToBase64String(cipherTextBytes);
        }

        /// <summary>
        /// <para>
        /// Helper method to decrypt the password.
        /// </para>
        /// </summary>
        /// <param name="password">The password to decrypt.</param>
        /// <returns>The decrypted string.</returns>
        /// <exception cref="ArgumentException">If password is empty.</exception>
        /// <exception cref="ArgumentNullException">If password is null.</exception>
        public static string Decrypt(string password)
        {
            Helper.CheckNotNullOrEmpty(password, "password");
            var cipherTextBytes = Convert.FromBase64String(password);
            var keyBytes = new Rfc2898DeriveBytes(PasswordHash, Encoding.ASCII.GetBytes(SaltKey)).GetBytes(256 / 8);
            var symmetricKey = new RijndaelManaged() { Mode = CipherMode.CBC, Padding = PaddingMode.None };

            var decryptor = symmetricKey.CreateDecryptor(keyBytes, Encoding.ASCII.GetBytes(VIKey));
            var memoryStream = new MemoryStream(cipherTextBytes);
            var cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read);
            var plainTextBytes = new byte[cipherTextBytes.Length];

            var decryptedByteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);
            memoryStream.Close();
            cryptoStream.Close();
            return Encoding.UTF8.GetString(plainTextBytes, 0, decryptedByteCount).TrimEnd("\0".ToCharArray());
        }
    }
}

