/*
*  Copyright (c) 2015, TopCoder, Inc. All rights reserved.
*/

using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Security.Permissions;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Web.Hosting;
using Microsoft.Practices.Unity;
using Tholos.Entities;
using IsolationLevel = System.Transactions.IsolationLevel;

namespace Tholos.Services.Impl
{
    /// <summary>
    /// <para>
    /// This class is the realization of the <see cref="IParticipantSurveyService"/> service contract.
    /// It handles all the participant survey related operations.
    /// </para>
    /// <para>
    /// Changes in version 1.1 (THOLOS - Integrate Prototype with Backend APIs):
    /// <list type="bullet">
    /// <item>
    /// <description>Added a new parameter for getting prototype codes.</description>
    /// </item>
    /// <item>
    /// <description>Removed BeforeWeight when creating prototype test.</description>
    /// </item>
    /// </list>
    /// </para>
    /// </summary>
    ///
    /// <threadsafety>
    /// This class is mutable (base class is mutable) but effectively thread-safe.
    /// </threadsafety>
    /// 
    /// <author>veshu</author>
    /// <author>duxiaoyang</author>
    ///
    /// <version>1.1</version>
    /// <since>1.0</since>
    /// <copyright>Copyright (c) 2015, TopCoder, Inc. All rights reserved.</copyright>
    [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple,
        InstanceContextMode = InstanceContextMode.PerSession,
        ReleaseServiceInstanceOnTransactionComplete = false,
        TransactionIsolationLevel = IsolationLevel.RepeatableRead)]
    public class ParticipantSurveyService : BasePersistenceService, IParticipantSurveyService
    {
        #region used stored procedures names

        /// <summary>
        /// Represents the name of the stored procedure to retrieve participant survey by id.
        /// </summary>
        private const string GetParticipantSurveySpName = "spGetParticipantSurvey";

        /// <summary>
        /// Represents the name of the stored procedure to retrieve PrototypeTest ids by participant survey id.
        /// </summary>
        private const string GetPrototypeTestIdsByParticipantSurveyIdSpName =
            "spGetPrototypeTestIdsByParticipantSurveyId";

        /// <summary>
        /// Represents the name of the stored procedure to retrieve prototype Answers by prototype test id.
        /// </summary>
        private const string GetPrototypeAnswersByPrototypeTestIdSpName = "spGetPrototypeAnswersByPrototypeTestId";

        /// <summary>
        /// Represents the name of the stored procedure to get post testing answers by participant survey id.
        /// </summary>
        private const string GetPostTestingAnswersByParticipantSurveyIdSpName =
            "spGetPostTestingAnswersByParticipantSurveyId";

        /// <summary>
        /// Represents the name of the stored procedure to create prototype test.
        /// </summary>
        private const string CreatePrototypeTestSpName = "spCreatePrototypeTest";

        /// <summary>
        /// Represents the name of the stored procedure to update current prototype test of participant survey.
        /// </summary>
        private const string UpdateCurrentPrototypeTestIdSpName = "spUpdateCurrentPrototypeTestId";

        /// <summary>
        /// Represents the name of the stored procedure to get prototype test by id.
        /// </summary>
        private const string GetPrototypeTestSpName = "spGetPrototypeTest";

        /// <summary>
        /// Represents the name of the stored procedure to create prototype answer.
        /// </summary>
        private const string CreatePrototypeAnswerSpName = "spCreatePrototypeAnswer";

        /// <summary>
        /// Represents the name of the stored procedure to update prototype answer.
        /// </summary>
        private const string UpdatePrototypeAnswerSpName = "spUpdatePrototypeAnswer";

        /// <summary>
        /// Represents the name of the stored procedure to get participant survey by prototype test id.
        /// </summary>
        private const string GetParticipantSurveyByPrototypeTestIdSpName = "spGetParticipantSurveyByPrototypeTestId";

        /// <summary>
        /// Represents the name of the stored procedure to update prototype test with complete data.
        /// </summary>
        private const string UpdatePrototypeTestWithCompleteDataSpName = "spUpdatePrototypeTestWithCompleteData";

        /// <summary>
        /// Represents the name of the stored procedure to create post testing answer.
        /// </summary>
        private const string CreatePostTestingAnswerSpName = "spCreatePostTestingAnswer";

        /// <summary>
        /// Represents the name of the stored procedure to increase completed survey number.
        /// </summary>
        private const string IncreaseCompetedSurveysNumberSpName = "spIncreaseCompetedSurveysNumber";

        /// <summary>
        /// Represents the name of the stored procedure to get survey prototype questions for participant survey id.
        /// </summary>
        private const string GetSurveyPrototypeQuestionsByParticipantSurveyIdSpName =
            "spGetSurveyPrototypeQuestionsByParticipantSurveyId";

        /// <summary>
        /// Represents the name of the stored procedure to get survey postTesting questions for participant survey id.
        /// </summary>
        private const string GetSurveyPostTestingQuestionsByParticipantSurveyIdSpName =
            "spGetSurveyPostTestingQuestionsByParticipantSurveyId";
        #endregion

        /// <summary>
        /// <para>
        /// Represents the photo filename template.
        /// </para>
        /// </summary>
        /// <value>The participant role name. It should not be null or empty .
        /// Defaults to "&lt;survey_id&gt;&lt;participant_id&gt;&lt;prototype_code&gt;&lt;iteration&gt;".</value>
        private string _productPhotoFilenameTemplate = "{survey_id}{participant_id}{prototype_code}{iteration}";

        /// <summary>
        /// <para>
        /// Gets or sets the photo filename template.
        /// </para>
        /// </summary>
        /// <remarks>It's used in photo uploading process.</remarks>
        /// <value>The photo filename template. It should not be null or empty and injected through Unity. 
        /// Defaults to "&lt;survey_id&gt;&lt;participant_id&gt;&lt;prototype_code&gt;&lt;iteration&gt;".</value>
        [Dependency("ProductPhotoFilenameTemplate")]
        public string ProductPhotoFilenameTemplate
        {
            get
            {
                return _productPhotoFilenameTemplate;
            }
            set
            {
                _productPhotoFilenameTemplate = value;
            }
        }

        /// <summary>
        /// <para>
        /// Gets or sets the photo uploading directory where all product photos will be placed.
        /// </para>
        /// </summary>
        /// <remarks>It's used in photo uploading process.</remarks>
        /// <value>
        /// The photo uploading directory. It should not be null or empty and injected through Unity. 
        /// </value>
        [Dependency("FileUploadPath")]
        public string FileUploadPath
        {
            get;
            set;
        }

        /// <summary>
        /// <para>
        /// Gets or sets the photo base URI that will be accessed by the frontend to load the photos.
        /// </para>
        /// </summary>
        /// <remarks>It's used in photo uploading process.</remarks>
        /// <value>
        /// The photo base URI. It should not be null or empty and injected through Unity. 
        /// </value>
        [Dependency("BaseFileUri")]
        public string BaseFileUri
        {
            get;
            set;
        }

        /// <summary>
        /// <para>
        /// Initializes a new instance of the <see cref="ParticipantSurveyService"/> class.
        /// </para>
        /// </summary>
        public ParticipantSurveyService()
        {
        }

        ///  <summary>
        ///  Gest the photo and saves in upload file with unique name which will later 
        ///  renamed while creating parcipant survey.
        ///  </summary>
        /// <para>THis file name must send when creating prototype or update.</para>
        /// <param name="fileName">The file name.</param>
        /// <param name="photo">The photo.</param>
        /// 
        ///  <returns>The unique file name.</returns>
        /// 
        ///  <exception cref="WebFaultException{T}">
        ///  If any error occurred <see cref="ServiceFaultDetail"/> will be created from:
        ///  <para>
        ///  <list type="bullet">
        ///  <item><see cref="ArgumentNullException"/>If <paramref name="photo"/> is <c>null</c>.</item>
        ///  <item><see cref="TholosException"/> If any other errors occur while performing this operation.</item>
        ///  </list> </para> </exception>
        [OperationBehavior(TransactionScopeRequired = false)]
        [PrincipalPermission(SecurityAction.Demand, Role = "Participant")]
        public string UploadPhoto(string fileName, Stream photo)
        {
            return Helper.LoggingWrapper(Logger, () =>
            {
                Helper.CheckNotNull(photo, "photo");
                Helper.ValidateFileNameWithExtension(fileName, "fileName");
                try
                {
                    var newfileName = Guid.NewGuid() + fileName.Substring(fileName.LastIndexOf('.'));
                    UploadFile(photo, newfileName);

                    return newfileName;
                }
                finally
                {
                    if (photo != null)
                    {
                        photo.Close();
                    }
                }
            }, fileName, photo);
        }

        /// <summary>
        /// Gets the participant survey fully populated with the answers.
        /// </summary>
        ///
        /// <param name="participantSurveyId">The participant survey id.</param>
        ///
        /// <returns>The participant survey.</returns>
        ///
        /// <exception cref="WebFaultException{T}">
        /// If any error occurred <see cref="ServiceFaultDetail"/> will be created from:
        /// <para>
        /// <list type="bullet">
        /// <item><see cref="ArgumentNullException"/>If <paramref name="participantSurveyId"/> is <c>null</c>.</item>
        /// <item><see cref="ArgumentException"/> If <paramref name="participantSurveyId"/> is not valid positive
        ///  number.</item>
        /// <item><see cref="PersistenceException"/> If a DB-based error occurs.</item>
        /// <item><see cref="TholosException"/> If any other errors occur
        /// while performing this operation.</item>
        /// </list>
        /// </para>
        /// </exception>
        [OperationBehavior(TransactionScopeRequired = false)]
        [PrincipalPermission(SecurityAction.Demand, Role = "Participant")]
        public ParticipantSurvey GetParticipantSurvey(string participantSurveyId)
        {
            return Helper.LoggingWrapper(Logger, () =>
            {
                var id = Helper.CheckAndGetValidNumberAndPositive(participantSurveyId, "participantSurveyId");
                var conn = GetConnection();
                var parameter = new Dictionary<string, object> { { "ParticipantSurveyId", id } };

                var participantSurvey = Helper.ExecuteRetrieveSp(conn, GetParticipantSurveySpName,
                    Helper.ReaderParticipantSurvey, parameter);

                if (participantSurvey != null)
                {
                    var prototypeTestIds = Helper.ExecuteRetrieveSp(conn,
                        GetPrototypeTestIdsByParticipantSurveyIdSpName, (reader) =>
                        {
                            var ids = new List<long>();
                            while (reader.Read())
                            {
                                ids.Add(Convert.ToInt64(reader[0]));
                            }
                            return ids;
                        }, parameter);

                    participantSurvey.PrototypeTests = new List<PrototypeTest>();
                    foreach (var prototypeTestId in prototypeTestIds)
                    {
                        participantSurvey.PrototypeTests.Add(PopulatePrototypeTest(conn, prototypeTestId));
                    }
                    participantSurvey.PostTestingAnswers = Helper.ExecuteRetrieveSp(conn
                        , GetPostTestingAnswersByParticipantSurveyIdSpName, Helper.ReaderAnswer, parameter);
                }
                if (conn != null)
                {
                    CloseConnection(conn);
                }
                return participantSurvey;
            }, participantSurveyId);
        }

        ///  <summary>
        ///  Creates the prototype test.
        ///  </summary>
        ///  <remarks> Uploads the before photo to the given path and persists its uri path.</remarks>
        ///  <param name="participantSurveyId">The participant survey id.</param>
        ///  <param name="prototypeTest">The prototype test.</param>
        ///  <param name="beforePhoto">The before photo.</param>
        ///
        ///  <returns>The participant survey.</returns>
        /// 
        ///  <exception cref="WebFaultException{ServiceFaultDetail}">
        ///  If any error occurred <see cref="ServiceFaultDetail"/> will be created from:
        ///  <para>
        ///  <list type="bullet">
        ///  <item><see cref="ArgumentNullException"/> 
        ///  If <paramref name="participantSurveyId"/> or <paramref name="beforePhoto"/>  or 
        ///     <paramref name="prototypeTest"/> is <c>null</c>.</item>
        ///  <item><see cref="ArgumentException"/>
        ///  If <paramref name="participantSurveyId"/> is not valid positive number.</item>
        ///  <item><see cref="PersistenceException"/> If a DB-based error occurs.</item>
        ///  <item><see cref="TholosException"/> If any other errors occur while performing this operation.</item>
        ///  </list> </para>  </exception>
        [OperationBehavior(TransactionScopeRequired = true, TransactionAutoComplete = true)]
        [PrincipalPermission(SecurityAction.Demand, Role = "Participant")]
        public long CreatePrototypeTest(string participantSurveyId, PrototypeTest prototypeTest, string beforePhoto)
        {
            return Helper.LoggingWrapper(Logger, () =>
            {
                var id = Helper.CheckAndGetValidNumberAndPositive(participantSurveyId, "participantSurveyId");

                Helper.CheckNotNull(prototypeTest, "prototypeTest");
                Helper.CheckNotNullOrEmpty(beforePhoto, "beforePhoto");
                Helper.ValidatePrototypeTest(prototypeTest);

                using (var conn = GetConnection())
                {
                    var parameter = new Dictionary<string, object>
                    {
                        {"ParticipantSurveyId", id}
                    };

                    var participantSurvey = Helper.ExecuteRetrieveSp(conn, GetParticipantSurveySpName,
                        Helper.ReaderParticipantSurvey, parameter);

                    if (participantSurvey == null)
                    {
                        throw new EntityNotFoundException(
                            string.Format("No participant survey is found for id {0}", id));
                    }

                    var filename = ProcessTemplate(ProductPhotoFilenameTemplate, participantSurvey.SurveyId,
                        participantSurvey.ParticipantId, prototypeTest.PrototypeCode, prototypeTest.Iteration) +
                                   "_before";

                    RenamePhoto(beforePhoto, filename);

                    var prototypeTestId = Helper.ExecuteSpWithReturnOutParam(conn, CreatePrototypeTestSpName,
                        new Dictionary<string, object>
                        {
                            {"PrototypeCode", prototypeTest.PrototypeCode},
                            {"ParticipantSurveyId", participantSurvey.Id},
                            {"Iteration", prototypeTest.Iteration},
                            {"BeforePhotoLocalPath", FileUploadPath + filename},
                            {"BeforePhotoUri", BaseFileUri + filename},
                        }, "id");

                    if (prototypeTestId <= 0)
                    {
                        throw new Exception("Error while creating prototype test.");
                    }

                    Helper.ExecuteSp(conn, UpdateCurrentPrototypeTestIdSpName, new Dictionary<string, object>
                    {
                        {"ParticipantSurveyId", participantSurvey.Id},
                        {"PrototypeTestId", prototypeTestId}
                    });

                    return prototypeTestId;
                }

            }, participantSurveyId, prototypeTest, beforePhoto);
        }

        ///  <summary>
        ///  Gets the prototype test.
        ///  </summary>
        /// 
        ///  <param name="prototypeTestId">The prototype test id.</param>
        ///  <returns>The prototype test.</returns>
        /// 
        ///  <exception cref="WebFaultException{ServiceFaultDetail}">
        ///  If any error occurred <see cref="ServiceFaultDetail"/> will be created from:
        ///  <para>
        ///  <list type="bullet">
        ///  <item><see cref="ArgumentNullException"/> If
        ///  <paramref name="prototypeTestId"/> is <c>null</c>.</item>
        ///  <item><see cref="ArgumentException"/> If
        ///  <paramref name="prototypeTestId"/> is not valid positive number.</item>
        ///  <item><see cref="PersistenceException"/> If a DB-based error occurs.</item>
        ///  <item><see cref="TholosException"/> If any other errors occur while performing this operation.</item>
        ///  </list> </para> </exception>
        [OperationBehavior(TransactionScopeRequired = false)]
        [PrincipalPermission(SecurityAction.Demand, Role = "Participant")]
        public PrototypeTest GetPrototypeTest(string prototypeTestId)
        {
            return Helper.LoggingWrapper(Logger, () =>
            {
                var id = Helper.CheckAndGetValidNumberAndPositive(prototypeTestId, "prototypeTestId");
                using (var conn = GetConnection())
                {
                    return PopulatePrototypeTest(conn, id);
                }
            }, prototypeTestId);
        }

        ///  <summary>
        ///  Creates the prototype answer.
        ///  </summary>
        /// 
        ///  <param name="prototypeTestId">The participant test id.</param>
        ///  <param name="answer">The answer.</param>
        ///  <returns>The answer id.</returns>
        /// 
        ///  <exception cref="WebFaultException{ServiceFaultDetail}">
        ///  If any error occurred <see cref="ServiceFaultDetail"/> will be created from:
        ///  <para>
        ///  <list type="bullet">
        ///  <item><see cref="ArgumentNullException"/> 
        ///  If <paramref name="prototypeTestId"/> or <paramref name="answer"/> is <c>null</c>.</item>
        ///  <item>
        ///  <see cref="ArgumentException"/>
        ///  If <paramref name="prototypeTestId"/> is not valid positive number.</item>
        ///  <item><see cref="EntityNotFoundException"/> If the prototypeTest does not exist.</item>
        ///  <item><see cref="PersistenceException"/> If a DB-based error occurs.</item>
        ///  <item><see cref="TholosException"/> If any other errors occur while performing this operation.</item>
        ///  </list> </para> </exception>
        [OperationBehavior(TransactionScopeRequired = true, TransactionAutoComplete = true)]
        [PrincipalPermission(SecurityAction.Demand, Role = "Participant")]
        public long SubmitPrototypeAnswer(string prototypeTestId, Answer answer)
        {
            return Helper.LoggingWrapper(Logger, () =>
            {
                var id = Helper.CheckAndGetValidNumberAndPositive(prototypeTestId, "prototypeTestId");
                using (var conn = GetConnection())
                {
                    Helper.CheckExistence("PrototypeTest", id, conn);
                    Helper.CheckNotNull(answer, "answer");
                    Helper.ValidateAnswer(answer);
                    var paramaters = Helper.BuildAnswerParameter(answer);
                    paramaters.Add("PrototypeTestId", id);
                    var answerId = Helper.ExecuteSpWithReturnOutParam(conn, CreatePrototypeAnswerSpName,
                        paramaters, "Id");
                    return answerId;
                }
            }, prototypeTestId, answer);
        }

        ///  <summary>
        ///  Updates the prototype answer.
        ///  </summary>
        /// 
        ///  <param name="answer">The prototype answer.</param>
        /// 
        ///  <exception cref="WebFaultException{ServiceFaultDetail}">
        ///  If any error occurred <see cref="ServiceFaultDetail"/> will be created from:
        ///  <para>
        ///  <list type="bullet">
        ///  <item><see cref="ArgumentNullException"/> If <paramref name="answer"/> is <c>null</c>.</item>
        ///  <item><see cref="EntityNotFoundException"/> If the entity does not exist.</item>
        ///  <item><see cref="PersistenceException"/> If a DB-based error occurs.</item>
        ///  <item><see cref="TholosException"/> If any other errors occur while performing this operation.</item>
        ///  </list> </para> </exception>
        [OperationBehavior(TransactionScopeRequired = true, TransactionAutoComplete = true)]
        [PrincipalPermission(SecurityAction.Demand, Role = "Participant")]
        public void UpdatePrototypeAnswer(Answer answer)
        {
            Helper.LoggingWrapper(Logger, () =>
            {
                Helper.CheckNotNull(answer, "answer");
                using (var conn = GetConnection())
                {
                    var paramaters = Helper.BuildAnswerParameter(answer);
                    paramaters.Add("Id", answer.Id);
                    paramaters.Remove("QuestionId");

                    Helper.ExecuteUpdateOrDeleteSp<Answer>(conn, UpdatePrototypeAnswerSpName,
                        "Id=" + answer.Id, paramaters, true);
                }
            }, answer);
        }

        ///  <summary>
        ///  Complete the prototype test. Uploads the after photo to the given path and persists its uri path.
        ///  </summary>
        /// <param name="prototypeTestId">The prototype test id.</param>
        /// <param name="fileName">The file name used to get the extension of photo.</param>
        /// <param name="afterWeight">The after weight.</param>
        /// <param name="afterPhoto">The after photo.</param>
        /// <exception cref="WebFaultException{ServiceFaultDetail}">
        ///  If any error occurred <see cref="ServiceFaultDetail"/> will be created from:
        ///  <para>
        ///  <list type="bullet">
        ///  <item>
        /// <see cref="ArgumentNullException"/> 
        ///   If <paramref name="prototypeTestId"/> or <paramref name="afterPhoto"/> is <c>null</c>.</item>
        ///  <item>
        /// <see cref="ArgumentException"/> 
        ///  If <paramref name="prototypeTestId"/> is not valid positive number.</item>
        ///  <item><see cref="EntityNotFoundException"/> If the entity does not exist.</item>
        ///  <item><see cref="PersistenceException"/> If a DB-based error occurs.</item>
        ///  <item><see cref="TholosException"/> If any other errors occur
        ///  while performing this operation.</item>
        ///  </list>
        ///  </para>
        ///  </exception>
        [OperationBehavior(TransactionScopeRequired = true, TransactionAutoComplete = true)]
        [PrincipalPermission(SecurityAction.Demand, Role = "Participant")]
        public void CompletePrototypeTest(string prototypeTestId, string fileName, string afterWeight
            , Stream afterPhoto)
        {
            Helper.LoggingWrapper(Logger, () =>
            {
                var id = Helper.CheckAndGetValidNumberAndPositive(prototypeTestId, "prototypeTestId");
                double? weight = null;
                if (!string.IsNullOrEmpty(fileName))
                {
                    Helper.ValidateFileNameWithExtension(fileName, "fileName");
                }
                if (!string.IsNullOrEmpty(afterWeight))
                {
                    try
                    {
                        weight = Convert.ToDouble(afterWeight);
                    }
                    catch (Exception)
                    {
                        throw new ArgumentException(
                            "The parameter afterWeight is not valid double. It should be double e.g. 10.2");
                    }
                }
                using (var conn = GetConnection())
                {
                    var prototypeTest = Helper.ExecuteRetrieveSp(conn, GetPrototypeTestSpName
                        , Helper.ReaderPrototypeTests, new Dictionary<string, object>
                        {
                            {"prototypeTestId", id}
                        });

                    if (prototypeTest == null)
                    {
                        throw new EntityNotFoundException(string.Format(
                            "PrototypeTest with id {0} is not found.", id));
                    }

                    var parameter = new Dictionary<string, object>
                    {
                        {"PrototypeTestId", prototypeTest.Id}
                    };
                    var participantSurvey = Helper.ExecuteRetrieveSp(conn,
                        GetParticipantSurveyByPrototypeTestIdSpName, Helper.ReaderParticipantSurvey, parameter);

                    if (participantSurvey == null)
                    {
                        throw new EntityNotFoundException(
                            string.Format("No participation survey is asscociated with prototye " +
                                          "test with id {0} is not found.", id));
                    }

                    string filename = null;

                    if (!string.IsNullOrEmpty(fileName))
                    {
                        filename = ProcessTemplate(ProductPhotoFilenameTemplate, participantSurvey.SurveyId,
                            participantSurvey.ParticipantId, prototypeTest.PrototypeCode, prototypeTest.Iteration) +
                                   "_after" + fileName.Substring(fileName.LastIndexOf('.'));

                        UploadFile(afterPhoto, filename);
                    }

                    Helper.ExecuteSp(conn, UpdatePrototypeTestWithCompleteDataSpName,
                        new Dictionary<string, object>
                        {
                            {"Id", id},
                            {"AfterWeight", weight},
                            {"AfterPhotoLocalPath", filename != null ? FileUploadPath + filename : null},
                            {"AfterPhotoUri", filename != null ? BaseFileUri + filename : null}
                        });
                }
            }, prototypeTestId, fileName, afterWeight, afterPhoto);
        }

        ///  <summary>
        ///  Submits the post testing answer.
        ///  </summary>
        /// 
        ///  <param name="participantSurveyId">The participant survey id.</param>
        ///  <param name="postTestingAnswer">The answer.</param>
        /// 
        ///  <exception cref="WebFaultException{ServiceFaultDetail}">
        ///  If any error occurred <see cref="ServiceFaultDetail"/> will be created from:
        ///  <para>
        ///  <list type="bullet">
        ///  <item><see cref="ArgumentNullException"/> If <paramref name="participantSurveyId"/> or
        ///  <paramref name="postTestingAnswer"/> is <c>null</c>.</item>
        ///  <item><see cref="ArgumentException"/> 
        ///     If <paramref name="participantSurveyId"/> is not valid positive number.</item>
        ///  <item><see cref="EntityNotFoundException"/> If the entity does not exist.</item>
        ///  <item><see cref="PersistenceException"/> If a DB-based error occurs.</item>
        ///  <item><see cref="TholosException"/> If any other errors occur while performing this operation.</item>
        ///  </list> </para> </exception>
        [OperationBehavior(TransactionScopeRequired = true, TransactionAutoComplete = true)]
        [PrincipalPermission(SecurityAction.Demand, Role = "Participant")]
        public void SubmitPostTestingAnswer(string participantSurveyId, Answer postTestingAnswer)
        {
            Helper.LoggingWrapper(Logger, () =>
            {
                var id = Helper.CheckAndGetValidNumberAndPositive(participantSurveyId, "participantSurveyId");
                Helper.CheckNotNull(postTestingAnswer, "postTestingAnswer");
                using (var conn = GetConnection())
                {
                    Helper.CheckExistence("ParticipantSurvey", id, conn);
                    Helper.ValidateAnswer(postTestingAnswer);

                    var paramaters = Helper.BuildAnswerParameter(postTestingAnswer);
                    paramaters.Add("ParticipantSurveyId", id);
                    Helper.ExecuteSpWithReturnOutParam(conn, CreatePostTestingAnswerSpName,
                        paramaters, "Id");
                }
            }, participantSurveyId, postTestingAnswer);
        }

        /// <summary>
        /// Completes the post testing.
        /// </summary>
        ///
        /// <param name="participantSurveyId">The participant survey id.</param>
        ///
        /// <exception cref="WebFaultException{ServiceFaultDetail}">
        /// If any error occurred <see cref="ServiceFaultDetail"/> will be created from:
        /// <para>
        /// <list type="bullet">
        /// <item><see cref="ArgumentNullException"/>If <paramref name="participantSurveyId"/> is <c>null</c>.</item>
        /// <item><see cref="ArgumentException"/> 
        /// If <paramref name="participantSurveyId"/> is not valid positive number.</item>
        /// <item><see cref="EntityNotFoundException"/> If the entity does not exist.</item>
        /// <item><see cref="PersistenceException"/> If a DB-based error occurs.</item>
        /// <item><see cref="TholosException"/> If any other errors occur
        /// while performing this operation.</item>
        /// </list>
        /// </para>
        /// </exception>
        [OperationBehavior(TransactionScopeRequired = true, TransactionAutoComplete = true)]
        [PrincipalPermission(SecurityAction.Demand, Role = "Participant")]
        public void CompletePostTesting(string participantSurveyId)
        {
            Helper.LoggingWrapper(Logger, () =>
            {
                var id = Helper.CheckAndGetValidNumberAndPositive(participantSurveyId, "participantSurveyId");
                using (var conn = GetConnection())
                {
                    Helper.CheckExistence("ParticipantSurvey", id, conn);
                    var paramaters = new Dictionary<string, object>
                    {
                        {"ParticipantSurveyId", id}
                    };
                    Helper.ExecuteSp(conn, IncreaseCompetedSurveysNumberSpName, paramaters);
                }
            }, participantSurveyId);

        }

        /// <summary>
        /// Gets the prototype questions fully populated with the answer options.
        /// </summary>
        ///
        /// <param name="participantSurveyId">The participant survey id.</param>
        ///
        /// <returns>The prototype questions.</returns>
        ///
        /// <exception cref="WebFaultException{ServiceFaultDetail}">
        /// If any error occurred <see cref="ServiceFaultDetail"/> will be created from:
        /// <para>
        /// <list type="bullet">
        /// <item><see cref="ArgumentNullException"/>If <paramref name="participantSurveyId"/> is <c>null</c>.</item>
        /// <item><see cref="ArgumentException"/> 
        /// If <paramref name="participantSurveyId"/> is not valid positive number.</item>
        /// <item><see cref="PersistenceException"/> If a DB-based error occurs.</item>
        /// <item><see cref="TholosException"/> If any other errors occur
        /// while performing this operation.</item>
        /// </list>
        /// </para>
        /// </exception>
        [OperationBehavior(TransactionScopeRequired = false)]
        [PrincipalPermission(SecurityAction.Demand, Role = "Participant")]
        public IList<Question> GetPrototypeQuestions(string participantSurveyId)
        {
            return Helper.LoggingWrapper(Logger, () =>
            {
                var id = Helper.CheckAndGetValidNumberAndPositive(participantSurveyId, "participantSurveyId");
                using (var conn = GetConnection())
                {
                    var questions = Helper.ExecuteRetrieveSp(conn,
                        GetSurveyPrototypeQuestionsByParticipantSurveyIdSpName,
                        Helper.ReaderQuestionsList, new Dictionary<string, object>
                        {
                            {"ParticipantSurveyId", id}
                        });

                    foreach (var question in questions)
                    {
                        question.AnswerOptions = Helper.GetAnswerOptionsByQuestionId(conn,
                            Helper.GetPrototypeAnswerOptionsByQuestionIdSpName, question.Id);
                    }
                    return questions;
                }
            }, participantSurveyId);
        }

        /// <summary>
        /// Gets the post testing questions fully populated with the answer options.
        /// </summary>
        ///
        /// <param name="participantSurveyId">The participant survey id.</param>
        ///
        /// <returns>The post testing questions.</returns>
        ///
        /// <exception cref="WebFaultException{ServiceFaultDetail}">
        /// If any error occurred <see cref="ServiceFaultDetail"/> will be created from:
        /// <para>
        /// <list type="bullet">
        /// <item><see cref="ArgumentNullException"/>If <paramref name="participantSurveyId"/> is <c>null</c>.</item>
        /// <item><see cref="ArgumentException"/> If <paramref name="participantSurveyId"/> is not valid positive
        ///  number.</item>
        /// <item><see cref="PersistenceException"/> If a DB-based error occurs.</item>
        /// <item><see cref="TholosException"/> If any other errors occur
        /// while performing this operation.</item>
        /// </list>
        /// </para>
        /// </exception>
        [OperationBehavior(TransactionScopeRequired = false)]
        [PrincipalPermission(SecurityAction.Demand, Role = "Participant")]
        public IList<Question> GetPostTestingQuestions(string participantSurveyId)
        {
            return Helper.LoggingWrapper(Logger, () =>
            {
                var id = Helper.CheckAndGetValidNumberAndPositive(participantSurveyId, "");
                using (var conn = GetConnection())
                {
                    var questions = Helper.ExecuteRetrieveSp(conn,
                        GetSurveyPostTestingQuestionsByParticipantSurveyIdSpName, Helper.ReaderQuestionsList
                        , new Dictionary<string, object>
                        {
                            {"ParticipantSurveyId", id}
                        });

                    foreach (var question in questions)
                    {
                        question.AnswerOptions = Helper.GetAnswerOptionsByQuestionId(conn,
                            Helper.GetPrototypeAnswerOptionsByQuestionIdSpName, question.Id);
                    }

                    return questions;
                }
            }, participantSurveyId);
        }

        /// <summary>
        /// Gets the survey prototype codes.
        /// </summary>
        ///
        /// <param name="surveyId">The survey id.</param>
        ///
        /// <returns>The prototype codes.</returns>
        ///
        /// <exception cref="WebFaultException{ServiceFaultDetail}">
        /// If any error occurred <see cref="ServiceFaultDetail"/> will be created from:
        /// <para>
        /// <list type="bullet">
        /// <item><see cref="ArgumentNullException"/>If <paramref name="surveyId"/> is <c>null</c>.</item>
        /// <item><see cref="ArgumentException"/> If <paramref name="surveyId"/> is not valid positive number.</item>
        /// <item><see cref="PersistenceException"/> If a DB-based error occurs.</item>
        /// <item><see cref="TholosException"/> If any other errors occur
        /// while performing this operation.</item>
        /// </list>
        /// </para>
        /// </exception>
        [OperationBehavior(TransactionScopeRequired = false)]
        [PrincipalPermission(SecurityAction.Demand, Role = "Participant")]
        public IList<String> GetSurveyPrototypeCodes(string surveyId)
        {
            return Helper.LoggingWrapper(Logger, () =>
             {
                 var id = Helper.CheckAndGetValidNumberAndPositive(surveyId, "surveyId");
                 using (var conn = GetConnection())
                 {
                     var parameters = new Dictionary<string, object>
                     {
                         {"SurveyId", id},
                         {"Status", SurveyStatus.Published.ToString()}
                     };
                     var codes = Helper.ExecuteRetrieveSp(conn
                         , Helper.GetPrototypeCodesBySurveyIdSpName, Helper.ReaderPrototypeCode, parameters);

                     return codes.Select(p => p.Code).Distinct().ToList();
                 }

             }, surveyId);
        }

        /// <summary>
        /// <para>
        /// Checks whether this instance was properly configured.
        /// </para>
        /// </summary>
        ///
        /// <exception cref="ConfigurationException ">
        /// If it is not configured.
        /// </exception>
        public override void CheckConfiguration()
        {
            base.CheckConfiguration();
            Helper.CheckConfiguration(ProductPhotoFilenameTemplate, "ProductPhotoFilenameTemplate");
            Helper.CheckConfiguration(BaseFileUri, "BaseFileUri");
            Helper.CheckConfiguration(FileUploadPath, "FileUploadPath");
        }

        /// <summary>
        /// Helper method to upload the file.
        /// </summary>
        /// <param name="file">The file stream</param>
        /// <param name="filename">The filename.</param>
        /// <exception>Any exception will be propagated to caller.</exception>
        private void UploadFile(Stream file, string filename)
        {
            var folderPath = HostingEnvironment.MapPath(FileUploadPath);
            if (!Directory.Exists(folderPath))
            {
                Directory.CreateDirectory(folderPath);
            }
            string filePath = Path.Combine(folderPath + filename);
            int length = 0;
            using (FileStream writer = new FileStream(filePath, FileMode.Create))
            {
                int readCount;
                var buffer = new byte[8192];
                while ((readCount = file.Read(buffer, 0, buffer.Length)) != 0)
                {
                    writer.Write(buffer, 0, readCount);
                    length += readCount;
                }
            }
        }

        /// <summary>
        /// Rename the photo
        /// </summary>
        /// <param name="beforePhoto">The photo uploaded previously.</param>
        /// <param name="filename">The filename.</param>
        private void RenamePhoto(string beforePhoto, string filename)
        {
            string newFilePath =
                Path.Combine(
                    HostingEnvironment.MapPath(FileUploadPath + filename +
                                               beforePhoto.Substring(beforePhoto.LastIndexOf('.'))));

            var oldFilePath = Path.Combine(HostingEnvironment.MapPath(FileUploadPath + beforePhoto));
            if (File.Exists(newFilePath))
            {
                File.Delete(newFilePath);
            }
            if (!File.Exists(oldFilePath))
            {
                throw new ArgumentException("No file is found.");
            }
            File.Move(oldFilePath, newFilePath);
        }

        /// <summary>
        /// Processes the photo filename template.
        /// </summary>
        /// <param name="template">The template.</param>
        /// <param name="surveyId">The survey Id</param>
        /// <param name="participantId">The participant id.</param>
        /// <param name="prototypeCode">The prototype code.</param>
        /// <param name="iteration">The iteration.</param>
        /// <returns>The processed template.</returns>
        private static string ProcessTemplate(string template, long surveyId, long participantId
            , string prototypeCode, int iteration)
        {
            template = template.Replace("{survey_id}", surveyId.ToString());
            template = template.Replace("{participant_id}", participantId.ToString());
            template = template.Replace("{prototype_code}", prototypeCode);
            template = template.Replace("{iteration}", iteration.ToString());
            return template;
        }

        /// <summary>
        /// Gets the prototypetest by prototype test id.
        /// </summary>
        /// <param name="conn">The open connection to database.</param>
        /// <param name="prototypeTestId">The prototype test id.</param>
        /// <returns>The prototype entity.</returns>
        private PrototypeTest PopulatePrototypeTest(IDbConnection conn, long prototypeTestId)
        {
            var prototypeTest = Helper.ExecuteRetrieveSp(conn, GetPrototypeTestSpName
                , Helper.ReaderPrototypeTests, new Dictionary<string, object>
                {
                    {"prototypeTestId", prototypeTestId}
                });
            if (prototypeTest != null)
            {
                prototypeTest.Answers = Helper.ExecuteRetrieveSp(conn
                    , GetPrototypeAnswersByPrototypeTestIdSpName, Helper.ReaderAnswer,
                    new Dictionary<string, object>
                    {
                        {"prototypeTestId", prototypeTestId}
                    });
            }
            return prototypeTest;
        }
    }
}

