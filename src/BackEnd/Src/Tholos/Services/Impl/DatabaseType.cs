/*
 * Copyright (C) 2015, TopCoder, Inc. All rights reserved.
 */

namespace Tholos.Services.Impl
{
    /// <summary>
    /// <para>
    /// This enum represents the database type.
    /// </para>
    /// </summary>
    ///
    /// <threadsafety>
    /// Enum is immutable and thread-safe.
    /// </threadsafety>
    ///
    /// <author>veshu</author>
    /// 
    /// <version>1.0</version>
    /// <copyright>Copyright (C) 2015, TopCoder, Inc. All rights reserved.</copyright>
    public enum DatabaseType
    {
        /// <summary>
        /// <para>
        /// Representing SQL Server database.
        /// </para>
        /// </summary>
        SqlServer,

        /// <summary>
        /// <para>
        /// Representing MySQL database.
        /// </para>
        /// </summary>
        MySql
    }
}

