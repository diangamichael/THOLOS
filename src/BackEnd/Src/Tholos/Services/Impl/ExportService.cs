/*
*  Copyright (c) 2015, TopCoder, Inc. All rights reserved.
*/

using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.ServiceModel.Web;
using CsvHelper;
using Tholos.Entities;

namespace Tholos.Services.Impl
{
    /// <summary>
    /// This class is the realization of the <see cref="IExportService"/> service contract.
    /// It handles the export functionality.
    /// </summary>
    ///
    /// <threadsafety>
    /// This class is mutable (base class is mutable) but effectively thread-safe.
    /// </threadsafety>
    ///
    /// <author>veshu</author>
    ///
    /// <version>1.0</version>
    /// <copyright>Copyright (c) 2015, TopCoder, Inc. All rights reserved.</copyright>
    public class ExportService : BaseService, IExportService
    {

        /// <summary>
        /// <para>
        /// Initializes a new instance of the <see cref="ExportService"/> class.
        /// </para>
        /// </summary>
        public ExportService()
        {
        }

        /// <summary>
        /// <para>
        ///  Export the survey results to the CSV file.
        /// </para>
        /// </summary>
        /// 
        /// <param name="participantSurveys">The participant surveys.</param>
        /// 
        /// <returns> The file content. </returns>
        /// 
        /// <exception cref="WebFaultException{T}">
        /// If any error occurred <see cref="ServiceFaultDetail"/> will be created from:
        /// <para>
        /// <list type="bullet">
        /// <item><see cref="ArgumentNullException"/> 
        /// If <paramref name="participantSurveys"/> is <c>null</c>
        /// </item>
        /// <item><see cref="ExportingException"/>If any other errors occur while performing this operation.</item>
        /// </list></para></exception>
        public byte[] ExportSurveyResults(IList<ParticipantSurvey> participantSurveys)
        {
            return Helper.LoggingWrapper(Logger, () =>
            {
                Helper.CheckNotNull(participantSurveys, "participantSurveys");
                try
                {
                    using (var memoryStream = new MemoryStream())
                    {
                        using (var streamWriter = new StreamWriter(memoryStream))
                        {
                            using (var csvWriter = new CsvWriter(streamWriter))
                            {
                                foreach (ParticipantSurvey ps in participantSurveys)
                                {
                                    csvWriter.WriteField(ps.Id);
                                    csvWriter.WriteField(ps.SurveyId);
                                    csvWriter.WriteField(ps.ParticipantId);
                                    csvWriter.WriteField(ps.CurrentPrototypeTestId);
                                    csvWriter.NextRecord();
                                }
                                streamWriter.Flush();
                                memoryStream.Position = 0;
                            }
                        }
                        return memoryStream.ToArray();
                    }
                }
                catch (Exception ex)
                {
                    throw new ExportingException("Error in exporting survey results.", ex);
                }
            }, participantSurveys);
        }

        /// <summary>
        /// <para>
        /// Export the product images along with the CSV description file to the zip and return its content.
        /// </para>
        /// </summary>
        /// 
        /// <param name="participantSurveys">The participant surveys.</param>
        /// 
        /// <returns> The file content. </returns>
        /// 
        /// <exception cref="WebFaultException{ServiceFaultDetail}">
        /// If any error occurred <see cref="ServiceFaultDetail"/> will be created from:
        /// <para>
        /// <list type="bullet">
        /// <item><see cref="ArgumentNullException"/> 
        /// If <paramref name="participantSurveys"/> is <c>null</c>
        /// </item>
        /// <item><see cref="ExportingException"/>If any other errors occur while performing this operation.</item>
        /// </list></para></exception>
        public byte[] ExportProductImages(IList<ParticipantSurvey> participantSurveys)
        {
            return Helper.LoggingWrapper(Logger, () =>
            {
                Helper.CheckNotNull(participantSurveys, "participantSurveys");
                try
                {
                    var filename = Helper.GetTempFilePathWithExtension(".csv");
                    using (var memoryStream = new MemoryStream())
                    {
                        using (var streamWriter = new StreamWriter(memoryStream))
                        {
                            using (var csvWriter = new CsvWriter(streamWriter))
                            {
                                foreach (ParticipantSurvey ps in participantSurveys)
                                {
                                    if (ps.PrototypeTests != null)
                                    {
                                        foreach (var pt in ps.PrototypeTests)
                                        {
                                            csvWriter.WriteField(ps.SurveyId);
                                            csvWriter.WriteField(ps.ParticipantId);
                                            csvWriter.WriteField(Path.GetFileName(pt.BeforePhotoLocalPath));
                                            csvWriter.WriteField(Path.GetFileName(pt.AfterPhotoLocalPath));
                                            csvWriter.NextRecord();
                                        }
                                    }
                                }
                                streamWriter.Flush();
                                memoryStream.Position = 0;
                                FileStream file = new FileStream(filename, FileMode.Create, FileAccess.Write);
                                memoryStream.WriteTo(file);
                                file.Close();
                            }

                        }

                    }
                    using (MemoryStream zipStream = new MemoryStream())
                    {
                        using (ZipArchive zip = new ZipArchive(zipStream, ZipArchiveMode.Create, true))
                        {
                            foreach (var survey in participantSurveys)
                            {
                                foreach (var ps in survey.PrototypeTests)
                                {
                                    string beforePhotoUriFileName = Path.GetFileName(ps.BeforePhotoLocalPath);
                                    zip.CreateEntryFromFile(ps.BeforePhotoLocalPath, beforePhotoUriFileName,
                                        CompressionLevel.Fastest);

                                    if (string.IsNullOrEmpty(ps.AfterPhotoUri))
                                    {
                                        continue;
                                    }

                                    var afterPhotoUriFileName = Path.GetFileName(ps.AfterPhotoLocalPath);

                                    zip.CreateEntryFromFile(ps.AfterPhotoLocalPath, afterPhotoUriFileName,
                                        CompressionLevel.Fastest);
                                }
                            }
                            zip.CreateEntryFromFile(filename, "PrototypeTests.csv", CompressionLevel.Fastest);
                        }
                        return zipStream.ToArray();
                    }
                }
                catch (Exception ex)
                {
                    throw new ExportingException("Error in exporting product images.", ex);
                }

            }, participantSurveys);
        }

        /// <summary>
        /// <para>
        /// Export the survey participant credentials to the CSV file.
        /// </para>
        /// </summary>
        /// 
        /// <param name="credentials">The participant credentials.</param>
        /// 
        /// <returns> The file content. </returns>
        /// 
        /// <exception cref="WebFaultException{ServiceFaultDetail}">
        /// If any error occurred <see cref="ServiceFaultDetail"/> will be created from:
        /// <para>
        /// <list type="bullet">
        /// <item><see cref="ArgumentNullException"/> 
        /// If <paramref name="credentials"/> is <c>null</c>
        /// </item>
        /// <item><see cref="ExportingException"/>If any other errors occur while performing this operation.</item>
        /// </list></para></exception>
        public byte[] ExportParticipantCredentials(IList<ParticipantCredentials> credentials)
        {
            return Helper.LoggingWrapper(Logger, () =>
             {
                 Helper.CheckNotNull(credentials, "credentials");
                 try
                 {
                     using (var memoryStream = new MemoryStream())
                     {
                         using (var streamWriter = new StreamWriter(memoryStream))
                         {
                             using (var csvWriter = new CsvWriter(streamWriter))
                             {
                                 foreach (var pc in credentials)
                                 {
                                     csvWriter.WriteField(pc.Id);
                                     csvWriter.WriteField(pc.SurveyId);
                                     csvWriter.WriteField(pc.Username);
                                     csvWriter.WriteField(pc.Password);
                                     csvWriter.WriteField(pc.Used);
                                     csvWriter.NextRecord();
                                 }
                                 streamWriter.Flush();
                                 memoryStream.Position = 0;
                             }
                         }
                         return memoryStream.ToArray();
                     }
                 }
                 catch (Exception ex)
                 {
                     throw new ExportingException("Error in exporting participant credentials.", ex);
                 }
             }, credentials);
        }
    }
}

