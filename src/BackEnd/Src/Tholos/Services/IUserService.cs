/*
*  Copyright (c) 2015, TopCoder, Inc. All rights reserved.
*/

using System;
using System.Security.Permissions;
using System.ServiceModel;
using System.ServiceModel.Web;
using Tholos.Entities;

namespace Tholos.Services
{
    /// <summary>
    /// <para>
    /// This service is used to define the contract for the user related operations.
    /// </para>
    /// </summary>
    /// 
    /// <remarks>
    /// This interface has <see cref="ServiceContractAttribute"/> attribute and all its methods are exposed
    /// with <c>OperationContract</c>, <c>TransactionFlow(TransactionFlowOption.Allowed)</c> and
    /// <c>FaultContract(typeof(ServiceFaultDetail))</c> attributes.
    /// </remarks>
    /// 
    /// <threadsafety>
    /// Implementations of this interface should be effectively thread safe.
    /// </threadsafety>
    /// 
    /// <author>veshu</author>
    /// 
    /// <version>1.0</version>
    /// 
    /// <copyright>Copyright (c) 2015, TopCoder, Inc. All rights reserved.</copyright>
    [ServiceContract(SessionMode = SessionMode.Allowed)]
    public interface IUserService
    {
        /// <summary>
        /// <para>
        /// Get Users by given user name.
        /// </para>
        /// </summary>
        /// <param name="username">The username.</param>
        /// <returns>The user.</returns>
        /// 
        /// <exception cref="WebFaultException{T}">
        /// If any error occurred <see cref="ServiceFaultDetail"/> will be created from:
        /// <para>
        /// <list type="bullet">
        /// <item><see cref="ArgumentNullException"/> If <paramref name="username"/> is <c>null</c>.</item>
        /// <item><see cref="ArgumentException"/> If <paramref name="username"/> is empty.</item>
        /// <item><see cref="PersistenceException"/> If a DB-based error occurs.</item>
        /// <item><see cref="TholosException"/> If any other errors occur
        /// while performing this operation.</item>
        /// </list>
        /// </para>
        /// </exception>
        [OperationContract]
        [TransactionFlow(TransactionFlowOption.Allowed)]
        [FaultContract(typeof(ServiceFaultDetail))]
        [PrincipalPermission(SecurityAction.Demand, Role = "Manager")]
        [WebInvoke(Method = "GET", UriTemplate = "/Users/Params?username={username}",
            RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        User GetUser(string username);
    }
}

