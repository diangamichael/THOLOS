/*
*  Copyright (c) 2015, TopCoder, Inc. All rights reserved.
*/

using System;
using System.Collections.Generic;
using System.IO;
using System.ServiceModel;
using System.ServiceModel.Web;
using Tholos.Entities;

namespace Tholos.Services
{
    /// <summary>
    /// <para>
    /// This service is used to define the contract for the participant survey related operations.
    /// </para>
    /// </summary>
    /// 
    /// <remarks>
    /// This interface has <see cref="ServiceContractAttribute"/> attribute and all its methods are exposed
    /// with <c>OperationContract</c>, <c>TransactionFlow(TransactionFlowOption.Allowed)</c> and
    /// <c>FaultContract(typeof(ServiceFaultDetail))</c> attributes.
    /// </remarks>
    /// 
    /// <threadsafety>
    /// Implementations of this interface should be effectively thread safe.
    /// </threadsafety>
    /// 
    /// <author>veshu</author>
    /// 
    /// <version>1.0</version>
    /// 
    /// <copyright>Copyright (c) 2015, TopCoder, Inc. All rights reserved.</copyright>
    [ServiceContract(SessionMode = SessionMode.Allowed)]
    [ServiceKnownType(typeof(RangeAnswer))]
    [ServiceKnownType(typeof(SingleAnswer))]
    [ServiceKnownType(typeof(SingleInputAnswer))]
    public interface IParticipantSurveyService
    {
        ///  <summary>
        ///  Gest the photo and saves in upload file with unique name which will later 
        ///  renamed while creating parcipant survey.
        ///  </summary>
        /// <para>THis file name must send when creating prototype or update.</para>
        /// <param name="fileName">The file name used to get the extension.</param>
        /// <param name="photo">The photo.</param>
        /// 
        ///  <returns>The unique file name.</returns>
        /// 
        ///  <exception cref="WebFaultException{T}">
        ///  If any error occurred <see cref="ServiceFaultDetail"/> will be created from:
        ///  <para>
        ///  <list type="bullet">
        ///  <item><see cref="ArgumentNullException"/>If <paramref name="photo"/> is <c>null</c>.</item>
        ///  <item><see cref="TholosException"/> If any other errors occur while performing this operation.</item>
        ///  </list> </para> </exception>
        [OperationContract]
        [TransactionFlow(TransactionFlowOption.Allowed)]
        [FaultContract(typeof(ServiceFaultDetail))]
        [WebInvoke(Method = "POST", UriTemplate = "/ParticipantSurveys/UploadPhoto?fileName={fileName}",
            BodyStyle = WebMessageBodyStyle.Bare,
            RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        string UploadPhoto(string fileName, Stream photo);

        /// <summary>
        /// Gets the participant survey fully populated with the answers.
        /// </summary>
        ///
        /// <param name="participantSurveyId">The participant survey id.</param>
        ///
        /// <returns>The participant survey.</returns>
        ///
        /// <exception cref="WebFaultException{T}">
        /// If any error occurred <see cref="ServiceFaultDetail"/> will be created from:
        /// <para>
        /// <list type="bullet">
        /// <item><see cref="ArgumentNullException"/>If <paramref name="participantSurveyId"/> is <c>null</c>.</item>
        /// <item><see cref="ArgumentException"/> If <paramref name="participantSurveyId"/> is not valid positive
        ///  number.</item>
        /// <item><see cref="PersistenceException"/> If a DB-based error occurs.</item>
        /// <item><see cref="TholosException"/> If any other errors occur
        /// while performing this operation.</item>
        /// </list>
        /// </para>
        /// </exception>
        [OperationContract]
        [TransactionFlow(TransactionFlowOption.Allowed)]
        [FaultContract(typeof(ServiceFaultDetail))]
        [WebInvoke(Method = "GET", UriTemplate = "/ParticipantSurveys/{participantSurveyId}",
            RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ParticipantSurvey GetParticipantSurvey(string participantSurveyId);

        ///  <summary>
        ///  Creates the prototype test.
        ///  </summary>
        ///  <remarks> Uploads the before photo to the given path and persists its uri path.</remarks>
        ///  <param name="participantSurveyId">The participant survey id.</param>
        ///  <param name="prototypeTest">The prototype test.</param>
        ///  <param name="beforePhotoName">The before photo name.</param>
        ///
        ///  <returns>The participant survey.</returns>
        /// 
        ///  <exception cref="WebFaultException{ServiceFaultDetail}">
        ///  If any error occurred <see cref="ServiceFaultDetail"/> will be created from:
        ///  <para>
        ///  <list type="bullet">
        ///  <item><see cref="ArgumentNullException"/> 
        ///  If <paramref name="participantSurveyId"/> or <paramref name="beforePhotoName"/>  or 
        ///     <paramref name="prototypeTest"/> is <c>null</c>.</item>
        ///  <item><see cref="ArgumentException"/>
        ///  If <paramref name="participantSurveyId"/> is not valid positive number.</item>
        ///  <item><see cref="PersistenceException"/> If a DB-based error occurs.</item>
        ///  <item><see cref="TholosException"/> If any other errors occur while performing this operation.</item>
        ///  </list> </para>  </exception>
        [OperationContract]
        [TransactionFlow(TransactionFlowOption.Allowed)]
        [FaultContract(typeof(ServiceFaultDetail))]
        [WebInvoke(Method = "POST",
            UriTemplate = "/ParticipantSurveys/{participantSurveyId}/PrototypeTests?beforePhotoName={beforePhotoName}",
            RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        long CreatePrototypeTest(string participantSurveyId, PrototypeTest prototypeTest, string beforePhotoName);

        ///  <summary>
        ///  Gets the prototype test.
        ///  </summary>
        /// 
        ///  <param name="prototypeTestId">The prototype test id.</param>
        ///  <returns>The prototype test.</returns>
        /// 
        ///  <exception cref="WebFaultException{ServiceFaultDetail}">
        ///  If any error occurred <see cref="ServiceFaultDetail"/> will be created from:
        ///  <para>
        ///  <list type="bullet">
        ///  <item><see cref="ArgumentNullException"/> If
        ///  <paramref name="prototypeTestId"/> is <c>null</c>.</item>
        ///  <item><see cref="ArgumentException"/> If
        ///  <paramref name="prototypeTestId"/> is not valid positive number.</item>
        ///  <item><see cref="PersistenceException"/> If a DB-based error occurs.</item>
        ///  <item><see cref="TholosException"/> If any other errors occur while performing this operation.</item>
        ///  </list> </para> </exception>
        [OperationContract]
        [TransactionFlow(TransactionFlowOption.Allowed)]
        [FaultContract(typeof(ServiceFaultDetail))]
        [WebGet(UriTemplate = "/PrototypeTests/{prototypeTestId}",
            RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        PrototypeTest GetPrototypeTest(string prototypeTestId);

        ///  <summary>
        ///  Creates the prototype answer.
        ///  </summary>
        /// 
        ///  <param name="prototypeTestId">The participant test id.</param>
        ///  <param name="answer">The answer.</param>
        ///  <returns>The answer id.</returns>
        /// 
        ///  <exception cref="WebFaultException{ServiceFaultDetail}">
        ///  If any error occurred <see cref="ServiceFaultDetail"/> will be created from:
        ///  <para>
        ///  <list type="bullet">
        ///  <item><see cref="ArgumentNullException"/> 
        ///  If <paramref name="prototypeTestId"/> or <paramref name="answer"/> is <c>null</c>.</item>
        ///  <item>
        ///  <see cref="ArgumentException"/>
        ///  If <paramref name="prototypeTestId"/> is not valid positive number.</item>
        ///  <item><see cref="EntityNotFoundException"/> If the entity does not exist.</item>
        ///  <item><see cref="PersistenceException"/> If a DB-based error occurs.</item>
        ///  <item><see cref="TholosException"/> If any other errors occur while performing this operation.</item>
        ///  </list> </para> </exception>
        [OperationContract]
        [TransactionFlow(TransactionFlowOption.Allowed)]
        [FaultContract(typeof(ServiceFaultDetail))]
        [WebInvoke(Method = "POST", UriTemplate = "/PrototypeTests/{prototypeTestId}/PrototypeAnswers"
            , RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        long SubmitPrototypeAnswer(string prototypeTestId, Answer answer);

        ///  <summary>
        ///  Updates the prototype answer.
        ///  </summary>
        /// 
        ///  <param name="answer">The prototype answer.</param>
        /// 
        ///  <exception cref="WebFaultException{ServiceFaultDetail}">
        ///  If any error occurred <see cref="ServiceFaultDetail"/> will be created from:
        ///  <para>
        ///  <list type="bullet">
        ///  <item><see cref="ArgumentNullException"/> If <paramref name="answer"/> is <c>null</c>.</item>
        ///  <item><see cref="EntityNotFoundException"/> If the entity does not exist.</item>
        ///  <item><see cref="PersistenceException"/> If a DB-based error occurs.</item>
        ///  <item><see cref="TholosException"/> If any other errors occur while performing this operation.</item>
        ///  </list> </para> </exception>
        [OperationContract]
        [TransactionFlow(TransactionFlowOption.Allowed)]
        [FaultContract(typeof(ServiceFaultDetail))]
        [WebInvoke(Method = "PUT", UriTemplate = "/PrototypeAnswers",
            RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        void UpdatePrototypeAnswer(Answer answer);

        ///  <summary>
        ///  Complete the prototype test. Uploads the after photo to the given path and persists its uri path.
        ///  </summary>
        /// <param name="prototypeTestId">The prototype test id.</param>
        /// <param name="fileName">The file name used to get the extension of photo.</param>
        /// <param name="afterWeight">The after weight.</param>
        /// <param name="afterPhoto">The after photo.</param>
        /// <exception cref="WebFaultException{ServiceFaultDetail}">
        ///  If any error occurred <see cref="ServiceFaultDetail"/> will be created from:
        ///  <para>
        ///  <list type="bullet">
        ///  <item>
        /// <see cref="ArgumentNullException"/> 
        ///   If <paramref name="prototypeTestId"/> or <paramref name="afterPhoto"/> is <c>null</c>.</item>
        ///  <item>
        /// <see cref="ArgumentException"/> 
        ///  If <paramref name="prototypeTestId"/> is not valid positive number.</item>
        ///  <item><see cref="EntityNotFoundException"/> If the entity does not exist.</item>
        ///  <item><see cref="PersistenceException"/> If a DB-based error occurs.</item>
        ///  <item><see cref="TholosException"/> If any other errors occur
        ///  while performing this operation.</item>
        ///  </list>
        ///  </para>
        ///  </exception>
        [OperationContract]
        [TransactionFlow(TransactionFlowOption.Allowed)]
        [FaultContract(typeof(ServiceFaultDetail))]
        [WebInvoke(Method = "PUT",
            UriTemplate = "/PrototypeTests/{prototypeTestId}/Complete?fileName={fileName}&afterWeight={afterWeight}"
            , BodyStyle = WebMessageBodyStyle.Bare,
            ResponseFormat = WebMessageFormat.Json)]
        void CompletePrototypeTest(string prototypeTestId, string fileName, string afterWeight, Stream afterPhoto);

        ///  <summary>
        ///  Submits the post testing answer.
        ///  </summary>
        /// 
        ///  <param name="participantSurveyId">The participant survey id.</param>
        ///  <param name="postTestingAnswer">The answer.</param>
        /// 
        ///  <exception cref="WebFaultException{ServiceFaultDetail}">
        ///  If any error occurred <see cref="ServiceFaultDetail"/> will be created from:
        ///  <para>
        ///  <list type="bullet">
        ///  <item><see cref="ArgumentNullException"/> If <paramref name="participantSurveyId"/> or
        ///  <paramref name="postTestingAnswer"/> is <c>null</c>.</item>
        ///  <item><see cref="ArgumentException"/> 
        ///     If <paramref name="participantSurveyId"/> is not valid positive number.</item>
        ///  <item><see cref="EntityNotFoundException"/> If the entity does not exist.</item>
        ///  <item><see cref="PersistenceException"/> If a DB-based error occurs.</item>
        ///  <item><see cref="TholosException"/> If any other errors occur while performing this operation.</item>
        ///  </list> </para> </exception>
        [OperationContract]
        [TransactionFlow(TransactionFlowOption.Allowed)]
        [FaultContract(typeof(ServiceFaultDetail))]
        [WebInvoke(Method = "POST", UriTemplate = "/ParticipantSurveys/{participantSurveyId}/PostTestingAnswers"
            , RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        void SubmitPostTestingAnswer(string participantSurveyId, Answer postTestingAnswer);

        /// <summary>
        /// Completes the post testing.
        /// </summary>
        ///
        /// <param name="participantSurveyId">The participant survey id.</param>
        ///
        /// <exception cref="WebFaultException{ServiceFaultDetail}">
        /// If any error occurred <see cref="ServiceFaultDetail"/> will be created from:
        /// <para>
        /// <list type="bullet">
        /// <item><see cref="ArgumentNullException"/>If <paramref name="participantSurveyId"/> is <c>null</c>.</item>
        /// <item><see cref="ArgumentException"/> 
        /// If <paramref name="participantSurveyId"/> is not valid positive number.</item>
        /// <item><see cref="EntityNotFoundException"/> If the entity does not exist.</item>
        /// <item><see cref="PersistenceException"/> If a DB-based error occurs.</item>
        /// <item><see cref="TholosException"/> If any other errors occur
        /// while performing this operation.</item>
        /// </list>
        /// </para>
        /// </exception>
        [OperationContract]
        [TransactionFlow(TransactionFlowOption.Allowed)]
        [FaultContract(typeof(ServiceFaultDetail))]
        [WebInvoke(Method = "PUT", UriTemplate = "/ParticipantSurveys/{participantSurveyId}/Complete"
             , RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        void CompletePostTesting(string participantSurveyId);

        /// <summary>
        /// Gets the prototype questions fully populated with the answer options.
        /// </summary>
        ///
        /// <param name="participantSurveyId">The participant survey id.</param>
        ///
        /// <returns>The prototype questions.</returns>
        ///
        /// <exception cref="WebFaultException{ServiceFaultDetail}">
        /// If any error occurred <see cref="ServiceFaultDetail"/> will be created from:
        /// <para>
        /// <list type="bullet">
        /// <item><see cref="ArgumentNullException"/>If <paramref name="participantSurveyId"/> is <c>null</c>.</item>
        /// <item><see cref="ArgumentException"/> 
        /// If <paramref name="participantSurveyId"/> is not valid positive number.</item>
        /// <item><see cref="PersistenceException"/> If a DB-based error occurs.</item>
        /// <item><see cref="TholosException"/> If any other errors occur
        /// while performing this operation.</item>
        /// </list>
        /// </para>
        /// </exception>
        [OperationContract]
        [TransactionFlow(TransactionFlowOption.Allowed)]
        [FaultContract(typeof(ServiceFaultDetail))]
        [WebInvoke(Method = "GET", UriTemplate = "/ParticipantSurveys/{participantSurveyId}/PrototypeQuestions"
            , RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        IList<Question> GetPrototypeQuestions(string participantSurveyId);

        /// <summary>
        /// Gets the post testing questions fully populated with the answer options.
        /// </summary>
        ///
        /// <param name="participantSurveyId">The participant survey id.</param>
        ///
        /// <returns>The post testing questions.</returns>
        ///
        /// <exception cref="WebFaultException{ServiceFaultDetail}">
        /// If any error occurred <see cref="ServiceFaultDetail"/> will be created from:
        /// <para>
        /// <list type="bullet">
        /// <item><see cref="ArgumentNullException"/>If <paramref name="participantSurveyId"/> is <c>null</c>.</item>
        /// <item><see cref="ArgumentException"/> If <paramref name="participantSurveyId"/> is not valid positive
        ///  number.</item>
        /// <item><see cref="PersistenceException"/> If a DB-based error occurs.</item>
        /// <item><see cref="TholosException"/> If any other errors occur
        /// while performing this operation.</item>
        /// </list>
        /// </para>
        /// </exception>
        [OperationContract]
        [TransactionFlow(TransactionFlowOption.Allowed)]
        [FaultContract(typeof(ServiceFaultDetail))]
        [WebInvoke(Method = "GET", UriTemplate = "/ParticipantSurveys/{participantSurveyId}/PostTestingQuestions"
            , RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        IList<Question> GetPostTestingQuestions(string participantSurveyId);

        /// <summary>
        /// Gets the survey prototype codes.
        /// </summary>
        ///
        /// <param name="surveyId">The survey id.</param>
        ///
        /// <returns>The prototype codes.</returns>
        ///
        /// <exception cref="WebFaultException{ServiceFaultDetail}">
        /// If any error occurred <see cref="ServiceFaultDetail"/> will be created from:
        /// <para>
        /// <list type="bullet">
        /// <item><see cref="ArgumentNullException"/>If <paramref name="surveyId"/> is <c>null</c>.</item>
        /// <item><see cref="ArgumentException"/> If <paramref name="surveyId"/> is not valid positive number.</item>
        /// <item><see cref="PersistenceException"/> If a DB-based error occurs.</item>
        /// <item><see cref="TholosException"/> If any other errors occur
        /// while performing this operation.</item>
        /// </list>
        /// </para>
        /// </exception>
        [OperationContract]
        [TransactionFlow(TransactionFlowOption.Allowed)]
        [FaultContract(typeof(ServiceFaultDetail))]
        [WebInvoke(Method = "GET", UriTemplate = "/Surveys/{SurveyId}/PrototypeCodes"
           , RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        IList<String> GetSurveyPrototypeCodes(string surveyId);
    }
}

