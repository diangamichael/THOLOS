/*
*  Copyright (c) 2015, TopCoder, Inc. All rights reserved.
*/


using System;
using System.Collections.Generic;
using System.ServiceModel.Web;
using Tholos.Entities;

namespace Tholos.Services
{
    /// <summary>
    /// <para>
    /// This service is used to define the contract for the export operation.
    /// </para>
    /// </summary>
    /// 
    /// <threadsafety>
    /// Implementations of this interface should be effectively thread safe.
    /// </threadsafety>
    /// 
    /// <author>veshu</author>
    /// 
    /// <version>1.0</version>
    /// 
    /// <copyright>Copyright (c) 2015, TopCoder, Inc. All rights reserved.</copyright>
    public interface IExportService
    {
        /// <summary>
        /// <para>
        ///  Export the survey results to the CSV file.
        /// </para>
        /// </summary>
        /// 
        /// <param name="participantSurveys">The participant surveys.</param>
        /// 
        /// <returns> The file content. </returns>
        /// 
        /// <exception cref="WebFaultException{T}">
        /// If any error occurred <see cref="ServiceFaultDetail"/> will be created from:
        /// <para>
        /// <list type="bullet">
        /// <item><see cref="ArgumentNullException"/> 
        /// If <paramref name="participantSurveys"/> is <c>null</c>
        /// </item>
        /// <item><see cref="ExportingException"/>If any other errors occur while performing this operation.</item>
        /// </list></para></exception>
        byte[] ExportSurveyResults(IList<ParticipantSurvey> participantSurveys);

        /// <summary>
        /// <para>
        /// Export the product images along with the CSV description file to the zip and return its content.
        /// </para>
        /// </summary>
        /// 
        /// <param name="participantSurveys">The participant surveys.</param>
        /// 
        /// <returns> The file content. </returns>
        /// 
        /// <exception cref="WebFaultException{ServiceFaultDetail}">
        /// If any error occurred <see cref="ServiceFaultDetail"/> will be created from:
        /// <para>
        /// <list type="bullet">
        /// <item><see cref="ArgumentNullException"/> 
        /// If <paramref name="participantSurveys"/> is <c>null</c>
        /// </item>
        /// <item><see cref="ExportingException"/>If any other errors occur while performing this operation.</item>
        /// </list></para></exception>
        byte[] ExportProductImages(IList<ParticipantSurvey> participantSurveys);

        /// <summary>
        /// <para>
        /// Export the survey participant credentials to the CSV file.
        /// </para>
        /// </summary>
        /// 
        /// <param name="credentials">The participant credentials.</param>
        /// 
        /// <returns> The file content. </returns>
        /// 
        /// <exception cref="WebFaultException{ServiceFaultDetail}">
        /// If any error occurred <see cref="ServiceFaultDetail"/> will be created from:
        /// <para>
        /// <list type="bullet">
        /// <item><see cref="ArgumentNullException"/> 
        /// If <paramref name="credentials"/> is <c>null</c>
        /// </item>
        /// <item><see cref="ExportingException"/>If any other errors occur while performing this operation.</item>
        /// </list></para></exception>
        byte[] ExportParticipantCredentials(IList<ParticipantCredentials> credentials);
    }
}

