/*
*  Copyright (c) 2015, TopCoder, Inc. All rights reserved.
*/

using System;
using System.ServiceModel.Web;

namespace Tholos.Services
{
    /// <summary>
    /// <para>
    /// This service is used to define the contract for the authentication operation.
    /// </para>
    /// </summary>
    /// 
    /// <threadsafety>
    /// Implementations of this interface should be effectively thread safe.
    /// </threadsafety>
    /// 
    /// <author>veshu</author>
    /// 
    /// <version>1.0</version>
    /// 
    /// <copyright>Copyright (c) 2015, TopCoder, Inc. All rights reserved.</copyright>
    public interface IAuthenticationService
    {
        /// <summary>
        /// <para>
        /// Performs authentication.
        /// </para>
        /// </summary>
        /// <remarks>It will check for both - Manager and Participant authentication.
        ///  Manager password will be hashed before checking.</remarks>
        /// 
        /// <param name="username">The username.</param>
        /// <param name="password">The password.</param>
        /// 
        /// <returns> <c>true</c> if authenticated, <c>false</c> otherwise. </returns>
        /// 
        /// <exception cref="WebFaultException{T}">
        /// If any error occurred <see cref="ServiceFaultDetail"/> will be created from:
        /// <para>
        /// <list type="bullet">
        /// <item><see cref="ArgumentNullException"/> 
        /// If <paramref name="username"/> or <paramref name="password"/> is <c>null</c>
        /// </item>
        /// <item><see cref="ArgumentException"/>
        /// If <paramref name="username"/> or <paramref name="password"/> is empty.</item>
        /// <item><see cref="PersistenceException"/>If a DB-based error occurs. </item>
        /// <item><see cref="TholosException"/>If any other errors occur while performing this operation.</item>
        /// </list></para></exception>
        bool Authenticate(string username, string password);
    }
}

