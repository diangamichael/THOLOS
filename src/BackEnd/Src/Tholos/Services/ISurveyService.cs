/*
*  Copyright (c) 2015, TopCoder, Inc. All rights reserved.
*/

using System;
using System.Collections.Generic;
using System.IO;
using System.ServiceModel;
using System.ServiceModel.Web;
using Tholos.Entities;

namespace Tholos.Services
{
    /// <summary>
    /// <para>
    /// This service is used to define the contract for the survey related operations.
    /// </para>
    /// </summary>
    /// 
    /// <remarks>
    /// This interface has <see cref="ServiceContractAttribute"/> attribute and all its methods are exposed
    /// with <c>OperationContract</c>, <c>TransactionFlow(TransactionFlowOption.Allowed)</c> and
    /// <c>FaultContract(typeof(ServiceFaultDetail))</c> attributes.
    /// </remarks>
    /// 
    /// <threadsafety>
    /// Implementations of this interface should be effectively thread safe.
    /// </threadsafety>
    /// 
    /// <author>veshu</author>
    /// 
    /// <version>1.0</version>
    /// 
    /// <copyright>Copyright (c) 2015, TopCoder, Inc. All rights reserved.</copyright>
    [ServiceContract(SessionMode = SessionMode.Allowed)]
    [ServiceKnownType(typeof(RangeAnswer))]
    [ServiceKnownType(typeof(SingleAnswer))]
    [ServiceKnownType(typeof(SingleInputAnswer))]
    [ServiceKnownType(typeof(RangeAnswerOption))]
    [ServiceKnownType(typeof(SingleAnswerOption))]
    [ServiceKnownType(typeof(SingleAnswerInputOption))]
    public interface ISurveyService
    {
        /// <summary>
        /// <para>
        /// Gets the prototype questions fully populated with the answer options.
        /// </para>
        /// </summary>
        /// <returns>The prototype questions.</returns>
        /// 
        /// <exception cref="WebFaultException{T}">
        /// If any error occurred <see cref="ServiceFaultDetail"/> will be created from:
        /// <para>
        /// <list type="bullet">
        /// <item><see cref="PersistenceException"/> If a DB-based error occurs.</item>
        /// <item><see cref="TholosException"/> If any other errors occur while performing this operation.</item>
        /// </list>
        /// </para>
        /// </exception>
        [OperationContract]
        [TransactionFlow(TransactionFlowOption.Allowed)]
        [FaultContract(typeof(ServiceFaultDetail))]
        [WebGet(UriTemplate = "/PrototypeQuestions",
            RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        IList<Question> GetAllPrototypeQuestions();

        /// <summary>
        /// <para>
        /// Gets the post testing questions fully populated with the answer options.
        /// </para>
        /// </summary>
        /// <returns>The post testing questions.</returns>
        /// 
        /// <exception cref="WebFaultException{ServiceFaultDetail}">
        /// If any error occurred <see cref="ServiceFaultDetail"/> will be created from:
        /// <para>
        /// <list type="bullet">
        /// <item><see cref="PersistenceException"/> If a DB-based error occurs.</item>
        /// <item><see cref="TholosException"/> If any other errors occur while performing this operation.</item>
        /// </list>
        /// </para>
        /// </exception>
        [OperationContract]
        [TransactionFlow(TransactionFlowOption.Allowed)]
        [FaultContract(typeof(ServiceFaultDetail))]
        [WebGet(UriTemplate = "/PostTestingQuestions",
            RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        IList<Question> GetAllPostTestingQuestions();

        /// <summary>
        /// <para>
        /// Creates the survey.
        /// </para>
        /// </summary>
        /// <remarks><para>Generates the credentials for the parcipants and persist it.
        /// It is supposed that only question ids are passed for the survey's prototype and post testing questions.
        /// </para></remarks>
        /// 
        /// <param name="survey">The survey.</param>
        /// <param name="userId">The id of the manager who creates the survey.</param>
        /// <returns>The id of created survey.</returns>
        /// 
        /// <exception cref="WebFaultException{ServiceFaultDetail}">
        /// If any error occurred <see cref="ServiceFaultDetail"/> will be created from:
        /// <para>
        /// <list type="bullet">
        /// <item><see cref="ArgumentNullException"/> If <paramref name="survey"/> is <c>null</c>.</item>
        /// <item><see cref="ArgumentException"/> If <paramref name="userId"/> is negative.</item>
        /// <item><see cref="PersistenceException"/> If a DB-based error occurs.</item>
        /// <item><see cref="TholosException"/> If any other errors occur while performing this operation.</item>
        /// </list>
        /// </para>
        /// </exception>
        [OperationContract]
        [TransactionFlow(TransactionFlowOption.Allowed)]
        [FaultContract(typeof(ServiceFaultDetail))]
        [WebInvoke(Method = "POST", UriTemplate = "/Surveys/Create?userId={userId}",
            RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        long CreateSurvey(Survey survey, long userId);

        /// <summary>
        /// <para>
        /// Publishes the survey. Updates the survey status and draft flag.
        /// </para>
        /// </summary>
        /// 
        /// <param name="surveyId">The survey id.</param>
        /// 
        /// <exception cref="WebFaultException{ServiceFaultDetail}">
        /// If any error occurred <see cref="ServiceFaultDetail"/> will be created from:
        /// <para>
        /// <list type="bullet">
        /// <item><see cref="ArgumentException"/> If <paramref name="surveyId"/> is not valid positive number.</item>
        /// <item><see cref="EntityNotFoundException"/>
        /// If If the entity does not exist with given <paramref name="surveyId"/>.</item>
        /// <item><see cref="PersistenceException"/> If a DB-based error occurs.</item>
        /// <item><see cref="TholosException"/> If any other errors occur while performing this operation.</item>
        /// </list>
        /// </para>
        /// </exception>
        [OperationContract]
        [TransactionFlow(TransactionFlowOption.Allowed)]
        [FaultContract(typeof(ServiceFaultDetail))]
        [WebInvoke(Method = "PUT", UriTemplate = "/Surveys/{surveyId}/Publish",
            RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        void PublishSurvey(string surveyId);

        /// <summary>
        /// <para>
        /// Updates the survey. All dependent entities are updated.
        /// </para>
        /// </summary>
        /// 
        /// <param name="survey">The survey.</param>
        /// 
        /// <exception cref="WebFaultException{ServiceFaultDetail}">
        /// If any error occurred <see cref="ServiceFaultDetail"/> will be created from:
        /// <para>
        /// <list type="bullet">
        /// <item><see cref="ArgumentNullException"/> If <paramref name="survey"/> is <c>null</c>.</item>
        /// <item><see cref="EntityNotFoundException"/>
        /// If If the entity does not exist with given <paramref name="survey"/>.</item>
        /// <item><see cref="PersistenceException"/> If a DB-based error occurs.</item>
        /// <item><see cref="TholosException"/> If any other errors occur while performing this operation.</item>
        /// </list>
        /// </para>
        /// </exception>
        [OperationContract]
        [TransactionFlow(TransactionFlowOption.Allowed)]
        [FaultContract(typeof(ServiceFaultDetail))]
        [WebInvoke(Method = "PUT", UriTemplate = "/Surveys",
             RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        void UpdateSurvey(Survey survey);

        /// <summary>
        /// <para>
        /// Gets the survey fully populated with the questions and answer options.
        /// </para>
        /// </summary>
        /// 
        /// <param name="surveyId">The survey id.</param>
        /// <returns>The survey.</returns>
        /// 
        /// <exception cref="WebFaultException{ServiceFaultDetail}">
        /// If any error occurred <see cref="ServiceFaultDetail"/> will be created from:
        /// <para>
        /// <list type="bullet">
        /// <item><see cref="ArgumentException"/> If <paramref name="surveyId"/> is negative.</item>
        /// <item><see cref="PersistenceException"/> If a DB-based error occurs.</item>
        /// <item><see cref="TholosException"/> If any other errors occur while performing this operation.</item>
        /// </list>
        /// </para>
        /// </exception>
        [OperationContract]
        [TransactionFlow(TransactionFlowOption.Allowed)]
        [FaultContract(typeof(ServiceFaultDetail))]
        [WebInvoke(Method = "GET", UriTemplate = "/Surveys/{surveyId}",
            RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        Survey GetSurvey(string surveyId);

        /// <summary>
        /// <para>
        /// Updates the survey status.
        /// </para>
        /// </summary>
        /// 
        /// <param name="surveyId">The survey id.</param>
        /// <param name="status">The survey status.</param>
        /// 
        /// <exception cref="WebFaultException{ServiceFaultDetail}">
        /// If any error occurred <see cref="ServiceFaultDetail"/> will be created from:
        /// <para>
        /// <list type="bullet">
        /// <item><see cref="ArgumentNullException"/> If <paramref name="status"/> is <c>null</c>.</item>
        /// <item><see cref="ArgumentException"/> If <paramref name="surveyId"/> is not valid positive number.</item>
        /// <item><see cref="EntityNotFoundException"/>
        /// If If the entity does not exist with given <paramref name="surveyId"/>.</item>
        /// <item><see cref="PersistenceException"/> If a DB-based error occurs.</item>
        /// <item><see cref="TholosException"/> If any other errors occur while performing this operation.</item>
        /// </list>
        /// </para>
        /// </exception>
        [OperationContract]
        [TransactionFlow(TransactionFlowOption.Allowed)]
        [FaultContract(typeof(ServiceFaultDetail))]
        [WebInvoke(Method = "PUT", UriTemplate = "/Surveys/{surveyId}/UpdateSurvey?status={status}",
            RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        void UpdateSurveyStatus(string surveyId, SurveyStatus status);

        /// <summary>
        /// <para>
        /// Searches the surveys. Survey quetsions are not loaded.
        /// </para>
        /// </summary>
        /// 
        /// <param name="name">The survey name.</param>
        /// <param name="filter">The search criteria.</param>
        /// <returns>The survery search results.</returns>
        /// 
        /// <exception cref="WebFaultException{ServiceFaultDetail}">
        /// If any error occurred <see cref="ServiceFaultDetail"/> will be created from:
        /// <para>
        /// <list type="bullet">
        /// <item><see cref="ArgumentNullException"/> If <paramref name="name"/> or <paramref name="filter"/> 
        /// is <c>null</c>.</item>
        /// <item><see cref="ArgumentException"/>If <paramref name="name"/> is empty or 
        /// If <c>filter.PageNumber</c> is negative,
        /// or <c>filter.PageNumber</c> is positive and <c>filter.PageSize</c> is not positive,
        /// or if <c>filter.SortColumn</c> is not a valid sort column.</item>
        /// <item><see cref="PersistenceException"/> If a DB-based error occurs.</item>
        /// <item><see cref="TholosException"/> If any other errors occur while performing this operation.</item>
        /// </list>
        /// </para>
        /// </exception>
        [OperationContract]
        [TransactionFlow(TransactionFlowOption.Allowed)]
        [FaultContract(typeof(ServiceFaultDetail))]
        [WebInvoke(Method = "POST", UriTemplate = "/Surveys/search?name={name}"
            , RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        SurveySearchResult SearchSurvey(string name, SearchFilter filter);

        /// <summary>
        /// <para>
        /// Get all surveys. Only data from the Survey tablse is loaded, questions are not loaded.
        /// </para>
        /// </summary>
        /// 
        /// <param name="filter">The search criteria.</param>
        /// <returns>The surveys.</returns>
        /// 
        /// <exception cref="WebFaultException{ServiceFaultDetail}">
        /// If any error occurred <see cref="ServiceFaultDetail"/> will be created from:
        /// <para>
        /// <list type="bullet">
        /// <item><see cref="ArgumentNullException"/> If <paramref name="filter"/>  is <c>null</c>.</item>
        /// <item><see cref="ArgumentException"/>If <c>filter.PageNumber</c> is negative,
        /// or <c>filter.PageNumber</c> is positive and <c>filter.PageSize</c> is not positive,
        /// or if <c>filter.SortColumn</c> is not a valid sort column.</item>
        /// <item><see cref="PersistenceException"/> If a DB-based error occurs.</item>
        /// <item><see cref="TholosException"/> If any other errors occur while performing this operation.</item>
        /// </list>
        /// </para>
        /// </exception>
        [OperationContract]
        [TransactionFlow(TransactionFlowOption.Allowed)]
        [FaultContract(typeof(ServiceFaultDetail))]
        [WebInvoke(Method = "POST", UriTemplate = "/Surveys/GetAll?all=true"
                    , RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        AllSurveysSearchResult GetAllSurveys(SearchFilter filter);

        /// <summary>
        /// <para>
        /// Gets the number of surveys for each survey status for the specific user.
        /// </para>
        /// </summary>
        /// 
        /// <param name="userId">The userId.</param>
        /// <returns>The user survey status.</returns>
        /// 
        /// <exception cref="WebFaultException{ServiceFaultDetail}">
        /// If any error occurred <see cref="ServiceFaultDetail"/> will be created from:
        /// <para>
        /// <list type="bullet">
        /// <item><see cref="ArgumentException"/> If <paramref name="userId"/>  is negative.</item>
        /// <item><see cref="EntityNotFoundException"/> If the user does not exist.</item>
        /// <item><see cref="PersistenceException"/> If a DB-based error occurs.</item>
        /// <item><see cref="TholosException"/> If any other errors occur while performing this operation.</item>
        /// </list>
        /// </para>
        /// </exception>
        [OperationContract]
        [TransactionFlow(TransactionFlowOption.Allowed)]
        [FaultContract(typeof(ServiceFaultDetail))]
        [WebGet(UriTemplate = "/Users/{userId}/SurveyStatus", ResponseFormat = WebMessageFormat.Json)]
        IDictionary<SurveyStatus, int> GetUserSurveyStatus(string userId);

        /// <summary>
        /// <para>
        /// Gets the quick access surveys. Surveys are not fully populated, only id, name and cell name are fetched.
        /// </para>
        /// </summary>
        /// 
        /// <param name="userId">The userId.</param>
        /// <returns>
        /// The quick access surveys in the dictionary where the keys are the positions in the quick access list.
        /// </returns>
        /// 
        /// <exception cref="WebFaultException{ServiceFaultDetail}">
        /// If any error occurred <see cref="ServiceFaultDetail"/> will be created from:
        /// <para>
        /// <list type="bullet">
        /// <item><see cref="ArgumentException"/> If <paramref name="userId"/>  is negative.</item>
        /// <item><see cref="EntityNotFoundException"/> If the user does not exist.</item>
        /// <item><see cref="PersistenceException"/> If a DB-based error occurs.</item>
        /// <item><see cref="TholosException"/> If any other errors occur while performing this operation.</item>
        /// </list>
        /// </para>
        /// </exception>
        [OperationContract]
        [TransactionFlow(TransactionFlowOption.Allowed)]
        [FaultContract(typeof(ServiceFaultDetail))]
        [WebGet(UriTemplate = "/Users/{userId}/QuickAccessSurveys"
            , RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        IList<QuickAccessSurvey> GetQuickAccessSurveys(string userId);

        /// <summary>
        /// <para>
        /// Updates the quick access survey.
        /// </para>
        /// </summary>
        /// <param name="userId">The userId.</param>
        /// <param name="surveys">The mapping between position in the list and survey id.</param>
        /// <exception cref="WebFaultException{ServiceFaultDetail}">
        /// If any error occurred <see cref="ServiceFaultDetail"/> will be created from:
        /// <para>
        /// <list type="bullet">
        /// <item><see cref="ArgumentNullException"/> If <paramref name="surveys"/>  is <c>null</c>.</item>
        /// <item><see cref="ArgumentException"/> If <paramref name="userId"/>  is negative.</item>
        /// <item><see cref="EntityNotFoundException"/> If the user does not exist.</item>
        /// <item><see cref="PersistenceException"/> If a DB-based error occurs.</item>
        /// <item><see cref="TholosException"/> If any other errors occur while performing this operation.</item>
        /// </list>
        /// </para>
        /// </exception>
        [OperationContract]
        [TransactionFlow(TransactionFlowOption.Allowed)]
        [FaultContract(typeof(ServiceFaultDetail))]
        [WebInvoke(Method = "PUT", UriTemplate = "/Users/{userId}/QuickAccessSurveys",
             RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        void UpdateQuickAccessSurveys(string userId, IList<QuickAccessSurvey> surveys);

        /// <summary>
        /// <para>
        /// Export the survey participant credentials to the CSV file.
        /// </para>
        /// </summary>
        /// 
        /// <param name="surveyId">The survey Id.</param>
        /// <returns>
        /// The file stream with the credentials.
        /// </returns>
        /// 
        /// <exception cref="WebFaultException{ServiceFaultDetail}">
        /// If any error occurred <see cref="ServiceFaultDetail"/> will be created from:
        /// <para>
        /// <list type="bullet">
        /// <item><see cref="ArgumentException"/> If <paramref name="surveyId"/>  is negative.</item>
        /// <item><see cref="EntityNotFoundException"/> If the entity does not exist.</item>
        /// <item><see cref="PersistenceException"/> If a DB-based error occurs.</item>
        /// <item><see cref="TholosException"/> If any other errors occur while performing this operation.</item>
        /// </list>
        /// </para>
        /// </exception>
        [OperationContract]
        [TransactionFlow(TransactionFlowOption.Allowed)]
        [FaultContract(typeof(ServiceFaultDetail))]
        [WebGet(UriTemplate = "/Surveys/{surveyId}/ExportParticipantCredentials",
             RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        Stream ExportParticipantCredentials(string surveyId);

        /// <summary>
        /// <para>
        /// Export the survey results to the CSV file.
        /// </para>
        /// </summary>
        /// 
        /// <param name="surveyId">The survey Id.</param>
        /// <returns>
        /// The file stream with the results.
        /// </returns>
        /// 
        /// <exception cref="WebFaultException{ServiceFaultDetail}">
        /// If any error occurred <see cref="ServiceFaultDetail"/> will be created from:
        /// <para>
        /// <list type="bullet">
        /// <item><see cref="ArgumentException"/> If <paramref name="surveyId"/>  is negative.</item>
        /// <item><see cref="EntityNotFoundException"/> If the entity does not exist.</item>
        /// <item><see cref="PersistenceException"/> If a DB-based error occurs.</item>
        /// <item><see cref="TholosException"/> If any other errors occur while performing this operation.</item>
        /// </list>
        /// </para>
        /// </exception>
        [OperationContract]
        [TransactionFlow(TransactionFlowOption.Allowed)]
        [FaultContract(typeof(ServiceFaultDetail))]
        [WebGet(UriTemplate = "/Surveys/{surveyId}/ExportSurveyResults"
            , RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        Stream ExportSurveyResults(string surveyId);

        /// <summary>
        /// <para>
        /// Export the before and after product images for the specific survey. 
        /// </para>
        /// </summary>
        /// <remarks><para>
        /// The result zip file contains the CSV file with the participant username, prototype code 
        /// and the names of the before/after photos.
        /// </para></remarks>
        /// <param name="surveyId">The survey Id.</param>
        /// <returns>
        /// The file stream with the results.
        /// </returns>
        /// 
        /// <exception cref="WebFaultException{ServiceFaultDetail}">
        /// If any error occurred <see cref="ServiceFaultDetail"/> will be created from:
        /// <para>
        /// <list type="bullet">
        /// <item><see cref="ArgumentException"/> If <paramref name="surveyId"/>  is negative.</item>
        /// <item><see cref="EntityNotFoundException"/> If the entity does not exist.</item>
        /// <item><see cref="PersistenceException"/> If a DB-based error occurs.</item>
        /// <item><see cref="TholosException"/> If any other errors occur while performing this operation.</item>
        /// </list>
        /// </para>
        /// </exception>
        [OperationContract]
        [TransactionFlow(TransactionFlowOption.Allowed)]
        [FaultContract(typeof(ServiceFaultDetail))]
        [WebGet(UriTemplate = "/Surveys/{surveyId}/ExportProductImages"
            , RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        Stream ExportProductImages(string surveyId);
    }
}

