/*
*  Copyright (c) 2015, TopCoder, Inc. All rights reserved.
*/

using System.Collections.Generic;
using System.Net;
using System.Security.Principal;
using System.ServiceModel.Web;
using Tholos.Services;

namespace Tholos.Security
{
    /// <summary>
    /// <para>
    /// This is class representing custom principal. It is used in the authorization process.
    /// </para>
    /// </summary>
    ///
    /// <threadsafety>
    /// This class is immutable and thread-safe.
    /// </threadsafety>
    ///
    /// <author>veshu</author>
    /// 
    /// <version>1.0</version>
    /// <copyright>Copyright (c) 2015, TopCoder, Inc. All rights reserved.</copyright>
    public class CustomPrincipal : IPrincipal
    {
        /// <summary>
        ///  Gets or sets the user's identity.
        /// </summary>
        /// <remark>
        /// <para>
        /// It's initialized in constructor and is immutable.
        /// </para>
        /// </remark>
        /// <value>The identity. It should not be null after initialization.</value>
        public IIdentity Identity
        {
            get;
            private set;
        }

        /// <summary>
        ///  Gets or sets the user's roles.
        /// </summary>
        /// <remark>
        /// <para>
        /// It's initialized in constructor and is immutable.
        /// </para>
        /// </remark>
        /// <value>The roles. It should not be null after initialization.</value>
        private IList<string> Roles
        {
            get;
            set;
        }

        /// <summary>
        /// <para>
        /// Initializes a new instance of the <see cref="CustomPrincipal"/> class.
        /// </para>
        /// </summary>
        /// <param name="identity">The  user's identity.</param>
        /// <param name="roles">The user's roles.</param>
        public CustomPrincipal(IIdentity identity, IList<string> roles)
        {
            this.Identity = identity;
            this.Roles = roles;
        }

        /// <summary>
        /// Determines whether the current principal belongs to the specified role.
        /// </summary>
        /// <param name="role">The role.</param>
        /// <returns>
        /// <c>true</c> if the current principal is a member of the specified role;  otherwise, <c>false</c>.
        /// </returns>
        public bool IsInRole(string role)
        {
            if (Roles.Contains(role))
                return true;
            else
            {
                var error = new ServiceFaultDetail
                {
                    ErrorMessage = "Access denied.",
                    ErrorType = "UnauthorizedAccessException"
                };
                throw new WebFaultException<ServiceFaultDetail>(error, HttpStatusCode.Unauthorized);
            }
        }
    }
}

