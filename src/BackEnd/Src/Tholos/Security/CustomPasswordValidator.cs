﻿/*
*  Copyright (c) 2015, TopCoder, Inc. All rights reserved.
*/

using System.IdentityModel.Selectors;
using System.IdentityModel.Tokens;
using log4net;
using Microsoft.Practices.Unity;
using Tholos.Services;

namespace Tholos.Security
{
    /// <summary>
    /// <para>
    /// This class is the custom password validator used to validate the username-password combination.
    /// </para>
    /// </summary>
    ///
    /// <threadsafety>
    /// This class is mutable but effectively thread-safe.
    /// </threadsafety>
    ///
    /// <author>veshu</author>
    /// 
    /// <version>1.0</version>
    /// <copyright>Copyright (c) 2015, TopCoder, Inc. All rights reserved.</copyright>
    public class CustomPasswordValidator : UserNamePasswordValidator
    {
        /// <summary>
        /// <para>
        /// Gets or sets the authentication service.
        /// </para>
        /// </summary>
        /// <value>
        /// The authentication service instance. It should not be null after initialization through Unity injection. 
        /// </value>
        [Dependency]
        public IAuthenticationService AuthenticationService
        {
            get;
            set;
        }

        /// <summary>
        /// <para>
        /// Gets or sets the logger. 
        /// </para>
        /// </summary>
        /// <value>
        /// The logger. It should not be null after initialization through Unity injection. 
        /// </value>
        [Dependency]
        public ILog Logger
        {
            get;
            set;
        }

        /// <summary>
        /// <para>
        /// Initializes a new instance of the <see cref="CustomPasswordValidator"/> class.
        /// </para>
        /// </summary>
        public CustomPasswordValidator()
        {
        }

        /// <summary>
        /// <para>
        /// Validates the specified username and password.
        /// </para>
        /// </summary>
        /// <param name="userName">The username to check.</param>
        /// <param name="password">The password.</param>
        /// <exception cref="SecurityTokenException">If username or password is unknown.</exception>
        public override void Validate(string userName, string password)
        {
            Helper.LoggingWrapper(Logger, () =>
            {
                if (!AuthenticationService.Authenticate(userName, password))
                {
                    throw new SecurityTokenException("Unknown username or password.");
                }
            }, userName, Helper.Mask);
        }

        /// <summary>
        /// <para>
        /// Checks whether this instance was properly configured.
        /// </para>
        /// </summary>
        ///
        /// <exception cref="ConfigurationException ">
        /// If it is not configured.
        /// </exception>
        public void CheckConfiguration()
        {
            Helper.CheckConfiguration(AuthenticationService, "AuthenticationService");
            Helper.CheckConfiguration(Logger, "Logger");
        }
    }
}
