/*
* Copyright (c) 2015, TopCoder, Inc. All rights reserved.
*/

using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Tholos.Entities
{
    /// <summary>
    /// This represents prototype code.
    /// </summary>
    /// 
    /// <remarks>
    /// <para>
    /// This class is marked with the <see cref="DataContractAttribute"/> attribute
    /// and the properties with the <see cref="DataMemberAttribute"/> attribute.
    /// </para>
    /// </remarks>
    ///
    /// <threadsafety>
    /// This class is mutable, so it is not thread-safe.
    /// </threadsafety>
    ///
    /// <author>veshu</author>
    ///
    /// <version>1.0</version>
    /// <copyright>Copyright (c) 2015, TopCoder, Inc. All rights reserved.</copyright>
    [DataContract]
    public class PrototypeCode : IdentifiableEntity
    {
        /// <summary>
        /// Gets or sets code.
        /// </summary>
        /// <value>The code.</value>
        [DataMember]
        [Required]
        [MaxLength(64)]
        public string Code
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets prototypes per code.
        /// </summary>
        /// <value>The prototypes per code.</value>
        [DataMember]
        [Required]
        public int PrototypesPerCode
        {
            get;
            set;
        }

        /// <summary>
        /// <para>
        /// Initializes new instance of the <see cref="PrototypeCode"/> class.
        /// </para>
        /// </summary>
        public PrototypeCode()
        {
        }
    }
}

