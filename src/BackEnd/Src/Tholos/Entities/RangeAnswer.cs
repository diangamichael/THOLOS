/*
* Copyright (c) 2015, TopCoder, Inc. All rights reserved.
*/

using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Tholos.Entities
{
    /// <summary>
    /// This represents range answer.
    /// </summary>
    /// 
    /// <remarks>
    /// <para>
    /// This class is marked with the <see cref="DataContractAttribute"/> attribute
    /// and the properties with the <see cref="DataMemberAttribute"/> attribute.
    /// </para>
    /// </remarks>
    ///
    /// <threadsafety>
    /// This class is mutable, so it is not thread-safe.
    /// </threadsafety>
    ///
    /// <author>veshu</author>
    ///
    /// <version>1.0</version>
    /// <copyright>Copyright (c) 2015, TopCoder, Inc. All rights reserved.</copyright>
    [DataContract]
    public class RangeAnswer : Answer
    {

        /// <summary>
        /// Gets or sets value.
        /// </summary>
        /// <value>The value.</value>
        [DataMember]
        [Required]
        public int Value
        {
            get;
            set;
        }

        /// <summary>
        /// <para>
        /// Initializes new instance of the <see cref="RangeAnswer"/> class.
        /// </para>
        /// </summary>
        public RangeAnswer()
        {
        }
    }
}

