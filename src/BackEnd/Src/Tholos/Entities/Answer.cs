/*
* Copyright (c) 2015, TopCoder, Inc. All rights reserved.
*/

using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Tholos.Entities
{
    /// <summary>
    /// This represents answer.
    /// </summary>
    /// 
    /// <remarks>
    /// <para>
    /// This class is marked with the <see cref="DataContractAttribute"/> attribute
    /// and the properties with the <see cref="DataMemberAttribute"/> attribute.
    /// </para>
    /// </remarks>
    ///
    /// <threadsafety>
    /// This class is mutable, so it is not thread-safe.
    /// </threadsafety>
    ///
    /// <author>veshu</author>
    ///
    /// <version>1.0</version>
    /// <copyright>Copyright (c) 2015, TopCoder, Inc. All rights reserved.</copyright>
    [KnownType(typeof(SingleAnswer))]
    [KnownType(typeof(RangeAnswer))]
    [KnownType(typeof(SingleInputAnswer))]
    [DataContract]
    public class Answer : IdentifiableEntity
    {
        /// <summary>
        /// Gets or sets question id.
        /// </summary>
        /// <value>The question id.</value>
        [DataMember]
        [Required]
        public long QuestionId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets answer type.
        /// </summary>
        /// <value>The answer type.</value>
        [DataMember]
        [Required]
        public AnswerType AnswerType
        {
            get;
            set;
        }

        /// <summary>
        /// <para>
        /// Initializes new instance of the <see cref="Answer"/> class.
        /// </para>
        /// </summary>
        public Answer()
        {
        }
    }
}

