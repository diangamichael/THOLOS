/*
* Copyright (c) 2015, TopCoder, Inc. All rights reserved.
*/

using System.Runtime.Serialization;

namespace Tholos.Entities
{
    /// <summary>
    /// This represents search filter.
    /// </summary>
    /// 
    /// <remarks>
    /// <para>
    /// This class is marked with the <see cref="DataContractAttribute"/> attribute
    /// and the properties with the <see cref="DataMemberAttribute"/> attribute.
    /// </para>
    /// </remarks>
    ///
    /// <threadsafety>
    /// This class is mutable, so it is not thread-safe.
    /// </threadsafety>
    ///
    /// <author>veshu</author>
    ///
    /// <version>1.0</version>
    /// <copyright>Copyright (c) 2015, TopCoder, Inc. All rights reserved.</copyright>
    [DataContract]
    public class SearchFilter
    {
        /// <summary>
        /// Gets or sets  page number.
        /// </summary>
        /// <value>The  page number.</value>
        [DataMember]
        public int PageNumber
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets page size.
        /// </summary>
        /// <value>The page size.</value>
        [DataMember]
        public int PageSize
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets sort column.
        /// </summary>
        /// <value>The  sort column.</value>
        [DataMember]
        public string SortColumn
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets sort ascending flag.
        /// </summary>
        /// <value>
        /// <c>true</c> if sort ascending; otherwise, <c>false</c>.
        /// </value>
        [DataMember]
        public bool? SortAscending
        {
            get;
            set;
        }

        /// <summary>
        /// Initializes new instance of the <see cref="SearchFilter"/> class.
        /// </summary>
        public SearchFilter()
        {
        }
    }
}

