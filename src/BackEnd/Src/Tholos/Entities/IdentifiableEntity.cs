/*
* Copyright (c) 2015, TopCoder, Inc. All rights reserved.
*/

using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Tholos.Entities
{
    /// <summary>
    /// An entity class that represents IdentifiableEntity. It is a base class for all identifiable entities.
    /// </summary>
    ///
    /// <remarks>
    /// <para>
    /// This class is marked with the <see cref="DataContractAttribute"/> attribute
    /// and the properties with the <see cref="DataMemberAttribute"/> attribute.
    /// </para>
    /// </remarks>
    ///
    /// <threadsafety>
    /// This class is mutable, so it is not thread-safe.
    /// </threadsafety>
    ///
    /// <author>veshu</author>
    ///
    /// <version>1.0</version>
    /// <copyright>Copyright (c) 2015, TopCoder, Inc. All rights reserved.</copyright>
    [DataContract]
    public abstract class IdentifiableEntity
    {
        /// <summary>
        /// Gets or sets the entity identifier.
        /// </summary>
        /// <value>
        /// The entity identifier.
        /// </value>
        [DataMember]
        [Required]
        public long Id { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="IdentifiableEntity"/> class.
        /// </summary>
        protected IdentifiableEntity()
        {
        }

        /// <summary>
        /// Determines whether the specified Object is equal to the current Object.
        /// </summary>
        /// <param name="obj">The Object to compare with the current Object</param>
        /// <returns>return true if the specified Object is equal to the current Object; otherwise, false.</returns>
        public override bool Equals(object obj)
        {
            var other = obj as IdentifiableEntity;
            return (other != null) && (other.Id == Id) && obj.GetType() == GetType();
        }

        /// <summary>
        /// Serves as a hash function for a particular type.
        /// </summary>
        /// <returns>return hash code for the current Object.</returns>
        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }
    }
}

