/*
* Copyright (c) 2015, TopCoder, Inc. All rights reserved.
*/

using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Tholos.Entities
{
    /// <summary>
    /// This represents range answer option.
    /// </summary>
    /// 
    /// <remarks>
    /// <para>
    /// This class is marked with the <see cref="DataContractAttribute"/> attribute
    /// and the properties with the <see cref="DataMemberAttribute"/> attribute.
    /// </para>
    /// </remarks>
    ///
    /// <threadsafety>
    /// This class is mutable, so it is not thread-safe.
    /// </threadsafety>
    ///
    /// <author>veshu</author>
    ///
    /// <version>1.0</version>
    /// <copyright>Copyright (c) 2015, TopCoder, Inc. All rights reserved.</copyright>
    [DataContract]
    public class RangeAnswerOption : AnswerOption
    {
        /// <summary>
        /// Gets or sets from value label.
        /// </summary>
        /// <value>The from value label.</value>
        [DataMember]
        [Required]
        [MaxLength(64)]
        public string FromValueLabel
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets to value label.
        /// </summary>
        /// <value>The to value label.</value>
        [DataMember]
        [Required]
        [MaxLength(64)]
        public string ToValueLabel
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets from value.
        /// </summary>
        /// <value>The from value.</value>
        [DataMember]
        [Required]
        public int FromValue
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets to value.
        /// </summary>
        /// <value>The to value.</value>
        [DataMember]
        [Required]
        public int ToValue
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets increment.
        /// </summary>
        /// <value>The increment.</value>
        [DataMember]
        public int? Increment
        {
            get;
            set;
        }

        /// <summary>
        /// <para>
        /// Initializes new instance of the <see cref="RangeAnswerOption"/> class.
        /// </para>
        /// </summary>
        public RangeAnswerOption()
        {
        }
    }
}

