/*
* Copyright (c) 2015, TopCoder, Inc. All rights reserved.
*/

using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Tholos.Entities
{
    /// <summary>
    /// This represents question.
    /// </summary>
    /// 
    /// <remarks>
    /// <para>
    /// This class is marked with the <see cref="DataContractAttribute"/> attribute
    /// and the properties with the <see cref="DataMemberAttribute"/> attribute.
    /// </para>
    /// </remarks>
    ///
    /// <threadsafety>
    /// This class is mutable, so it is not thread-safe.
    /// </threadsafety>
    ///
    /// <author>veshu</author>
    ///
    /// <version>1.0</version>
    /// <copyright>Copyright (c) 2015, TopCoder, Inc. All rights reserved.</copyright>
    [DataContract]
    public class Question : IdentifiableEntity
    {
        /// <summary>
        /// Relationship label.
        /// </summary>
        /// <value>The relationship label.</value>
        [DataMember]
        [Required]
        [MaxLength(1024)]
        public string RelationshipLabel
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets text.
        /// </summary>
        /// <value>The text.</value>
        [DataMember]
        [Required]
        [MaxLength(1024)]
        public string Text
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets answer options.
        /// </summary>
        /// <value>The answer options.</value>
        [DataMember]
        public IList<AnswerOption> AnswerOptions
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets question condition.
        /// </summary>
        /// <value>The question condition.</value>
        [DataMember]
        public QuestionCondition Condition
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets  question type.
        /// </summary>
        /// <value>The  question type.</value>
        [DataMember]
        [Required]
        [MaxLength(32)]
        public QuestionType QuestionType
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets conditional flag.
        /// </summary>
        /// <value>
        /// <c>true</c> if conditional flag; otherwise, <c>false</c>.
        /// </value>
        [DataMember]
        [Required]
        public bool Conditional
        {
            get;
            set;
        }

        /// <summary>
        /// <para>
        /// Initializes new instance of the <see cref="Question"/> class.
        /// </para>
        /// </summary>
        public Question()
        {
        }
    }
}

