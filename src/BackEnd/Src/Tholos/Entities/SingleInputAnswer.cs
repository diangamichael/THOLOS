/*
* Copyright (c) 2015, TopCoder, Inc. All rights reserved.
*/

using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Tholos.Entities
{
    /// <summary>
    /// This represents input answer.
    /// </summary>
    /// 
    /// <remarks>
    /// <para>
    /// This class is marked with the <see cref="DataContractAttribute"/> attribute
    /// and the properties with the <see cref="DataMemberAttribute"/> attribute.
    /// </para>
    /// </remarks>
    ///
    /// <threadsafety>
    /// This class is mutable, so it is not thread-safe.
    /// </threadsafety>
    ///
    /// <author>veshu</author>
    ///
    /// <version>1.0</version>
    /// <copyright>Copyright (c) 2015, TopCoder, Inc. All rights reserved.</copyright>
    [DataContract]
    public class SingleInputAnswer : SingleAnswer
    {
        /// <summary>
        /// Gets or sets input.
        /// </summary>
        /// <value>The input.</value>
        [DataMember]
        [Required]
        [MaxLength(128)]
        public string Input
        {
            get;
            set;
        }

        /// <summary>
        /// Initializes new instance of the <see cref="SingleInputAnswer"/> class.
        /// </summary>
        public SingleInputAnswer()
        {
        }
    }
}

