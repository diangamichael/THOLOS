/*
* Copyright (c) 2015, TopCoder, Inc. All rights reserved.
*/

using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Tholos.Entities
{
    /// <summary>
    /// This represents survey search result.
    /// </summary>
    /// 
    /// <remarks>
    /// <para>
    /// This class is marked with the <see cref="DataContractAttribute"/> attribute
    /// and the properties with the <see cref="DataMemberAttribute"/> attribute.
    /// </para>
    /// </remarks>
    ///
    /// <threadsafety>
    /// This class is mutable, so it is not thread-safe.
    /// </threadsafety>
    ///
    /// <author>veshu</author>
    ///
    /// <version>1.0</version>
    /// <copyright>Copyright (c) 2015, TopCoder, Inc. All rights reserved.</copyright>
    [DataContract]
    public class AllSurveysSearchResult
    {

        /// <summary>
        ///  Gets or sets total records count.
        /// </summary>
        /// <value>The total records count.</value>
        [DataMember]
        public int TotalRecordCount
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets total page count.
        /// </summary>
        /// <value>The total page count.</value>
        [DataMember]
        public int TotalPageCount
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets surveys grouped by the status.
        /// </summary>
        /// <value>The surveys grouped by the status.</value>
        [DataMember]
        public Dictionary<SurveyStatus, List<Survey>> Records
        {
            get;
            set;
        }

        /// <summary>
        /// <para>
        /// Initializes new instance of the <see cref="AllSurveysSearchResult"/> class.
        /// </para>
        /// </summary>
        public AllSurveysSearchResult()
        {
        }
    }
}

