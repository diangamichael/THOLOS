/*
* Copyright (c) 2015, TopCoder, Inc. All rights reserved.
*/

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Tholos.Entities
{
    /// <summary>
    /// This represents survey.
    /// </summary>
    /// 
    /// <remarks>
    /// <para>
    /// This class is marked with the <see cref="DataContractAttribute"/> attribute
    /// and the properties with the <see cref="DataMemberAttribute"/> attribute.
    /// </para>
    /// </remarks>
    ///
    /// <threadsafety>
    /// This class is mutable, so it is not thread-safe.
    /// </threadsafety>
    ///
    /// <author>veshu</author>
    ///
    /// <version>1.0</version>
    /// <copyright>Copyright (c) 2015, TopCoder, Inc. All rights reserved.</copyright>
    [DataContract]
    public class Survey : NamedEntity
    {
        /// <summary>
        /// Gets or sets the Name.
        /// </summary>
        /// <remarks>
        /// This override the property of base class in order to specify validation rules.
        /// </remarks>
        /// <value>The Survey Name</value>
        [DataMember]
        [Required]
        [StringLength(64)]
        public override string Name { get; set; }

        /// <summary>
        /// Gets or sets cell name.
        /// </summary>
        /// <value>The cell name.</value>
        [DataMember]
        [Required]
        [MaxLength(64)]
        public string CellName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets participants number.
        /// </summary>
        /// <value>The participants number.</value>
        [DataMember]
        [Required]
        public int ParticipantsNumber
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets prototpe codes.
        /// </summary>
        /// <value>The prototpe codes.</value>
        [DataMember]
        public IList<PrototypeCode> PrototypeCodes
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets status.
        /// </summary>
        /// <value>The status.</value>
        [DataMember]
        [Required]
        [MaxLength(64)]
        public SurveyStatus Status
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets date created.
        /// </summary>
        /// <value>The date created.</value>
        [DataMember]
        [SqlDateTimeRangeValidation]
        public DateTime DateCreated
        {
            get;
            set;
        }


        /// <summary>
        /// Gets or sets completed surveys number.
        /// </summary>
        /// <value>The completed surveys number.</value>
        [DataMember]
        [Required]
        public int CompletedSurveysNumber
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets prototype questions.
        /// <para>
        /// The keys are the indexes of the questions in the list.
        /// </para>
        /// </summary>
        /// <value>The completed surveys number.</value>
        [DataMember]
        public IList<IndexedQuestion> PrototypeQuestions
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets post testing questions.
        /// <para>
        /// The keys are the indexes of the questions in the list.
        /// </para>
        /// </summary>
        /// <value>The completed surveys number.</value>
        [DataMember]
        public IList<IndexedQuestion> PostTestingQuestions
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets draft flag.
        /// </summary>
        /// <value>
        /// <c>true</c> if draft; otherwise, <c>false</c>.
        /// </value>
        [DataMember]
        [Required]
        public bool Draft
        {
            get;
            set;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Survey"/> class.
        /// </summary>
        public Survey()
        {
        }
    }
}

