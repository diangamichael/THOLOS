/*
* Copyright (c) 2015, TopCoder, Inc. All rights reserved.
*/

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Tholos.Entities
{
    /// <summary>
    /// <para>
    /// This represents participant entity.
    /// </para>
    /// <para>
    /// Changes in version 1.1 (THOLOS - Integrate Prototype with Backend APIs):
    /// <list type="bullet">
    /// <item>
    /// <description>Added KnownType to deal with different answer types.</description>
    /// </item>
    /// </list>
    /// </para>
    /// </summary>
    /// 
    /// <remarks>
    /// <para>
    /// This class is marked with the <see cref="DataContractAttribute"/> attribute
    /// and the properties with the <see cref="DataMemberAttribute"/> attribute.
    /// </para>
    /// </remarks>
    ///
    /// <threadsafety>
    /// This class is mutable, so it is not thread-safe.
    /// </threadsafety>
    ///
    /// <author>veshu</author>
    /// <author>duxiaoyang</author>
    ///
    /// <version>1.1</version>
    /// <since>1.0</since>
    /// <copyright>Copyright (c) 2015, TopCoder, Inc. All rights reserved.</copyright>
    [DataContract]
    [KnownType(typeof(RangeAnswer))]
    [KnownType(typeof(SingleAnswer))]
    [KnownType(typeof(SingleInputAnswer))]
    public class Participant : IdentifiableEntity
    {
        /// <summary>
        /// Gets or sets the Username.
        /// </summary>
        /// <value>The Username</value>
        [DataMember]
        [Required]
        [StringLength(64)]
        public string Username { get; set; }

        /// <summary>
        /// Gets or sets the  survey id.
        /// </summary>
        /// <value>The  survey id</value>
        [DataMember]
        [Required]
        public long SurveyId
        {
            get;
            set;
        }

        /// /// <summary>
        /// Gets or sets the  birth date.
        /// </summary>
        /// <value>The  birth date.</value>
        [DataMember]
        [SqlDateTimeRangeValidation]
        public DateTime? BirthDate
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets weight.
        /// </summary>
        /// <value>The weight.</value>
        [DataMember]
        [Required]
        public int Weight
        {
            get;
            set;
        }

        /// <summary>
        ///  Gets or sets  height feet.
        /// </summary>
        /// <value>The height feet.</value>
        [DataMember]
        [Required]
        public int HeightFeet
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets height inches.
        /// </summary>
        /// <value>The height inches.</value>
        [DataMember]
        [Required]
        public int HeightInches
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets  profile answers.
        /// </summary>
        /// <value>The list of profile answers.</value>
        [DataMember]
        public IList<Answer> ProfileAnswers
        {
            get;
            set;
        }

        /// <summary>
        ///  Gets or sets  roles.
        /// </summary>
        /// <value>The list of role</value>
        [DataMember]
        public IList<Role> Roles
        {
            get;
            set;
        }

        /// <summary>
        /// <para>
        /// Initializes new instance of the <see cref="Participant"/> class.
        /// </para>
        /// </summary>
        public Participant()
        {
        }
    }
}

