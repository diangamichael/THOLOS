﻿/*
 * Copyright (c) 2015, TopCoder, Inc. All rights reserved.
 */

using System;
using System.Collections.Generic;
using System.ServiceModel.Web;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Tholos.Entities;
using Tholos.Services;
using Tholos.Services.Impl;
using Tholos.Tests.services.api.proxy;

namespace Tholos.Tests.services.api
{
    /// <summary>
    /// Unit tests for <see cref="SurveyService"/> class.
    /// </summary>
    ///
    /// <author>
    /// veshu
    /// </author>
    ///
    /// <version>
    /// 1.0
    /// </version>
    ///
    /// <copyright>
    /// Copyright (c) 2015, TopCoder, Inc. All rights reserved.
    /// </copyright>
    [TestClass]
    public class SurveyServiceRestApiUnitTests : BasePersistenceServiceUnitTest<SurveyService>
    {
        /// <summary>
        /// The service host address.
        /// </summary>
        private static readonly string ServiceHostAddress =
            TestHelper.WcfServiceHostBaseUri + typeof(SurveyService).Name + ".svc/";

        /// <summary>
        /// Represents the <see cref="SurveyServiceRestApiProxy"/> instance used for REST API tests.
        /// </summary>
        private readonly ISurveyService _restApiProxy = new SurveyServiceRestApiProxy();

        /// <summary>
        /// Sets up the environment before executing each test in this class.
        /// </summary>
        [TestInitialize]
        public override void SetUp()
        {
            base.SetUp();
            _instance.ExportService = new ExportService();
            _instance.ParticipantPasswordLength = TestHelper.ParticipantPasswordLength;
            _instance.ParticipantUsernameLength = TestHelper.ParticipantUsernameLength;
        }

        /// <summary>
        /// Cleans up the environment after executing each test in this class.
        /// </summary>
        [TestCleanup]
        public override void CleanUp()
        {
            base.CleanUp();
            TestHelper.ClearUploadFolder();
        }

        /// <summary>
        /// Accuracy test of the class constructor.
        /// </summary>
        [TestMethod]
        public void TestCtorAccuracy()
        {
            Assert.AreEqual(typeof(BasePersistenceService), _instance.GetType().BaseType,
                "The class should inherit from BasePersistenceService.");
            Assert.IsTrue(_instance is ISurveyService,
                "The class should implement ISurveyService.");
        }

        #region CheckConfiguration() method tests

        /// <summary>
        /// Failure test of <c>CheckConfiguration</c> method,
        /// should throw <see cref="ConfigurationException"/> when <c>ExportService</c> is <c>null</c>.
        /// </summary>
        [TestMethod, ExpectedException(typeof(ConfigurationException))]
        public void TestCheckExportServiceNull()
        {
            // arrange
            _instance.ExportService = null;

            // act
            _instance.CheckConfiguration();
        }

        /// <summary>
        /// Failure test of <c>CheckConfiguration</c> method,
        /// should throw <see cref="ConfigurationException"/> 
        /// when <c>ParticipantPasswordLength</c> is negative.
        /// </summary>
        [TestMethod, ExpectedException(typeof(ConfigurationException))]
        public void TestCheckConfigurationParticipantPasswordLengthNegative()
        {
            // arrange
            _instance.ParticipantPasswordLength = -10;

            // act
            _instance.CheckConfiguration();
        }

        /// <summary>
        /// Failure test of <c>CheckConfiguration</c> method,
        /// should throw <see cref="ConfigurationException"/> 
        /// when <c>ParticipantPasswordLength</c> is zero.
        /// </summary>
        [TestMethod, ExpectedException(typeof(ConfigurationException))]
        public void TestCheckConfigurationParticipantPasswordLengthZero()
        {
            // arrange
            _instance.ParticipantPasswordLength = 0;

            // act
            _instance.CheckConfiguration();
        }

        /// <summary>
        /// Failure test of <c>CheckConfiguration</c> method,
        /// should throw <see cref="ConfigurationException"/> 
        /// when <c>ParticipantUsernameLength</c> is negative.
        /// </summary>
        [TestMethod, ExpectedException(typeof(ConfigurationException))]
        public void TestCheckConfigurationParticipantUsernameLengthNegative()
        {
            // arrange
            _instance.ParticipantPasswordLength = -10;

            // act
            _instance.CheckConfiguration();
        }

        /// <summary>
        /// Failure test of <c>CheckConfiguration</c> method,
        /// should throw <see cref="ConfigurationException"/> 
        /// when <c>ParticipantUsernameLength</c> is zero.
        /// </summary>
        [TestMethod, ExpectedException(typeof(ConfigurationException))]
        public void TestCheckConfigurationParticipantUsernameLengthZero()
        {
            // arrange
            _instance.ParticipantUsernameLength = 0;

            // act
            _instance.CheckConfiguration();
        }

        /// <summary>
        /// Success test of <c>CheckConfiguration</c> method,
        /// should not throw any exception.
        /// </summary>
        [TestMethod]
        public void TestCheckConfiguration()
        {
            // act
            _instance.CheckConfiguration();
        }

        #endregion

        #region GetAllPrototypeQuestions() method tests
        /// <summary>
        /// Tests <c>GetAllPrototypeQuestions</c> method accuracy.
        /// </summary>
        [TestMethod]
        public void TestGetAllPrototypeQuestionsAccuracy()
        {
            var result = _restApiProxy.GetAllPrototypeQuestions();

            Assert.IsNotNull(result, "questions should not be null");
            Assert.AreEqual(4, result.Count, "Number of questions is wrong");

            TestHelper.VerifyAnswerOption(result);

        }

        #endregion

        #region GetAllPostTestingQuestions() method tests
        /// <summary>
        /// Tests <c>GetAllPostTestingQuestions</c> method accuracy.
        /// </summary>
        [TestMethod]
        public void TestGetAllPostTestingQuestionsAccuracy()
        {
            var result = _restApiProxy.GetAllPostTestingQuestions();

            Assert.IsNotNull(result, "questions should not be null");
            Assert.AreEqual(4, result.Count, "Number of questions is wrong");

            TestHelper.VerifyAnswerOption(result);
        }


        #endregion

        #region CreateSurvey() method tests

        /// <summary>
        /// Accuracy test of <c>CreateSurvey</c> method,
        /// </summary>
        [TestMethod]
        public void TestCreateSurvey1()
        {
            // arrange
            var id = 1;
            var survey = new Survey()
            {
                Name = "newSurvey1",
                CellName = "Cellname1",
                Status = SurveyStatus.Draft,
                CompletedSurveysNumber = 0,
                Draft = true,
                ParticipantsNumber = 5,
                PostTestingQuestions = TestHelper.GetQuestionsList(),
                PrototypeCodes = TestHelper.GetPrototypeCodes(),
                PrototypeQuestions = TestHelper.GetQuestionsList()
            };

            // act
            var result = _restApiProxy.CreateSurvey(survey, id);
            Assert.AreNotEqual(0, result, "Creation failed");

            TestHelper.AssertDatabaseRecordExists("Survey", new Dictionary<string, object>
            {
                {"Id", result}
            });

            foreach (var questions in survey.PrototypeQuestions)
            {
                TestHelper.AssertDatabaseRecordExists("SurveyProrotypeQuestion", new Dictionary<string, object>
                {
                    {"SurveyId", result},
                    {"QuestionId", questions.Question.Id},
                    {"QuestionNumber", questions.Position}
                });
            }
            foreach (var questions in survey.PostTestingQuestions)
            {
                TestHelper.AssertDatabaseRecordExists("SurveyPostTestingQuestion", new Dictionary<string, object>
                {
                    {"SurveyId", result},
                    {"QuestionId", questions.Question.Id},
                    {"QuestionNumber", questions.Position}
                });
            }
            foreach (var codes in survey.PrototypeCodes)
            {
                TestHelper.AssertDatabaseRecordExists("PrototypeCode", new Dictionary<string, object>
                {
                    {"SurveyId", result},
                    {"Code", codes.Code},
                    {"PrototypesPerCode", codes.PrototypesPerCode}
                });
            }

            // verify participant credentials 
            TestHelper.AssertDatabaseRecordCount("ParticipantCredentials", new Dictionary<string, object>
            {
                  {"SurveyId", result}
            }, survey.ParticipantsNumber);

        }

        /// <summary>
        /// Accuracy test of <c>CreateSurvey</c> method,
        /// </summary>
        [TestMethod]
        public void TestCreateSurvey2()
        {
            // arrange
            var id = 1;
            var survey = new Survey()
            {
                Name = "newSurvey1",
                CellName = "Cellname1",
                Status = SurveyStatus.Draft,
                CompletedSurveysNumber = 0,
                Draft = true,
                ParticipantsNumber = 5,
                PostTestingQuestions = new List<IndexedQuestion>(),
                PrototypeCodes = new List<PrototypeCode>(),
                PrototypeQuestions = new List<IndexedQuestion>()
            };

            // act
            var result = _restApiProxy.CreateSurvey(survey, id);
            Assert.AreNotEqual(0, result, "Creation failed");

            TestHelper.AssertDatabaseRecordExists("Survey", new Dictionary<string, object>
            {
                {"Id", result}
            });

            // verify participant credentials 
            TestHelper.AssertDatabaseRecordCount("ParticipantCredentials", new Dictionary<string, object>
            {
                  {"SurveyId", result}
            }, survey.ParticipantsNumber);

        }


        /// <summary>
        /// Accuracy test of <c>CreateSurvey</c> method.
        ///  without name of survey
        /// </summary>
        [TestMethod]
        public void TestCreateSurvey3()
        {
            // arrange
            var id = 1;
            var survey = new Survey()
            {
                Name = null,
                CellName = "",
                Status = SurveyStatus.Draft,
                CompletedSurveysNumber = 0,
                Draft = true,
                ParticipantsNumber = 5,
                PostTestingQuestions = TestHelper.GetQuestionsList(),
                PrototypeCodes = TestHelper.GetPrototypeCodes(),
                PrototypeQuestions = TestHelper.GetQuestionsList()
            };
            // act
            TestHelper.AssertThrows<ArgumentException>(() => _restApiProxy.CreateSurvey(survey, id));
        }


        /// <summary>
        /// Accuracy test of <c>CreateSurvey</c> method.
        /// check negative survey ids
        /// </summary>
        [TestMethod]
        public void TestCreateSurvey8()
        {
            var wrongQuestionid = new List<IndexedQuestion>
            {
                {
                    new IndexedQuestion
                    {
                        Position = 1,
                        Question = new Question
                        {
                            Id = -7
                        }
                    }
                }
            };
            // arrange
            var id = 1;
            var survey = new Survey()
            {
                Name = null,
                CellName = "",
                Status = SurveyStatus.Draft,
                CompletedSurveysNumber = 0,
                Draft = true,
                ParticipantsNumber = 5,
                PostTestingQuestions = wrongQuestionid,
                PrototypeCodes = TestHelper.GetPrototypeCodes(),
                PrototypeQuestions = TestHelper.GetQuestionsList()
            };
            // act
            TestHelper.AssertThrows<ArgumentException>(() => _restApiProxy.CreateSurvey(survey, id));
        }

        /// <summary>
        /// Accuracy test of <c>CreateSurvey</c> method.
        /// check negative survey ids
        /// </summary>
        [TestMethod]
        public void TestCreateSurvey9()
        {
            // arrange
            var id = 100;
            var survey = new Survey()
            {
                Name = "NameNew1",
                CellName = "New1",
                Status = SurveyStatus.Draft,
                CompletedSurveysNumber = 0,
                Draft = true,
                ParticipantsNumber = 5,
                PostTestingQuestions = TestHelper.GetQuestionsList(),
                PrototypeCodes = TestHelper.GetPrototypeCodes(),
                PrototypeQuestions = TestHelper.GetQuestionsList()
            };
            // act
            TestHelper.AssertThrows<EntityNotFoundException>(() => _restApiProxy.CreateSurvey(survey, id));
        }

        /// <summary>
        /// Accuracy test of <c>CreateSurvey</c> method.
        /// check long name
        /// </summary>
        [TestMethod]
        public void TestCreateSurvey10()
        {
            // arrange
            var id = 1;
            var survey = new Survey()
            {
                Name = "NameNew1NameNew1 NameNew1NameNew1 NameNew1 NameNew1NameNew1 NameNew1NameNew1 NameNew1" +
                       "NameNew1NameNew1 NameNew1NameNew1 NameNew1NameNew1NameNew1 NameNew1NameNew1 NameNew1",
                CellName = "New1",
                Status = SurveyStatus.Draft,
                CompletedSurveysNumber = 0,
                Draft = true,
                ParticipantsNumber = 5,
                PostTestingQuestions = TestHelper.GetQuestionsList(),
                PrototypeCodes = TestHelper.GetPrototypeCodes(),
                PrototypeQuestions = TestHelper.GetQuestionsList()
            };
            // act
            TestHelper.AssertThrows<ArgumentException>(() => _restApiProxy.CreateSurvey(survey, id));
        }

        #endregion

        #region PublishSurvey() method tests

        /// <summary>
        /// Tests <c>PublishSurvey</c> method accuracy.
        /// entity not found
        /// </summary>
        [TestMethod]
        public void TestPublishSurveyAccuracy1()
        {
            var id = "400";

            TestHelper.AssertThrows<EntityNotFoundException>(() => _restApiProxy.PublishSurvey(id));
        }

        /// <summary>
        /// Tests <c>PublishSurvey</c> method accuracy.
        /// negative id
        /// </summary>
        [TestMethod]
        public void TestPublishSurveyAccuracy2()
        {
            var id = "-19";

            TestHelper.AssertThrows<ArgumentException>(() => _restApiProxy.PublishSurvey(id));
        }

        /// <summary>
        /// Tests <c>PublishSurvey</c> method accuracy.
        /// success case check status only
        /// </summary>
        [TestMethod]
        public void TestPublishSurveyAccuracy3()
        {
            var id = "2";

            TestHelper.AssertDatabaseRecordCount("Survey", new Dictionary<string, object>
            {
                {"Id", id},
                {"Status", SurveyStatus.Published.ToString()}
            }, 0);

            _restApiProxy.PublishSurvey(id);

            TestHelper.AssertDatabaseRecordExists("Survey", new Dictionary<string, object>
            {
                {"Id", id},
                {"Status", SurveyStatus.Published.ToString()}
            });
        }

        /// <summary>
        /// Tests <c>PublishSurvey</c> method accuracy.
        /// success case check draft bit only
        /// </summary>
        [TestMethod]
        public void TestPublishSurveyAccuracy4()
        {
            var id = "2";

            TestHelper.AssertDatabaseRecordCount("Survey", new Dictionary<string, object>
            {
                {"Id", id},
                {"Draft", false}
            }, 0);

            _restApiProxy.PublishSurvey(id);

            TestHelper.AssertDatabaseRecordExists("Survey", new Dictionary<string, object>
            {
                {"Id", id},
                {"Draft", false}
            });
        }

        /// <summary>
        /// Tests <c>PublishSurvey</c> method accuracy.
        /// success case
        /// </summary>
        [TestMethod]
        public void TestPublishSurveyAccuracy5()
        {
            var id = "2";

            TestHelper.AssertDatabaseRecordCount("Survey", new Dictionary<string, object>
            {
                {"Id", id},
                {"Draft", false},
                {"Status", SurveyStatus.Published.ToString()}
            }, 0);

            _restApiProxy.PublishSurvey(id);

            TestHelper.AssertDatabaseRecordExists("Survey", new Dictionary<string, object>
            {
                {"Id", id},
                {"Draft", false},
                {"Status", SurveyStatus.Published.ToString()}
            });
        }
        #endregion

        #region UpdateSurvey() method tests

        /// <summary>
        /// Accuracy test of <c>UpdateSurvey</c> method,
        /// </summary>
        [TestMethod]
        public void TestUpdateSurvey1()
        {
            // arrange
            var id = 1;
            var survey = new Survey()
            {
                Id = id,
                Name = "newSurvey1",
                CellName = "Cellname1",
                Status = SurveyStatus.Draft,
                CompletedSurveysNumber = 0,
                Draft = true,
                ParticipantsNumber = 5,
                PostTestingQuestions = TestHelper.GetQuestionsList(),
                PrototypeCodes = TestHelper.GetPrototypeCodes(),
                PrototypeQuestions = TestHelper.GetQuestionsList()
            };

            // before updating count should be 0
            TestHelper.AssertDatabaseRecordCount("Survey", new Dictionary<string, object>
            {
                {"Id", id},
                {"Name", "newSurvey1"},
                {"CellName","Cellname1"}
            }, 0);

            // act
            _restApiProxy.UpdateSurvey(survey);

            // assert
            // after updating record should exist
            TestHelper.AssertDatabaseRecordExists("Survey", new Dictionary<string, object>
            {
                {"Id", id},
                {"Name", "newSurvey1"},
                {"CellName","Cellname1"}
            });

            foreach (var questions in survey.PrototypeQuestions)
            {
                TestHelper.AssertDatabaseRecordExists("SurveyProrotypeQuestion", new Dictionary<string, object>
                {
                    {"SurveyId", id},
                    {"QuestionId", questions.Question.Id},
                    {"QuestionNumber", questions.Position}
                });
            }
            foreach (var questions in survey.PostTestingQuestions)
            {
                TestHelper.AssertDatabaseRecordExists("SurveyPostTestingQuestion", new Dictionary<string, object>
                {
                    {"SurveyId", id},
                    {"QuestionId", questions.Question.Id},
                    {"QuestionNumber", questions.Position}
                });
            }
            foreach (var codes in survey.PrototypeCodes)
            {
                TestHelper.AssertDatabaseRecordExists("PrototypeCode", new Dictionary<string, object>
                {
                    {"SurveyId", id},
                    {"Code", codes.Code},
                    {"PrototypesPerCode", codes.PrototypesPerCode}
                });
            }

        }

        /// <summary>
        /// Accuracy test of <c>UpdateSurvey</c> method,
        /// </summary>
        [TestMethod]
        public void TestUpdateSurvey2()
        {
            // arrange
            var id = 3;
            var survey = new Survey()
            {
                Id = id,
                Name = "newSurvey1",
                CellName = "Cellname1",
                Status = SurveyStatus.Published,
                CompletedSurveysNumber = 0,
                Draft = true,
                ParticipantsNumber = 5,
                PostTestingQuestions = new List<IndexedQuestion>(),
                PrototypeCodes = new List<PrototypeCode>(),
                PrototypeQuestions = new List<IndexedQuestion>()
            };

            // act
            _restApiProxy.UpdateSurvey(survey);

            TestHelper.AssertDatabaseRecordExists("Survey", new Dictionary<string, object>
            {
                {"Id", id},
                {"Name", "newSurvey1"},
                {"CellName","Cellname1"}
            });

        }


        /// <summary>
        /// Accuracy test of <c>UpdateSurvey</c> method.
        ///  without name of survey
        /// </summary>
        [TestMethod]
        public void TestUpdateSurvey3()
        {
            // arrange
            var survey = new Survey()
            {
                Id = 1,
                Name = null,
                CellName = "",
                Status = SurveyStatus.Draft,
                CompletedSurveysNumber = 0,
                Draft = true,
                ParticipantsNumber = 5,
                PostTestingQuestions = TestHelper.GetQuestionsList(),
                PrototypeCodes = TestHelper.GetPrototypeCodes(),
                PrototypeQuestions = TestHelper.GetQuestionsList()
            };
            // act
            TestHelper.AssertThrows<ArgumentException>(() => _restApiProxy.UpdateSurvey(survey));
        }


        /// <summary>
        /// Accuracy test of <c>UpdateSurvey</c> method.
        /// check negative survey ids
        /// </summary>
        [TestMethod]
        public void TestUpdateSurvey8()
        {
            var wrongQuestionid = new List<IndexedQuestion>
            {
                {
                    new IndexedQuestion
                    {
                        Position = 1,
                        Question = new Question
                        {
                            Id = -7
                        }
                    }
                }
            };
            // arrange
            var survey = new Survey()
            {
                Id = 1,
                Name = "newName",
                CellName = "CellnewName1",
                Status = SurveyStatus.Draft,
                CompletedSurveysNumber = 0,
                Draft = true,
                ParticipantsNumber = 5,
                PostTestingQuestions = wrongQuestionid,
                PrototypeCodes = TestHelper.GetPrototypeCodes(),
                PrototypeQuestions = TestHelper.GetQuestionsList()
            };
            // act
            TestHelper.AssertThrows<ArgumentException>(() => _restApiProxy.UpdateSurvey(survey));
        }

        /// <summary>
        /// Accuracy test of <c>UpdateSurvey</c> method.
        /// check negative not existing id.
        /// </summary>
        [TestMethod]
        public void TestUpdateSurvey9()
        {
            // arrange
            var survey = new Survey()
            {
                Id = 100,
                Name = "NameNew1",
                CellName = "New1",
                Status = SurveyStatus.Draft,
                CompletedSurveysNumber = 0,
                Draft = true,
                ParticipantsNumber = 5,
                PostTestingQuestions = TestHelper.GetQuestionsList(),
                PrototypeCodes = TestHelper.GetPrototypeCodes(),
                PrototypeQuestions = TestHelper.GetQuestionsList()
            };
            // act
            TestHelper.AssertThrows<EntityNotFoundException>(() => _restApiProxy.UpdateSurvey(survey));
        }

        /// <summary>
        /// Accuracy test of <c>UpdateSurvey</c> method.
        /// check long name
        /// </summary>
        [TestMethod]
        public void TestUpdateSurvey10()
        {

            var survey = new Survey()
            {
                Id = 1,
                Name = "NameNew1NameNew1 NameNew1NameNew1 NameNew1 NameNew1NameNew1 NameNew1NameNew1 NameNew1" +
                       "NameNew1NameNew1 NameNew1NameNew1 NameNew1NameNew1NameNew1 NameNew1NameNew1 NameNew1",
                CellName = "New1",
                Status = SurveyStatus.Draft,
                CompletedSurveysNumber = 0,
                Draft = true,
                ParticipantsNumber = 5,
                PostTestingQuestions = TestHelper.GetQuestionsList(),
                PrototypeCodes = TestHelper.GetPrototypeCodes(),
                PrototypeQuestions = TestHelper.GetQuestionsList()
            };
            // act
            TestHelper.AssertThrows<ArgumentException>(() => _restApiProxy.UpdateSurvey(survey));
        }

        #endregion

        #region GetSurvey() method tests

        /// <summary>
        /// Accuracy test of <c>GetSurvey</c> method,
        /// no exception is expected to be thrown.
        /// </summary>
        [TestMethod]
        public void TestGetSurveyAccuracy1()
        {
            var result = _restApiProxy.GetSurvey("1");

            Assert.AreEqual(1, result.Id, "Id is wrong");
            Assert.AreEqual("CellName1", result.CellName, "CellName is wrong");
            Assert.AreEqual("Survey1", result.Name, "Name is wrong");
            Assert.AreEqual(0, result.CompletedSurveysNumber, "CompletedSurveysNumber is wrong");
            Assert.AreEqual(3, result.ParticipantsNumber, "ParticipantsNumber is wrong");
            Assert.AreEqual(3, result.PostTestingQuestions.Count, "Post Testing questions is wrong");
            Assert.AreEqual(3, result.PrototypeQuestions.Count, "PrototypeQuestions is wrong");
            Assert.AreEqual(3, result.PrototypeCodes.Count, "PrototypeCodes is wrong");
            Assert.AreEqual(SurveyStatus.Published, result.Status, "Status is wrong");
        }

        /// <summary>
        /// Accuracy test of <c>GetSurvey</c> method,
        /// no exception is expected to be thrown.
        /// </summary>
        [TestMethod]
        public void TestGetSurveyAccuracy5()
        {
            var result = _restApiProxy.GetSurvey("2");

            Assert.AreEqual(2, result.Id, "Id is wrong");
            Assert.AreEqual("CellName2", result.CellName, "CellName is wrong");
            Assert.AreEqual("Survey2", result.Name, "Name is wrong");
            Assert.AreEqual(0, result.CompletedSurveysNumber, "CompletedSurveysNumber is wrong");
            Assert.AreEqual(3, result.ParticipantsNumber, "ParticipantsNumber is wrong");
            Assert.AreEqual(1, result.PostTestingQuestions.Count, "Post Testing questions is wrong");
            Assert.AreEqual(1, result.PrototypeQuestions.Count, "PrototypeQuestions is wrong");
            Assert.AreEqual(1, result.PrototypeCodes.Count, "PrototypeCodes is wrong");
            Assert.AreEqual(SurveyStatus.Draft, result.Status, "Status is wrong");
        }

        /// <summary>
        /// Accuracy test of <c>GetSurvey</c> method,
        /// no exception is expected to be thrown. check without questions and count
        /// </summary>
        [TestMethod]
        public void TestGetSurveyAccuracy6()
        {
            var result = _restApiProxy.GetSurvey("3");

            Assert.AreEqual(3, result.Id, "Id is wrong");
            Assert.AreEqual("CellName3", result.CellName, "CellName is wrong");
            Assert.AreEqual("Survey3", result.Name, "Name is wrong");
            Assert.AreEqual(0, result.CompletedSurveysNumber, "CompletedSurveysNumber is wrong");
            Assert.AreEqual(3, result.ParticipantsNumber, "ParticipantsNumber is wrong");
            Assert.AreEqual(0, result.PostTestingQuestions.Count, "Post Testing questions is wrong");
            Assert.AreEqual(0, result.PrototypeQuestions.Count, "PrototypeQuestions is wrong");
            Assert.AreEqual(0, result.PrototypeCodes.Count, "PrototypeCodes is wrong");
            Assert.AreEqual(SurveyStatus.Draft, result.Status, "Status is wrong");
        }

        /// <summary>
        /// Accuracy test of <c>GetSurvey</c> method,
        /// </summary>
        [TestMethod]
        public void TestGetSurveyAccuracy2()
        {
            // arrange
            string id = "6";

            // act
            var result = _restApiProxy.GetSurvey(id);

            // assert
            Assert.IsNull(result, "result is expected to be null.");
        }

        /// <summary>
        /// Accuracy test of <c>GetSurvey</c> method .
        /// </summary>
        [TestMethod]
        public void TestGetSurveyAccuracy3()
        {
            // arrange
            string id = "invalidNumber";
            TestHelper.AssertThrows<ArgumentException>(() => _restApiProxy.GetSurvey(id));
        }

        /// <summary>
        /// Accuracy test of <c>GetSurvey</c> method.
        /// </summary>
        [TestMethod]
        public void TestGetSurveyAccuracy4()
        {
            // arrange
            string id = "-10";
            TestHelper.AssertThrows<ArgumentException>(() => _restApiProxy.GetSurvey(id));
        }

        #endregion

        #region UpdateSurveyStatus() method tests
        /// <summary>
        /// Tests <c>UpdateSurveyStatus</c> method accuracy.
        /// entity not found
        /// </summary>
        [TestMethod]
        public void TestUpdateSurveyStatusAccuracy1()
        {
            var id = "400";
            var status = SurveyStatus.Draft;
            TestHelper.AssertThrows<EntityNotFoundException>(() => _restApiProxy.UpdateSurveyStatus(id, status));
        }

        /// <summary>
        /// Tests <c>UpdateSurveyStatus</c> method accuracy.
        /// negative id
        /// </summary>
        [TestMethod]
        public void TestUpdateSurveyStatusAccuracy2()
        {
            var id = "-19";
            var status = SurveyStatus.Draft;
            TestHelper.AssertThrows<ArgumentException>(() => _restApiProxy.UpdateSurveyStatus(id, status));
        }

        /// <summary>
        /// Tests <c>UpdateSurveyStatus</c> method accuracy.
        /// negative id
        /// </summary>
        [TestMethod]
        public void TestUpdateSurveyStatusAccuracy6()
        {
            var id = "invalidNumber";
            var status = SurveyStatus.Draft;
            TestHelper.AssertThrows<ArgumentException>(() => _restApiProxy.UpdateSurveyStatus(id, status));
        }

        /// <summary>
        /// Tests <c>UpdateSurveyStatus</c> method accuracy.
        /// success case check status only
        /// </summary>
        [TestMethod]
        public void TestUpdateSurveyStatusAccuracy3()
        {
            var id = "1";
            var status = SurveyStatus.Draft;
            TestHelper.AssertDatabaseRecordCount("Survey", new Dictionary<string, object>
            {
                {"Id", id},
                {"Status", status.ToString()}
            }, 0);

            _restApiProxy.UpdateSurveyStatus(id, status);

            TestHelper.AssertDatabaseRecordExists("Survey", new Dictionary<string, object>
            {
                {"Id", id},
                {"Status", status.ToString()}
            });
        }

        #endregion

        #region SearchSurvey() method tests
        /// <summary>
        /// Accuracy test of <c>SearchSurvey</c> method,
        /// result should be correct.
        /// </summary>
        [TestMethod]
        public void TestSearchAccuracy1()
        {
            // arrange
            var filter = new SearchFilter();
            var name = "";

            // act
            var result = _restApiProxy.SearchSurvey(name, filter);

            // assert
            Assert.IsNotNull(result, "result is expected to be not null.");
            Assert.AreEqual(3, result.TotalRecordCount, "result.TotalRecordCount is incorrect.");
            Assert.AreEqual(1, result.TotalPageCount, "result.TotalPageCount is incorrect.");
            Assert.IsNotNull(result.Records, "result.Records is expected to be not null.");
            Assert.AreEqual(3, result.Records.Count, "result.Records.Count is incorrect.");
            Assert.IsNotNull(result.Records[0], "result.Records[0] is expected to be not null.");
            Assert.IsNotNull(result.Records[0].Name, "result.Records[0].Name is expected to be not null.");

        }

        /// <summary>
        /// Accuracy test of <c>SearchSurvey</c> method,
        /// result should be correct.
        /// </summary>
        [TestMethod]
        public void TestSearchAccuracy2()
        {
            // arrange
            var filter = new SearchFilter();
            var name = "Survey1";

            // act
            var result = _restApiProxy.SearchSurvey(name, filter);

            // assert
            Assert.IsNotNull(result, "result is expected to be not null.");
            Assert.AreEqual(1, result.TotalRecordCount, "result.TotalRecordCount is incorrect.");
            Assert.AreEqual(1, result.TotalPageCount, "result.TotalPageCount is incorrect.");
            Assert.IsNotNull(result.Records, "result.Records is expected to be not null.");
            Assert.AreEqual(1, result.Records.Count, "result.Records.Count is incorrect.");
            Assert.IsNotNull(result.Records[0], "result.Records[0] is expected to be not null.");
            Assert.IsNotNull(result.Records[0].Name, "result.Records[0].Name is expected to be not null.");
            Assert.AreEqual("Survey1", result.Records[0].Name, "result.Records[0].Name is wrong.");
            Assert.IsNull(result.Records[0].PostTestingQuestions, "result.Records[0].PostTestingQuestions " +
                                                                  "is expected to be null.");
            Assert.IsNull(result.Records[0].PrototypeCodes, "result.Records[0].PrototypeCodes " +
                                                                  "is expected to be null.");
            Assert.IsNull(result.Records[0].PostTestingQuestions, "result.Records[0].PostTestingQuestions " +
                                                              "is expected to be null.");
        }

        /// <summary>
        /// Accuracy test of <c>SearchSurvey</c> method,
        /// result should be correct.
        /// </summary>
        [TestMethod]
        public void TestSearchAccuracy3()
        {
            // arrange
            var filter = new SearchFilter();
            var name = "doesnotexist";

            // act
            var result = _restApiProxy.SearchSurvey(name, filter);

            // assert
            Assert.IsNotNull(result, "result is expected to be not null.");
            Assert.AreEqual(0, result.TotalRecordCount, "result.TotalRecordCount is incorrect.");
            Assert.AreEqual(0, result.TotalPageCount, "result.TotalPageCount is incorrect.");
            Assert.IsNotNull(result.Records, "result.Records is expected to be not null.");
            Assert.AreEqual(0, result.Records.Count, "result.Records.Count is incorrect.");


        }

        /// <summary>
        /// Accuracy test of <c>SearchSurvey</c> method,
        /// result should be correct.
        /// </summary>
        [TestMethod]
        public void TestSearchAccuracy4()
        {
            // arrange
            var filter = new SearchFilter();
            filter.SortColumn = "Name";
            filter.SortAscending = false;
            filter.PageSize = 2;
            filter.PageNumber = 2;
            var name = "";

            // act
            var result = _restApiProxy.SearchSurvey(name, filter);

            // assert
            Assert.IsNotNull(result, "result is expected to be not null.");
            Assert.AreEqual(3, result.TotalRecordCount, "result.TotalRecordCount is incorrect.");
            Assert.AreEqual(2, result.TotalPageCount, "result.TotalPageCount is incorrect.");
            Assert.IsNotNull(result.Records, "result.Records is expected to be not null.");
            Assert.AreEqual(1, result.Records.Count, "result.Records.Count is incorrect.");
            Assert.AreEqual(1, result.Records[0].Id, "result.Records[0].Id is wrong.");
            Assert.AreEqual("Survey1", result.Records[0].Name, "result.Records[0].Name is wrong.");
            Assert.AreEqual("CellName1", result.Records[0].CellName, "result.Records[0].CellName is wrong.");
            Assert.IsNull(result.Records[0].PostTestingQuestions, "result.Records[0].PostTestingQuestions " +
                                                                  "is expected to be null.");
            Assert.IsNull(result.Records[0].PrototypeCodes, "result.Records[0].PrototypeCodes " +
                                                                  "is expected to be null.");
            Assert.IsNull(result.Records[0].PostTestingQuestions, "result.Records[0].PostTestingQuestions " +
                                                              "is expected to be null.");

        }

        /// <summary>
        /// Failure test of <c>Search</c> method,
        /// </summary>
        [TestMethod]
        public void TestSearchFailureArgumentNull()
        {
            // arrange
            SearchFilter filter = null;
            var name = "";

            // act, assert
            TestHelper.AssertThrows<ArgumentNullException>(() => _restApiProxy.SearchSurvey(name, filter));
        }

        /// <summary>
        /// Failure test of <c>Search</c> method,
        /// <see cref="WebFaultException{T}"/> wrapping <see cref="ArgumentException"/> is
        /// expected to be thrown when <c>filter.SortColumn</c> is invalid.
        /// </summary>
        [TestMethod]
        public void TestSearchFailureSortColumnInvalid()
        {
            // arrange
            var filter = new SearchFilter { SortColumn = "fake" };
            var name = "";
            // act, assert
            TestHelper.AssertThrows<ArgumentException>(() => _restApiProxy.SearchSurvey(name, filter));
        }

        /// <summary>
        /// Failure test of <c>Search</c> method,
        /// <see cref="WebFaultException{ServiceFaultDetail}"/> wrapping <see cref="ArgumentException"/> is
        /// expected to be thrown when <c>filter.PageNumber</c> is invalid.
        /// </summary>
        [TestMethod]
        public void TestSearchFailurePageNumberInvalid()
        {
            // arrange
            var filter = new SearchFilter { PageNumber = -1 };
            var name = "";

            // act, assert
            TestHelper.AssertThrows<ArgumentException>(() => _restApiProxy.SearchSurvey(name, filter));
        }

        /// <summary>
        /// Failure test of <c>Search</c> method,
        /// <see cref="WebFaultException{ServiceFaultDetail}"/> wrapping <see cref="ArgumentException"/> is
        /// expected to be thrown when <c>filter.PageSize</c> is invalid.
        /// </summary>
        [TestMethod]
        public void TestSearchFailurePageSizeInvalid()
        {
            // arrange
            var filter = new SearchFilter { PageNumber = 1, PageSize = 0 };
            var name = "";

            // act, assert
            TestHelper.AssertThrows<ArgumentException>(() => _restApiProxy.SearchSurvey(name, filter));
        }
        #endregion

        #region GetAllSurveys() method tests

        /// <summary>
        /// Accuracy test of <c>GetAllSurveys</c> method,
        /// result should be correct.
        /// </summary>
        [TestMethod]
        public void TestGetAllSurveysAccuracy1()
        {
            // arrange
            var filter = new SearchFilter();


            // act
            var result = _restApiProxy.GetAllSurveys(filter);

            // assert
            Assert.IsNotNull(result, "result is expected to be not null.");
            Assert.AreEqual(3, result.TotalRecordCount, "result.TotalRecordCount is incorrect.");
            Assert.AreEqual(1, result.TotalPageCount, "result.TotalPageCount is incorrect.");
            Assert.IsNotNull(result.Records, "result.Records is expected to be not null.");
            Assert.AreEqual(2, result.Records.Count, "result.Records.Count is incorrect.");

            Assert.AreEqual(2, result.Records.Keys.Count, "result.Records.Keys.Count is wrong.");
            Assert.AreEqual(1, result.Records[SurveyStatus.Published].Count, "Survey count in published is wrong.");
            Assert.AreEqual(2, result.Records[SurveyStatus.Draft].Count, "Survey count in draft is wrong.");
        }

        /// <summary>
        /// Failure test of <c>GetAllSurveys</c> method,
        /// </summary>
        [TestMethod]
        public void TestGetAllSurveysFailureArgumentNull()
        {
            // arrange
            SearchFilter filter = null;

            // act, assert
            TestHelper.AssertThrows<ArgumentNullException>(() => _restApiProxy.GetAllSurveys(filter));
        }

        /// <summary>
        /// Failure test of <c>GetAllSurveys</c> method,
        /// <see cref="WebFaultException"/> wrapping <see cref="ArgumentException"/> is
        /// expected to be thrown when <c>filter.SortColumn</c> is invalid.
        /// </summary>
        [TestMethod]
        public void TestGetAllSurveysFailureSortColumnInvalid()
        {
            // arrange
            var filter = new SearchFilter { SortColumn = "fake" };

            // act, assert
            TestHelper.AssertThrows<ArgumentException>(() => _restApiProxy.GetAllSurveys(filter));
        }

        /// <summary>
        /// Failure test of <c>GetAllSurveys</c> method,
        /// <see cref="WebFaultException{ServiceFaultDetail}"/> wrapping <see cref="ArgumentException"/> is
        /// expected to be thrown when <c>filter.PageNumber</c> is invalid.
        /// </summary>
        [TestMethod]
        public void TestGetAllSurveysFailurePageNumberInvalid()
        {
            // arrange
            var filter = new SearchFilter { PageNumber = -1 };


            // act, assert
            TestHelper.AssertThrows<ArgumentException>(() => _restApiProxy.GetAllSurveys(filter));
        }

        /// <summary>
        /// Failure test of <c>GetAllSurveys</c> method,
        /// <see cref="WebFaultException{ServiceFaultDetail}"/> wrapping <see cref="ArgumentException"/> is
        /// expected to be thrown when <c>filter.PageSize</c> is invalid.
        /// </summary>
        [TestMethod]
        public void TestGetAllSurveysFailurePageSizeInvalid()
        {
            // arrange
            var filter = new SearchFilter { PageNumber = 1, PageSize = 0 };

            // act, assert
            TestHelper.AssertThrows<ArgumentException>(() => _restApiProxy.GetAllSurveys(filter));
        }

        #endregion

        #region GetUserSurveyStatus() method tests

        /// <summary>
        /// Accuracy test of <c>GetUserSurveyStatus</c> method .
        /// </summary>
        [TestMethod]
        public void TestGetUserSurveyStatusAccuracy2()
        {
            // arrange
            string id = "1";
            var result = _restApiProxy.GetUserSurveyStatus(id);
            Assert.AreEqual(2, result.Keys.Count, "result.Records.Keys.Count is wrong.");
            Assert.AreEqual(1, result[SurveyStatus.Published], "Survey count in published is wrong.");
            Assert.AreEqual(2, result[SurveyStatus.Draft], "Survey count in draft is wrong.");

        }

        /// <summary>
        /// Accuracy test of <c>GetUserSurveyStatus</c> method .
        /// </summary>
        [TestMethod]
        public void TestGetUserSurveyStatusAccuracy3()
        {
            // arrange
            string id = "invalidNumber";
            TestHelper.AssertThrows<ArgumentException>(() => _restApiProxy.GetUserSurveyStatus(id));
        }

        /// <summary>
        /// Accuracy test of <c>GetUserSurveyStatus</c> method.
        /// </summary>
        [TestMethod]
        public void TestGetUserSurveyStatusAccuracy4()
        {
            // arrange
            string id = "-10";
            TestHelper.AssertThrows<ArgumentException>(() => _restApiProxy.GetUserSurveyStatus(id));
        }
        #endregion

        #region GetQuickAccessSurveys() method tests

        /// <summary>
        /// Accuracy test of <c>GetQuickAccessSurveys</c> method,
        /// </summary>
        [TestMethod]
        public void TestGetQuickAccessSurveys1()
        {
            // arrange
            string id = "1";

            // act
            var result = _restApiProxy.GetQuickAccessSurveys(id);

            // assert
            Assert.IsNotNull(result, "result is not expected to be null.");
            Assert.AreEqual(2, result.Count, "result count is wrong");
            Assert.AreEqual(1, result[0].SurveyId, "result[0].SurveyId is wrong.");
            Assert.AreEqual(1, result[0].Position, "result[0].Position is wrong.");
            Assert.AreEqual("CellName1", result[0].CellName, "result[0].CellName is wrong.");
            Assert.AreEqual("Survey1", result[0].SurveyName, "result[0].SurveyName is wrong.");
            Assert.AreEqual(2, result[1].SurveyId, "result[1].SurveyId is wrong.");
            Assert.AreEqual(2, result[1].Position, "result[1].Position is wrong.");
            Assert.AreEqual("CellName2", result[1].CellName, "result[1].CellName is wrong.");
            Assert.AreEqual("Survey2", result[1].SurveyName, "result[1].SurveyName is wrong.");
        }

        /// <summary>
        /// Accuracy test of <c>GetQuickAccessSurveys</c> method,
        /// </summary>
        [TestMethod]
        public void TestGetQuickAccessSurveys2()
        {
            // arrange
            string id = "2";

            // act
            var result = _restApiProxy.GetQuickAccessSurveys(id);

            // assert
            Assert.IsNotNull(result, "result is not expected to be null.");
            Assert.AreEqual(0, result.Count, "result count is wrong");
        }

        /// <summary>
        /// Accuracy test of <c>GetQuickAccessSurveys</c> method,
        /// </summary>
        [TestMethod]
        public void TestGetQuickAccessSurveys5()
        {
            // arrange
            string id = "11";

            // act
            TestHelper.AssertThrows<EntityNotFoundException>(() => _restApiProxy.GetQuickAccessSurveys(id));
        }

        /// <summary>
        /// Accuracy test of <c>GetQuickAccessSurveys</c> method .
        /// </summary>
        [TestMethod]
        public void TestGetQuickAccessSurveys3()
        {
            // arrange
            string id = "invalidNumber";
            TestHelper.AssertThrows<ArgumentException>(() => _restApiProxy.GetQuickAccessSurveys(id));
        }

        /// <summary>
        /// Accuracy test of <c>GetQuickAccessSurveys</c> method.
        /// </summary>
        [TestMethod]
        public void TestGetQuickAccessSurveys4()
        {
            // arrange
            string id = "-10";
            TestHelper.AssertThrows<ArgumentException>(() => _restApiProxy.GetQuickAccessSurveys(id));
        }
        #endregion

        #region UpdateQuickAccessSurveys() method tests

        /// <summary>
        /// Accuracy test of <c>UpdateQuickAccessSurveys</c> method.
        /// verify update case
        /// </summary>
        [TestMethod]
        public void TestUpdateQuickAccessSurveys1()
        {
            // arrange
            string id = "1";
            var previousQuickAcces = _restApiProxy.GetQuickAccessSurveys(id);

            IList<QuickAccessSurvey> surveys = new List<QuickAccessSurvey>();
            surveys.Add(new QuickAccessSurvey
            {
                Position = 1,
                SurveyId = 1,
            });
            // act
            _restApiProxy.UpdateQuickAccessSurveys(id, surveys);

            //verify
            var newQuickAccess = _restApiProxy.GetQuickAccessSurveys(id);

            Assert.AreNotEqual(previousQuickAcces.Count, newQuickAccess.Count, "updated record is wrong");

        }

        /// <summary>
        /// Accuracy test of <c>UpdateQuickAccessSurveys</c> method.
        /// </summary>
        [TestMethod]
        public void TestUpdateQuickAccessSurveys7()
        {
            // arrange
            string id = "1";
            var previousQuickAcces = _restApiProxy.GetQuickAccessSurveys(id);
            IList<QuickAccessSurvey> surveys = new List<QuickAccessSurvey>();


            _restApiProxy.UpdateQuickAccessSurveys(id, surveys);

            var newQuickAccess = _restApiProxy.GetQuickAccessSurveys(id);

            Assert.AreNotEqual(previousQuickAcces, newQuickAccess, "update is wrong");
            Assert.AreEqual(0, newQuickAccess.Count, "update is wrong");
        }

        /// <summary>
        /// Accuracy test of <c>UpdateQuickAccessSurveys</c> method,
        /// </summary>
        [TestMethod]
        public void TestUpdateQuickAccessSurveys5()
        {
            // arrange
            string id = "11";
            IList<QuickAccessSurvey> surveys = new List<QuickAccessSurvey>();
            surveys.Add(new QuickAccessSurvey
            {
                Position = 1,
                SurveyId = 1,
            });
            // act
            TestHelper.AssertThrows<EntityNotFoundException>(
                () => _restApiProxy.UpdateQuickAccessSurveys(id, surveys));
        }

        /// <summary>
        /// Accuracy test of <c>UpdateQuickAccessSurveys</c> method .
        /// </summary>
        [TestMethod]
        public void TestUpdateQuickAccessSurveys3()
        {
            // arrange
            string id = "invalidNumber";
            IList<QuickAccessSurvey> surveys = new List<QuickAccessSurvey>();
            surveys.Add(new QuickAccessSurvey
            {
                Position = 1,
                SurveyId = 1,
            });
            TestHelper.AssertThrows<ArgumentException>(() => _restApiProxy.UpdateQuickAccessSurveys(id, surveys));
        }

        /// <summary>
        /// Accuracy test of <c>UpdateQuickAccessSurveys</c> method.
        ///  negatvie user id.
        /// </summary>
        [TestMethod]
        public void TestUpdateQuickAccessSurveys4()
        {
            // arrange
            string id = "-10";
            IList<QuickAccessSurvey> surveys = new List<QuickAccessSurvey>();
            surveys.Add(new QuickAccessSurvey
            {
                Position = 1,
                SurveyId = 1,
            });

            TestHelper.AssertThrows<ArgumentException>(() => _restApiProxy.UpdateQuickAccessSurveys(id, surveys));
        }


        /// <summary>
        /// Accuracy test of <c>UpdateQuickAccessSurveys</c> method.
        /// check negative survey ids
        /// </summary>
        [TestMethod]
        public void TestUpdateQuickAccessSurveys8()
        {
            // arrange
            string id = "-10";
            IList<QuickAccessSurvey> surveys = new List<QuickAccessSurvey>();
            surveys.Add(new QuickAccessSurvey
            {
                Position = -1,
                SurveyId = -1,
            });

            TestHelper.AssertThrows<ArgumentException>(() => _restApiProxy.UpdateQuickAccessSurveys(id, surveys));
        }

        /// <summary>
        /// Accuracy test of <c>UpdateQuickAccessSurveys</c> method.
        /// check negative survey ids
        /// </summary>
        [TestMethod]
        public void TestUpdateQuickAccessSurveys9()
        {
            // arrange
            string id = "-10";
            IList<QuickAccessSurvey> surveys = null;

            TestHelper.AssertThrows<ArgumentException>(() => _restApiProxy.UpdateQuickAccessSurveys(id, surveys));
        }
        #endregion

        #region ExportParticipantCredentials method tests
        [TestMethod]
        public void TestExportParticipantCredentialsAccuracy()
        {
            // arrange
            var result =
                TestHelper.ProcessHttpRequest(ServiceHostAddress + "Surveys/1/ExportParticipantCredentials", "GET");
            // act
            // assert
            Assert.IsNotNull(result, "result is expected to be null.");
        }

        /// <summary>
        /// Accuracy test of <c>ExportParticipantCredentials</c> method,
        /// </summary>
        [TestMethod]
        public void TestExportParticipantCredentialsAccuracy2()
        {
            // arrange
            string id = "6";

            // act
            TestHelper.AssertThrows<EntityNotFoundException>(() => _restApiProxy.ExportParticipantCredentials(id));

        }

        /// <summary>
        /// Accuracy test of <c>ExportParticipantCredentials</c> method .
        /// </summary>
        [TestMethod]
        public void TestExportParticipantCredentialsAccuracy3()
        {
            // arrange
            string id = "invalidNumber";
            TestHelper.AssertThrows<ArgumentException>(() => _restApiProxy.ExportParticipantCredentials(id));
        }

        /// <summary>
        /// Accuracy test of <c>ExportParticipantCredentials</c> method.
        /// </summary>
        [TestMethod]
        public void TestExportParticipantCredentialsAccuracy4()
        {
            // arrange
            string id = "-10";
            TestHelper.AssertThrows<ArgumentException>(() => _restApiProxy.ExportParticipantCredentials(id));
        }
        #endregion

        #region ExportSurveyResults method tests
        [TestMethod]
        public void TestExportSurveyResultsAccuracy()
        {
            // arrange
            var result =
                TestHelper.ProcessHttpRequest(ServiceHostAddress + "Surveys/1/ExportSurveyResults", "GET");
            // act
            // assert
            Assert.IsNotNull(result, "result is expected to be null.");
        }

        /// <summary>
        /// Accuracy test of <c>ExportSurveyResults</c> method,
        /// </summary>
        [TestMethod]
        public void TestExportSurveyResultsAccuracy2()
        {
            // arrange
            string id = "6";

            // act
            TestHelper.AssertThrows<EntityNotFoundException>(() => _restApiProxy.ExportSurveyResults(id));

        }

        /// <summary>
        /// Accuracy test of <c>ExportSurveyResults</c> method .
        /// </summary>
        [TestMethod]
        public void TestExportSurveyResultsAccuracy3()
        {
            // arrange
            string id = "invalidNumber";
            TestHelper.AssertThrows<ArgumentException>(() => _restApiProxy.ExportSurveyResults(id));
        }

        /// <summary>
        /// Accuracy test of <c>ExportSurveyResults</c> method.
        /// </summary>
        [TestMethod]
        public void TestExportSurveyResultsAccuracy4()
        {
            // arrange
            string id = "-10";
            TestHelper.AssertThrows<ArgumentException>(() => _restApiProxy.ExportSurveyResults(id));
        }
        #endregion

        #region ExportProductImages method tests
        [TestMethod]
        public void TestExportProductImagesAccuracy()
        {
            var result =
                TestHelper.ProcessHttpRequest(ServiceHostAddress + "Surveys/1/ExportProductImages", "GET");
            // act
            // assert
            Assert.IsNotNull(result, "result is expected to be null.");
        }

        /// <summary>
        /// Accuracy test of <c>ExportProductImages</c> method,
        /// </summary>
        [TestMethod]
        public void TestExportProductImagesAccuracy2()
        {
            // arrange
            string id = "6";

            // act
            TestHelper.AssertThrows<EntityNotFoundException>(() => _restApiProxy.ExportProductImages(id));

        }

        /// <summary>
        /// Accuracy test of <c>ExportProductImages</c> method .
        /// </summary>
        [TestMethod]
        public void TestExportProductImagesAccuracy3()
        {
            // arrange
            string id = "invalidNumber";
            TestHelper.AssertThrows<ArgumentException>(() => _restApiProxy.ExportProductImages(id));
        }

        /// <summary>
        /// Accuracy test of <c>ExportProductImages</c> method.
        /// </summary>
        [TestMethod]
        public void TestExportProductImagesAccuracy4()
        {
            // arrange
            string id = "-10";
            TestHelper.AssertThrows<ArgumentException>(() => _restApiProxy.ExportProductImages(id));
        }
        #endregion

    }
}

