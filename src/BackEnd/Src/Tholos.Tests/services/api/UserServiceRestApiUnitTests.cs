﻿/*
 * Copyright (c) 2015, TopCoder, Inc. All rights reserved.
 */

using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Tholos.Services;
using Tholos.Services.Impl;
using Tholos.Tests.services.api.proxy;

namespace Tholos.Tests.services.api
{
    /// <summary>
    /// <para>
    /// Unit tests for <see cref="UserService"/> class.
    /// </para>
    /// <para>
    /// Changes in version 1.1 (THOLOS - Integrate Prototype with Backend APIs):
    /// <list type="bullet">
    /// <item>
    /// <description>Fixed the test for user role.</description>
    /// </item>
    /// </list>
    /// </para>
    /// </summary>
    ///
    /// <author>veshu</author>
    /// <author>duxiaoyang</author>
    ///
    /// <version>1.1</version>
    /// <since>1.0</since>
    ///
    /// <copyright>
    /// Copyright (c) 2015, TopCoder, Inc. All rights reserved.
    /// </copyright>
    [TestClass]
    public class UserServiceRestApiUnitTests : BasePersistenceServiceUnitTest<UserService>
    {
        /// <summary>
        /// Represents the <see cref="UserServiceRestApiProxy"/> instance used for REST API tests.
        /// </summary>
        private readonly IUserService _restApiProxy = new UserServiceRestApiProxy();


        /// <summary>
        /// Accuracy test of the class constructor.
        /// </summary>
        [TestMethod]
        public void TestCtorAccuracy()
        {
            Assert.AreEqual(typeof(BasePersistenceService), _instance.GetType().BaseType,
                "The class should inherit from BasePersistenceService.");
            Assert.IsTrue(_instance is IUserService,
                "The class should implement IUserService.");
        }
        #region GetUser tests

        /// <summary>
        /// Accuracy test of <c>GetUser</c> method.
        /// </summary>
        [TestMethod]
        public void TestGetUserAccuracy1()
        {
            // arrange
            string username = "topcoder";

            // act
            var user = _restApiProxy.GetUser(username);

            // assert
            Assert.IsNotNull(user, "user is expected to be not null.");
            Assert.AreEqual(1, user.Id, "user.id is wrong.");
            Assert.AreEqual("Topcoder Manager", user.Name, "user.Name is wrong.");
            Assert.AreEqual(@"c:\tholos\ic", user.UserpicPath, "user.UserpicPath is wrong.");
            Assert.IsNotNull(user.Roles, "user.Roles is expected to be not null.");
            Assert.AreEqual(1, user.Roles.Count, "user.roles is wrong");
            Assert.AreEqual(1, user.Roles[0].Id);
        }

        /// <summary>
        /// Accuracy test of <c>GetUser</c> method
        /// </summary>
        [TestMethod]
        public void TestGetUserAccuracy2()
        {
            // arrange
            string username = "NotExistUserName";

            // act
            var user = _restApiProxy.GetUser(username);

            // assert
            Assert.IsNull(user, "user is expected to be null.");
        }

        /// <summary>
        /// Accuracy test of <c>GetUser</c> method with empty username.
        /// </summary>
        [TestMethod]
        public void TestGetUserAccuracy3()
        {
            // arrange
            string username = "";
            TestHelper.AssertThrows<ArgumentException>(() => _restApiProxy.GetUser(username));
        }

        /// <summary>
        /// Accuracy test of <c>GetUser</c> method with null username.
        /// </summary>
        [TestMethod]
        public void TestGetUserAccuracy4()
        {
            // arrange
            string username = null;
            TestHelper.AssertThrows<ArgumentNullException>(() => _restApiProxy.GetUser(username));
        }

        #endregion
    }
}
