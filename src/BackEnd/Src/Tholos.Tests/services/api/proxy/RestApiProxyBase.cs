﻿/*
 * Copyright (c) 2015, TopCoder, Inc. All rights reserved.
 */

using System.IO;
using System.Runtime.Serialization.Json;
using System.Text;
using Tholos.Services.Impl;

namespace Tholos.Tests.services.api
{
    /// <summary>
    /// <para>
    /// Represents base class for all REST API proxy classes. It provides simplified methods to get and send responses.
    /// </para>
    /// <para>
    /// Changes in version 1.1 (THOLOS - Integrate Prototype with Backend APIs):
    /// <list type="bullet">
    /// <item>
    /// <description>Added the option to send request using participant account.</description>
    /// </item>
    /// </list>
    /// </para>
    /// </summary>
    ///
    /// <typeparam name="TService">The actual type of the target WCF service.</typeparam>
    ///
    /// <author>veshu</author>
    /// <author>duxiaoyang</author>
    ///
    /// <version>1.1</version>
    /// <since>1.0</since>
    ///
    /// <copyright>
    /// Copyright (c) 2015, TopCoder, Inc. All rights reserved.
    /// </copyright>
    public abstract class RestApiProxyBase<TService>
        where TService : BaseService
    {
        /// <summary>
        /// The service host address.
        /// </summary>
        private static readonly string ServiceHostAddress =
            TestHelper.WcfServiceHostBaseUri + typeof(TService).Name + ".svc/";

        /// <summary>
        /// Sends the request to the RESTful WCF service and returns deserialized entity from the response.
        /// </summary>
        /// <typeparam name="T">The type of the response entity.</typeparam>
        /// <param name="query">The query portion of the Uri.</param>
        /// <param name="method">The HTTP request method.</param>
        /// <param name="parameter">The optional parameter that will be sent in request body.</param>
        /// <param name="serializeWithType">if true typehandling will be all</param>
        /// <param name="asParticipant">True if the request is sent as participant</param>
        /// <returns>The deserialized entity from the response.</returns>
        protected static T GetResponse<T>(string query, string method, object parameter = null
            , bool serializeWithType = false, bool asParticipant = false)
        {
            string response = TestHelper.ProcessHttpRequest(ServiceHostAddress + query, method, parameter,
                serializeWithType, asParticipant);

            if (response.Length > 0)
            {
                DataContractJsonSerializer deserializer = new DataContractJsonSerializer(typeof(T));
                using (MemoryStream stream = new MemoryStream(Encoding.Unicode.GetBytes(response)))
                {
                    return (T)deserializer.ReadObject(stream);
                }
            }
            else
            {
                return default(T);
            }
        }

        /// <summary>
        /// Sends the request to the RESTful WCF service.
        /// </summary>
        /// <param name="query">The query portion of the Uri.</param>
        /// <param name="method">The HTTP request method.</param>
        /// <param name="parameter">The optional parameter that will be sent in request body.</param>
        /// <param name="serializeWithType">if true typehandling will be all</param>
        /// <param name="asParticipant">True if the request is sent as participant</param>
        protected static void SendRequest(string query, string method, object parameter = null
            , bool serializeWithType = false, bool asParticipant = false)
        {
            TestHelper.ProcessHttpRequest(ServiceHostAddress + query, method, parameter, serializeWithType, asParticipant);
        }
    }
}
