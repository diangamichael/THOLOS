﻿/*
 * Copyright (c) 2015, TopCoder, Inc. All rights reserved.
 */

using System.Collections.Generic;
using System.IO;
using Tholos.Entities;
using Tholos.Services;
using Tholos.Services.Impl;

namespace Tholos.Tests.services.api.proxy
{
    /// <summary>
    /// <para>
    /// Represents proxy class for hosted WCF RESTful <see cref="ParticipantSurveyService"/> service.
    /// </para>
    /// <para>
    /// Changes in version 1.1 (THOLOS - Integrate Prototype with Backend APIs):
    /// <list type="bullet">
    /// <item>
    /// <description>Changed to send request using participant account.</description>
    /// </item>
    /// </list>
    /// </para>
    /// </summary>
    ///
    /// <author>veshu</author>
    /// <author>duxiaoyang</author>
    ///
    /// <version>1.1</version>
    /// <since>1.0</since>
    ///
    /// <copyright>
    /// Copyright (c) 2015, TopCoder, Inc. All rights reserved.
    /// </copyright>
    public class ParticipantSurveyServiceRestApiProxy : RestApiProxyBase<ParticipantSurveyService>
        , IParticipantSurveyService
    {
        /// <summary>
        /// uploads the before photo file.
        /// </summary>
        /// <param name="filename">The filename.</param>
        /// <param name="photo">The photo stream.</param>
        /// <returns>The filename</returns>
        public string UploadPhoto(string filename, Stream photo)
        {
            return GetResponse<string>("/ParticipantSurveys/UploadPhoto?filename=" + filename, "POST", photo);
        }

        /// <summary>
        /// Gets participant survey.
        /// </summary>
        /// <param name="participantSurveyId">The participant survey id.</param>
        /// <returns>The participant survey.</returns>
        public ParticipantSurvey GetParticipantSurvey(string participantSurveyId)
        {
            return GetResponse<ParticipantSurvey>("/ParticipantSurveys/" + participantSurveyId, "GET", null, false, true);
        }

        /// <summary>
        /// Creates PrototypeTest.
        /// </summary>
        /// <param name="participantSurveyId">The participant survey id.</param>
        /// <param name="prototypeTest">The prototype test.</param>
        /// <param name="beforePhoto">The before photo name.</param>
        /// <returns>The Id of created entity.</returns>
        public long CreatePrototypeTest(string participantSurveyId, PrototypeTest prototypeTest
            , string beforePhoto)
        {
            return
                GetResponse<long>(
                    "/ParticipantSurveys/" + participantSurveyId + "/PrototypeTests?beforePhotoName=" +
                    beforePhoto, "POST", prototypeTest, false, true);
        }

        /// <summary>
        /// Gets proptotype test by id.
        /// </summary>
        /// <param name="prototypeTestId">The entity id.</param>
        /// <returns>The entity.</returns>
        public PrototypeTest GetPrototypeTest(string prototypeTestId)
        {
            return GetResponse<PrototypeTest>("/PrototypeTests/" + prototypeTestId, "GET", null, false, true);
        }

        /// <summary>
        /// Creates prototype answer of a test
        /// </summary>
        /// <param name="prototypeTestId">The prototype test id.</param>
        /// <param name="answer">The answer</param>
        /// <returns>The id of created answer. </returns>
        public long SubmitPrototypeAnswer(string prototypeTestId, Answer answer)
        {
            if (answer != null)
            {
                return GetResponse<long>("/PrototypeTests/" + prototypeTestId + "/PrototypeAnswers", "POST", answer,
                    true, true);
            }
            else
            {
                return GetResponse<long>("/PrototypeTests/" + prototypeTestId + "/PrototypeAnswers", "POST", answer, false, true);
            }

        }

        /// <summary>
        /// Updates prototype answer.
        /// </summary>
        /// <param name="answer">The prototype answer.</param>
        public void UpdatePrototypeAnswer(Answer answer)
        {
            if (answer != null)
            {
                SendRequest("/PrototypeAnswers", "PUT", answer, true, true);
            }
            else
            {
                SendRequest("/PrototypeAnswers", "PUT", null, false, true);
            }
        }

        /// <summary>
        /// Completes prototype test.
        /// </summary>
        /// <param name="prototypeTestId">The prototype test id.</param>
        /// <param name="fileName">The filename of after photo.</param>
        /// <param name="afterWeight">The after weight.</param>
        /// <param name="afterPhoto">The after photo.</param>
        public void CompletePrototypeTest(string prototypeTestId, string fileName, string afterWeight,
            Stream afterPhoto)
        {
            SendRequest(
                "/PrototypeTests/" + prototypeTestId + "/Complete?fileName=" + fileName + "&afterWeight=" +
                afterWeight
                , "PUT", afterPhoto);
        }

        /// <summary>
        /// Creates post testing answer.
        /// </summary>
        /// <param name="participantSurveyId">The participant survey id.</param>
        /// <param name="postTestingAnswer">The post testing answer.</param>
        public void SubmitPostTestingAnswer(string participantSurveyId, Answer postTestingAnswer)
        {
            if (postTestingAnswer != null)
            {
                SendRequest("/ParticipantSurveys/" + participantSurveyId + "/PostTestingAnswers",
                    "POST", postTestingAnswer, true, true);
            }
            else
            {
                SendRequest("/ParticipantSurveys/" + participantSurveyId + "/PostTestingAnswers",
                   "POST", postTestingAnswer, false, true);
            }
        }

        /// <summary>
        /// Completes the post testing.
        /// </summary>
        /// <param name="participantSurveyId">The participant survey id.</param>
        public void CompletePostTesting(string participantSurveyId)
        {
            SendRequest("/ParticipantSurveys/" + participantSurveyId + "/Complete", "PUT", null, false, true);
        }

        /// <summary>
        /// Get prototype questions.
        /// </summary>
        /// <param name="participantSurveyId">The participant survey id.</param>
        /// <returns>The questions.</returns>
        public IList<Question> GetPrototypeQuestions(string participantSurveyId)
        {
            return GetResponse<IList<Question>>("/ParticipantSurveys/" + participantSurveyId + "/PrototypeQuestions"
                , "GET", null, false, true);
        }

        /// <summary>
        /// Get PostTesting questions.
        /// </summary>
        /// <param name="participantSurveyId">The participant survey id.</param>
        /// <returns>The questions.</returns>
        public IList<Question> GetPostTestingQuestions(string participantSurveyId)
        {
            return GetResponse<IList<Question>>("/ParticipantSurveys/" + participantSurveyId + "/PostTestingQuestions"
                , "GET", null, false, true);
        }

        /// <summary>
        /// Get survey prototype codes.
        /// </summary>
        /// <param name="surveyId">THe survey id.</param>
        /// <returns>The prototype codes.</returns>
        public IList<string> GetSurveyPrototypeCodes(string surveyId)
        {
            return GetResponse<IList<string>>("/Surveys/" + surveyId + "/PrototypeCodes", "GET", null, false, true);
        }
    }
}
