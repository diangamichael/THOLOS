/*
 * @Description: Defines the tables for Tholos Survey applications
 * @Author: veshu
 * @Version: 1.0
 * @Copyright: Copyright (C) 2015, TopCoder, Inc. All rights reserved.
 */
DROP TABLE IF EXISTS UserRole;
DROP TABLE IF EXISTS ParticipantRole;
DROP TABLE IF EXISTS Role;
DROP TABLE IF EXISTS Users;
DROP TABLE IF EXISTS ParticipantCredentials;
DROP TABLE IF EXISTS SurveyProrotypeQuestion;
DROP TABLE IF EXISTS PrototypeCode;
DROP TABLE IF EXISTS SurveyPostTestingQuestion;
DROP TABLE IF EXISTS ProfileRangeAnswer;
DROP TABLE IF EXISTS ProfileSingleInputAnswer;
DROP TABLE IF EXISTS ProfileSingleAnswer;
DROP TABLE IF EXISTS ProfileAnswer;
DROP TABLE IF EXISTS ProfileRangeAnswerOption;
DROP TABLE IF EXISTS ProfileSingleAnswerOption;
DROP TABLE IF EXISTS ProfileAnswerOption;
DROP TABLE IF EXISTS ProfileQuestionCondition;
DROP TABLE IF EXISTS ProfileQuestion;
DROP TABLE IF EXISTS PostTestingRangeAnswer;
DROP TABLE IF EXISTS PostTestingSingleInputAnswer;
DROP TABLE IF EXISTS PostTestingSingleAnswer;
DROP TABLE IF EXISTS PostTestingAnswer;
DROP TABLE IF EXISTS PostTestingSingleAnswerOption;
DROP TABLE IF EXISTS PostTestingRangeAnswerOption;
DROP TABLE IF EXISTS PostTestingAnswerOption;
DROP TABLE IF EXISTS PostTestingQuestionCondition;
DROP TABLE IF EXISTS PostTestingQuestion;
DROP TABLE IF EXISTS PrototypeRangeAnswer;
DROP TABLE IF EXISTS PrototypeSingleInputAnswer;
DROP TABLE IF EXISTS PrototypeSingleAnswer;
DROP TABLE IF EXISTS PrototypeAnswer;
DROP TABLE IF EXISTS PrototypeSingleAnswerOption;
DROP TABLE IF EXISTS PrototypeRangeAnswerOption;
DROP TABLE IF EXISTS PrototypeAnswerOption;
DROP TABLE IF EXISTS PrototypeQuestionCondition;
DROP TABLE IF EXISTS PrototypeQuestion;
DROP TABLE IF EXISTS PrototypeTest;
DROP TABLE IF EXISTS QuickAccessSurvey;
DROP TABLE IF EXISTS ParticipantSurvey;
DROP TABLE IF EXISTS Survey;
DROP TABLE IF EXISTS Participant;


-- -----------------------------------------------------
-- Table `Participant`
-- -----------------------------------------------------
CREATE TABLE `Participant`( 
  `Id` bigint AUTO_INCREMENT NOT NULL,
  `Username` varchar(64) NOT NULL,
  `SurveyId` bigint NOT NULL,
  `BirthDate` DateTime NULL,
  `Weight` int NOT NULL,
  `HeightFeet` int NOT NULL,
  `HeightInches` int NOT NULL,
  PRIMARY KEY (`Id`)
);
CREATE UNIQUE INDEX IX_Participant_Username_SurveyId   ON `Participant` (SurveyId, Username);

-- -----------------------------------------------------
-- Table `ParticipantSurvey`
-- -----------------------------------------------------
CREATE TABLE `ParticipantSurvey` (
  `Id` bigint NOT NULL,
  `ParticipantId` bigint NOT NULL,
  `SurveyId` bigint NOT NULL,
  `CurrentPrototypeTestId` bigint NOT NULL,
  PRIMARY KEY (`Id`)
);

-- -----------------------------------------------------
-- Table `Survey`
-- -----------------------------------------------------
CREATE TABLE `Survey` (
  `Id` bigint AUTO_INCREMENT NOT NULL,
  `Name` varchar(64) NOT NULL,
  `CellName` varchar(64) NOT NULL,
  `ParticipantsNumber` int NOT NULL,
  `Status` varchar(32) NOT NULL,
  `DateCreated` TIMESTAMP NOT NULL  DEFAULT CURRENT_TIMESTAMP,
  `CompletedSurveysNumber` int NOT NULL,
  `Draft` bit NOT NULL,
  `CreatedBy` bigint NOT NULL,
  PRIMARY KEY (`Id`)
);

-- -----------------------------------------------------
-- Table `PrototypeQuestion`
-- -----------------------------------------------------
CREATE TABLE `PrototypeQuestion` (
  `Id` bigint NOT NULL,
  `Text` varchar(1024) NOT NULL,
  `QuestionType` varchar(32) NOT NULL,
  `Conditional` bit NOT NULL,
  PRIMARY KEY (`Id`)
);

-- -----------------------------------------------------
-- Table `PrototypeAnswerOption`
-- -----------------------------------------------------
CREATE TABLE `PrototypeAnswerOption` (
  `Id` bigint NOT NULL,
  `QuestionId` bigint NOT NULL,
  `AnswerType` varchar(32) NOT NULL,
  PRIMARY KEY (`Id`),
  CONSTRAINT `PrototypeAnswerOption_PrototypeQuestion_fk`
  FOREIGN KEY (`QuestionId`)
  REFERENCES `PrototypeQuestion` (`Id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION
);
CREATE INDEX `PrototypeAnswerOption_PrototypeQuestion_fk_idx` ON `PrototypeAnswerOption` (`QuestionId` ASC);

-- -----------------------------------------------------
-- Table `PrototypeSingleAnswerOption`
-- -----------------------------------------------------
CREATE TABLE `PrototypeSingleAnswerOption` (
  `Id` bigint NOT NULL,
  `Value` int NOT NULL,
  `Label` varchar(64) NOT NULL,
  `ConditionalQuestionId` bigint NULL,
  PRIMARY KEY (`Id`),
  CONSTRAINT `PrototypeSingleAnswerOption_PrototypeAnswerOption_fk`
  FOREIGN KEY (`Id`)
  REFERENCES `PrototypeAnswerOption` (`Id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION
);

-- -----------------------------------------------------
-- Table `PrototypeRangeAnswerOption`
-- -----------------------------------------------------
CREATE TABLE `PrototypeRangeAnswerOption` (
  `Id` bigint NOT NULL,
  `FromValueLabel` varchar(64) NOT NULL,
  `ToValueLabel` varchar(64) NOT NULL,
  `FromValue` int NOT NULL,
  `ToValue` int NOT NULL,
  `Increment` int NULL,
  PRIMARY KEY (`Id`),
  CONSTRAINT `PrototypeRangeAnswerOption_PrototypeAnswerOption_FK`
  FOREIGN KEY (`Id`)
  REFERENCES `PrototypeAnswerOption` (`Id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION
);

-- -----------------------------------------------------
-- Table `SurveyProrotypeQuestion`
-- -----------------------------------------------------
CREATE TABLE `SurveyProrotypeQuestion` (
  `Id` bigint AUTO_INCREMENT NOT NULL,
  `SurveyId` bigint NOT NULL,
  `QuestionId` bigint NOT NULL,
  `QuestionNumber` int NOT NULL,
  PRIMARY KEY (`Id`),
  CONSTRAINT `SurveyProrotypeQuestion_Survey_FK`
  FOREIGN KEY (`SurveyId`)
  REFERENCES `Survey` (`Id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
  CONSTRAINT `SurveyProrotypeQuestion_PrototypeQuestion_FK`
  FOREIGN KEY (`QuestionId`)
  REFERENCES `PrototypeQuestion` (`Id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION
);
CREATE INDEX `SurveyProrotypeQuestion_Survey_FK_idx` ON `SurveyProrotypeQuestion` (`SurveyId` ASC);
CREATE INDEX `SurveyProrotypeQuestion_PrototypeQuestion_FK_idx` ON `SurveyProrotypeQuestion` (`QuestionId` ASC);

-- -----------------------------------------------------
-- Table `PostTestingQuestion`
-- -----------------------------------------------------
CREATE TABLE `PostTestingQuestion` (
  `Id` bigint NOT NULL,
  `Text` varchar(1024) NOT NULL,
  `Conditional` bit NOT NULL,
  `QuestionType` varchar(32) NOT NULL,
  PRIMARY KEY (`Id`)
);

-- -----------------------------------------------------
-- Table `PostTestingAnswerOption`
-- -----------------------------------------------------
CREATE TABLE `PostTestingAnswerOption` (
  `Id` bigint NOT NULL,
  `QuestionId` bigint NOT NULL,
  `AnswerType` varchar(32) NOT NULL,
  PRIMARY KEY (`Id`),
  CONSTRAINT `PostTestingAnswerOption_PostTestingQuestion_fk0`
  FOREIGN KEY (`QuestionId`)
  REFERENCES `PostTestingQuestion` (`Id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION
);
CREATE INDEX `PostTestingAnswerOption_PostTestingQuestion_fk_idx` ON `PostTestingAnswerOption` (`QuestionId` ASC);

-- -----------------------------------------------------
-- Table `PostTestingRangeAnswerOption`
-- -----------------------------------------------------
CREATE TABLE `PostTestingRangeAnswerOption` (
  `Id` bigint NOT NULL,
  `FromValueLabel` varchar(64) NOT NULL,
  `ToValueLabel` varchar(64) NOT NULL,
  `FromValue` int NOT NULL,
  `ToValue` int NOT NULL,
  `Increment` int NULL,
  PRIMARY KEY (`Id`),
  CONSTRAINT `PrototypeRangeAnswerOption_PostTestingAnswerOption_FK0`
  FOREIGN KEY (`Id`)
  REFERENCES `PostTestingAnswerOption` (`Id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION
);

-- -----------------------------------------------------
-- Table `PostTestingSingleAnswerOption`
-- -----------------------------------------------------
CREATE TABLE `PostTestingSingleAnswerOption` (
  `Id` bigint NOT NULL,
  `Value` int NOT NULL,
  `Label` varchar(64) NOT NULL,
  `ConditionalQuestionId` bigint NULL,
  PRIMARY KEY (`Id`),
  CONSTRAINT `PostTestingSingleAnswerOption_PostTestingAnswerOption_fk0`
  FOREIGN KEY (`Id`)
  REFERENCES `PostTestingAnswerOption` (`Id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION
);

-- -----------------------------------------------------
-- Table `SurveyPostTestingQuestion`
-- -----------------------------------------------------
CREATE TABLE `SurveyPostTestingQuestion` (
  `Id` bigint AUTO_INCREMENT NOT NULL,
  `SurveyId` bigint NOT NULL,
  `QuestionId` bigint NOT NULL,
  `QuestionNumber` int NOT NULL,
  PRIMARY KEY (`Id`),
  CONSTRAINT `SurveyPostTestingQuestion_Survey_FK`
  FOREIGN KEY (`SurveyId`)
  REFERENCES `Survey` (`Id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
  CONSTRAINT `SurveyPostTestingQuestion_PostTestingQuestion_FK`
  FOREIGN KEY (`QuestionId`)
  REFERENCES `PostTestingQuestion` (`Id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION
);
CREATE INDEX `SurveyPostTestingQuestion_Survey_FK_idx` ON `SurveyPostTestingQuestion` (`SurveyId` ASC);
CREATE INDEX `SurveyPostTestingQuestion_PostTestingQuestion_FK_idx` ON `SurveyPostTestingQuestion` (`QuestionId` ASC);

-- -----------------------------------------------------
-- Table `PrototypeCode`
-- -----------------------------------------------------
CREATE TABLE `PrototypeCode` (
  `Id` bigint AUTO_INCREMENT NOT NULL,
  `Code` varchar(64) NOT NULL,
  `PrototypesPerCode` int NOT NULL,
  `SurveyId` bigint NOT NULL,
  PRIMARY KEY (`Id`),
  CONSTRAINT `PrototypeCode_Survey_FK`
  FOREIGN KEY (`SurveyId`)
  REFERENCES `Survey` (`Id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION
);
CREATE INDEX `PrototypeCode_Survey_FK_idx` ON `PrototypeCode` (`SurveyId` ASC);

-- -----------------------------------------------------
-- Table `Role`
-- -----------------------------------------------------
CREATE TABLE `Role` (
  `Id` bigint NOT NULL,
  `Name` varchar(64) NOT NULL,
  PRIMARY KEY (`Id`)
);

-- -----------------------------------------------------
-- Table `ParticipantRole`
-- -----------------------------------------------------
CREATE TABLE `ParticipantRole` (
  `ParticipantId` bigint NOT NULL,
  `RoleId` bigint NOT NULL,
  PRIMARY KEY (`ParticipantId`, `RoleId`),
  CONSTRAINT `ParticipantRole_Role_FK`
  FOREIGN KEY (`RoleId`)
  REFERENCES `Role` (`Id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
  CONSTRAINT `ParticipantRole_Participant_FK`
  FOREIGN KEY (`ParticipantId`)
  REFERENCES `Participant` (`Id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION
);
CREATE INDEX `ParticipantRole_Role_FK_idx` ON `ParticipantRole` (`RoleId` ASC);

-- -----------------------------------------------------
-- Table `ParticipantCredentials`
-- -----------------------------------------------------
CREATE TABLE `ParticipantCredentials` (
  `Id` bigint AUTO_INCREMENT NOT NULL,
  `SurveyId` bigint NOT NULL,
  `Username` varchar(64) NOT NULL,
  `Password` varchar(64) NOT NULL,
  `Used` bit NOT NULL,
  PRIMARY KEY (`Id`),
  CONSTRAINT `SurveyParticipantCredentials_Survey_FK`
  FOREIGN KEY (`SurveyId`)
  REFERENCES `Survey` (`Id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION
);
CREATE INDEX `SurveyParticipantCredentials_Survey_FK_idx` ON `ParticipantCredentials` (`SurveyId` ASC);
CREATE UNIQUE INDEX IX_SurveyParticipantCredentials_Username_SurveyId   ON `ParticipantCredentials` (SurveyId, Username);

-- -----------------------------------------------------
-- Table `PrototypeTest`
-- -----------------------------------------------------
CREATE TABLE `PrototypeTest` (
  `Id` bigint AUTO_INCREMENT NOT NULL,
  `ParticipantSurveyId` bigint NOT NULL,
  `PrototypeCode` varchar(64) NOT NULL,
  `Iteration` int NOT NULL,
  `BeforeWeight` real NOT NULL,
  `AfterWeight` real NULL,
  `BeforePhotoLocalPath` varchar(1024) NOT NULL,
  `AfterPhotoLocalPath` varchar(1024) NULL,
  `BeforePhotoUri` varchar(1024) NOT NULL,
  `AfterPhotoUri` varchar(1024) NULL,
  `Completed` bit NOT NULL default 0,
  PRIMARY KEY (`Id`),
  CONSTRAINT `PrototypeTest_ParticipantSurvey_FK`
  FOREIGN KEY (`ParticipantSurveyId`)
  REFERENCES `ParticipantSurvey` (`Id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION
);
CREATE INDEX `PrototypeTest_ParticipantSurvey_FK_idx` ON `PrototypeTest` (`ParticipantSurveyId` ASC);

-- -----------------------------------------------------
-- Table `PrototypeAnswer`
-- -----------------------------------------------------
CREATE TABLE `PrototypeAnswer` (
  `Id` bigint AUTO_INCREMENT NOT NULL,
  `QuestionId` bigint NOT NULL,
  `AnswerType` varchar(32) NOT NULL,
  `PrototypeTestId` bigint NOT NULL,
  PRIMARY KEY (`Id`),
  CONSTRAINT `PrototypeAnswer_Question_FK`
  FOREIGN KEY (`QuestionId`)
  REFERENCES `PrototypeQuestion` (`Id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
  CONSTRAINT `PrototypeAnswer_PrototypeTest_FK`
  FOREIGN KEY (`PrototypeTestId`)
  REFERENCES `PrototypeTest` (`Id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION
);
CREATE INDEX `PrototypeAnswer_Question_FK_idx` ON `PrototypeAnswer` (`QuestionId` ASC);
CREATE INDEX `PrototypeAnswer_PrototypeTest_FK_idx` ON `PrototypeAnswer` (`PrototypeTestId` ASC);

-- -----------------------------------------------------
-- Table `PrototypeSingleAnswer`
-- -----------------------------------------------------
CREATE TABLE `PrototypeSingleAnswer` (
  `Id` bigint NOT NULL,
  `AnswerOptionId` bigint NOT NULL,
  PRIMARY KEY (`Id`),
  CONSTRAINT `prototopesingleanswerinputoption_PrototypeAnswer_fk1`
  FOREIGN KEY (`Id`)
  REFERENCES `PrototypeAnswer` (`Id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
  CONSTRAINT `PrototypeSingleAnswer_PrototypeAnswerOption`
  FOREIGN KEY (`AnswerOptionId`)
  REFERENCES `PrototypeAnswerOption` (`Id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION
);
CREATE INDEX `PrototypeSingleAnswer_PrototypeAnswerOption_idx` ON `PrototypeSingleAnswer` (`AnswerOptionId` ASC);

-- -----------------------------------------------------
-- Table `PrototypeRangeAnswer`
-- -----------------------------------------------------
CREATE TABLE `PrototypeRangeAnswer` (
  `Id` bigint NOT NULL,
  `Value` int NOT NULL,
  PRIMARY KEY (`Id`),
  CONSTRAINT `PrototypeRangeAnswerOption_PrototypeAnswer_FK1`
  FOREIGN KEY (`Id`)
  REFERENCES `PrototypeAnswer` (`Id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION
);

-- -----------------------------------------------------
-- Table `PrototypeSingleInputAnswer`
-- -----------------------------------------------------
CREATE TABLE `PrototypeSingleInputAnswer` (
  `Id` bigint NOT NULL,
  `Input` varchar(128) NOT NULL,
  PRIMARY KEY (`Id`),
  CONSTRAINT `PrototypeSingleInputAnswer_PrototypeSingleAnswer_FK`
  FOREIGN KEY (`Id`)
  REFERENCES `PrototypeSingleAnswer` (`Id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION
);

-- -----------------------------------------------------
-- Table `ProfileQuestion`
-- -----------------------------------------------------
CREATE TABLE `ProfileQuestion` (
  `Id` bigint NOT NULL,
  `Text` varchar(1024) NOT NULL,
  `Conditional` bit NOT NULL,
  `QuestionType` varchar(32) NOT NULL,
  PRIMARY KEY (`Id`)
);

-- -----------------------------------------------------
-- Table `ProfileAnswerOption`
-- -----------------------------------------------------
CREATE TABLE `ProfileAnswerOption` (
  `Id` bigint NOT NULL,
  `QuestionId` bigint NOT NULL,
  `AnswerType` varchar(32) NOT NULL,
  PRIMARY KEY (`Id`),
  CONSTRAINT `ProfileAnswerOption_ProfileQuestion_fk1`
  FOREIGN KEY (`QuestionId`)
  REFERENCES `ProfileQuestion` (`Id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION
);
CREATE INDEX `ProfileAnswerOption_ProfileQuestion_fk_idx` ON `ProfileAnswerOption` (`QuestionId` ASC);

-- -----------------------------------------------------
-- Table `ProfileRangeAnswerOption`
-- -----------------------------------------------------
CREATE TABLE `ProfileRangeAnswerOption` (
  `Id` bigint NOT NULL,
  `FromValueLabel` varchar(64) NOT NULL,
  `ToValueLabel` varchar(64) NOT NULL,
  `FromValue` int NOT NULL,
  `ToValue` int NOT NULL,
  `Increment` int NULL,
  PRIMARY KEY (`Id`),
  CONSTRAINT `ProfileRangeAnswerOption_ProfileAnswerOption_FK2`
  FOREIGN KEY (`Id`)
  REFERENCES `ProfileAnswerOption` (`Id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION
);
-- -----------------------------------------------------
-- Table `ProfileSingleAnswerOption`
-- -----------------------------------------------------
CREATE TABLE `ProfileSingleAnswerOption` (
  `Id` bigint NOT NULL,
  `Value` int NOT NULL,
  `Label` varchar(64) NOT NULL,
  `ConditionalQuestionId` bigint NULL,
  PRIMARY KEY (`Id`),
  CONSTRAINT `ProfileSingleAnswerOption_ProfileAnswerOption_FK2`
  FOREIGN KEY (`Id`)
  REFERENCES `ProfileAnswerOption` (`Id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION
);
-- -----------------------------------------------------
-- Table `PostTestingAnswer`
-- -----------------------------------------------------
CREATE TABLE `PostTestingAnswer` (
  `Id` bigint AUTO_INCREMENT NOT NULL,
  `QuestionId` bigint NOT NULL,
  `AnswerType` varchar(32) NOT NULL,
  `ParticipantSurveyId` bigint NOT NULL,
  PRIMARY KEY (`Id`),
  CONSTRAINT `PrototypeAnswer_PostTestingQuestion_FK0`
  FOREIGN KEY (`QuestionId`)
  REFERENCES `PostTestingQuestion` (`Id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
  CONSTRAINT `PostTestingAnswer_ParticipantSurvey_FK`
  FOREIGN KEY (`ParticipantSurveyId`)
  REFERENCES `ParticipantSurvey` (`Id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION
);
CREATE INDEX `PrototypeAnswer_PostTestingQuestion_FK0_idx` ON `PostTestingAnswer` (`QuestionId` ASC);
CREATE INDEX `PostTestingAnswer_ParticipantSurvey_FK_idx` ON `PostTestingAnswer` (`ParticipantSurveyId` ASC);

-- -----------------------------------------------------
-- Table `PostTestingSingleAnswer`
-- -----------------------------------------------------
CREATE TABLE `PostTestingSingleAnswer` (
  `Id` bigint NOT NULL,
  `AnswerOptionId` bigint NOT NULL,
  PRIMARY KEY (`Id`),
  CONSTRAINT `PostTestingSingleAnswer_PostTestingAnswer_FK10`
  FOREIGN KEY (`Id`)
  REFERENCES `PostTestingAnswer` (`Id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
  CONSTRAINT `PrototypeSingleAnswer_PostTestingAnswerOption`
  FOREIGN KEY (`AnswerOptionId`)
  REFERENCES `PostTestingAnswerOption` (`Id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION
);

-- -----------------------------------------------------
-- Table `PostTestingSingleInputAnswer`
-- -----------------------------------------------------
CREATE TABLE `PostTestingSingleInputAnswer` (
  `Id` bigint NOT NULL,
  `Input` varchar(128) NOT NULL,
  PRIMARY KEY (`Id`),
  CONSTRAINT `PrototypeSingleInputAnswer_PostTestingSingleAnswer_FK0`
  FOREIGN KEY (`Id`)
  REFERENCES `PostTestingSingleAnswer` (`Id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION
);

-- -----------------------------------------------------
-- Table `PostTestingRangeAnswer`
-- -----------------------------------------------------
CREATE TABLE `PostTestingRangeAnswer` (
  `Id` bigint NOT NULL,
  `Value` int NOT NULL,
  PRIMARY KEY (`Id`),
  CONSTRAINT `PrototypeRangeAnswerOption_PostTestingAnswer_FK10`
  FOREIGN KEY (`Id`)
  REFERENCES `PostTestingAnswer` (`Id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION
);

-- -----------------------------------------------------
-- Table `ProfileAnswer`
-- -----------------------------------------------------
CREATE TABLE `ProfileAnswer` (
  `Id` bigint AUTO_INCREMENT NOT NULL,
  `QuestionId` bigint NOT NULL,
  `AnswerType` varchar(32) NOT NULL,
  `ParticipantId` bigint NOT NULL,
  PRIMARY KEY (`Id`),
  CONSTRAINT `ProfileAnswer_ProfileQuestion_FK00`
  FOREIGN KEY (`QuestionId`)
  REFERENCES `ProfileQuestion` (`Id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
  CONSTRAINT `ProfileAnswer_Participant`
  FOREIGN KEY (`ParticipantId`)
  REFERENCES `Participant` (`Id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION
);
CREATE INDEX `ProfileAnswer_ProfileQuestion_FK0_idx` ON `ProfileAnswer` (`QuestionId` ASC);
CREATE INDEX `ProfileAnswer_Participant_idx` ON `ProfileAnswer` (`ParticipantId` ASC);

-- -----------------------------------------------------
-- Table `ProfileSingleAnswer`
-- -----------------------------------------------------
CREATE TABLE `ProfileSingleAnswer` (
  `Id` bigint NOT NULL,
  `AnswerOptionId` bigint NOT NULL,
  PRIMARY KEY (`Id`),
  CONSTRAINT `ProfileSingleAnswer_ProfileAnswer_FK100`
  FOREIGN KEY (`Id`)
  REFERENCES `ProfileAnswer` (`Id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
  CONSTRAINT `PrototypeSingleAnswer_ProfileAnswerOption_FK101`
  FOREIGN KEY (`AnswerOptionId`)
  REFERENCES `ProfileAnswerOption` (`Id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION
);

-- -----------------------------------------------------
-- Table `ProfileSingleInputAnswer`
-- -----------------------------------------------------
CREATE TABLE `ProfileSingleInputAnswer` (
  `Id` bigint NOT NULL,
  `Input` varchar(128) NOT NULL,
  PRIMARY KEY (`Id`),
  CONSTRAINT `PrototypeSingleInputAnswer_ProfileSingleAnswer_FK00`
  FOREIGN KEY (`Id`)
  REFERENCES `ProfileSingleAnswer` (`Id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION
);

-- -----------------------------------------------------
-- Table `ProfileRangeAnswer`
-- -----------------------------------------------------
CREATE TABLE `ProfileRangeAnswer` (
  `Id` bigint NOT NULL,
  `Value` int NOT NULL,
  PRIMARY KEY (`Id`),
  CONSTRAINT `PrototypeRangeAnswerOption_ProfileAnswer_FK100`
  FOREIGN KEY (`Id`)
  REFERENCES `ProfileAnswer` (`Id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION
);

-- -----------------------------------------------------
-- Table `Users`
-- -----------------------------------------------------
CREATE TABLE `Users` (
  `Id` bigint NOT NULL,
  `Username` varchar(64) NOT NULL,
  `UserpicPath` varchar(1024) NOT NULL,
  `Password` varchar(256) NOT NULL,
  `Name` varchar(500) NULL,
  PRIMARY KEY (`Id`)
);

-- -----------------------------------------------------
-- Table `UserRole`
-- -----------------------------------------------------
CREATE TABLE `UserRole` (
  `UserId` bigint NOT NULL,
  `RoleId` bigint NOT NULL,
  PRIMARY KEY (`UserId`, `RoleId`),
  CONSTRAINT `UserRole_Role_FK0`
  FOREIGN KEY (`RoleId`)
  REFERENCES `Role` (`Id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
  CONSTRAINT `UserRole_User_FK0`
  FOREIGN KEY (`UserId`)
  REFERENCES `Users` (`Id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION
);
CREATE INDEX `ParticipantRole_Role_FK_idx` ON `UserRole` (`RoleId` ASC);

-- -----------------------------------------------------
-- Table `QuickAccessSurvey`
-- -----------------------------------------------------
CREATE TABLE `QuickAccessSurvey` (
  `Id` bigint AUTO_INCREMENT NOT NULL,
  `UserId` bigint NOT NULL,
  `SurveyId` bigint NOT NULL,
  `Position` int NOT NULL,
  PRIMARY KEY (`Id`)
);

-- -----------------------------------------------------
-- Table `PrototypeQuestionCondition`
-- -----------------------------------------------------
CREATE TABLE `PrototypeQuestionCondition` (
  `Id` bigint NOT NULL,
  `QuestionId` bigint NOT NULL,
  `ConditionalOnQuestionId` bigint NOT NULL,
  `ConditionalOnAnswerId` bigint NOT NULL,
  PRIMARY KEY (`Id`)
);

-- -----------------------------------------------------
-- Table `PostTestingQuestionCondition`
-- -----------------------------------------------------
CREATE TABLE `PostTestingQuestionCondition` (
  `Id` bigint NOT NULL,
  `QuestionId` bigint NOT NULL,
  `ConditionalOnQuestionId` bigint NOT NULL,
  `ConditionalOnAnswerId` bigint NOT NULL,
  PRIMARY KEY (`Id`)
);

-- -----------------------------------------------------
-- Table `ProfileQuestionCondition`
-- -----------------------------------------------------
CREATE TABLE `ProfileQuestionCondition` (
  `Id` bigint NOT NULL,
  `QuestionId` bigint NOT NULL,
  `ConditionalOnQuestionId` bigint NOT NULL,
  `ConditionalOnAnswerId` bigint NOT NULL,
  PRIMARY KEY (`Id`),
  CONSTRAINT `ProfileQuestionCondition_ProfileQuestionId`
  FOREIGN KEY (`QuestionId`)
  REFERENCES `ProfileQuestion` (`Id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION
);
CREATE INDEX `ProfileQuestionCondition_ProfileQuestionId_idx` ON `ProfileQuestionCondition` (`QuestionId` ASC);