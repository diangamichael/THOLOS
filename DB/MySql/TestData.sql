SET autocommit = 0;
set innodb_lock_wait_timeout=500;
START TRANSACTION;
DELETE FROM `SurveyProrotypeQuestion`;
DELETE FROM `SurveyPostTestingQuestion`;
DELETE FROM `participantCredentials`;
DELETE FROM `ProfileRangeAnswer`;
DELETE FROM `ProfileSingleInputAnswer`;
DELETE FROM `ProfileSingleAnswer`;
DELETE FROM `ProfileAnswer`;
DELETE FROM `ProfileRangeAnswerOption`;
DELETE FROM `ProfileSingleAnswerOption`;
DELETE FROM `ProfileAnswerOption`;
DELETE FROM `ProfileQuestionCondition`;
DELETE FROM `ProfileQuestion`;
DELETE FROM `PrototypeRangeAnswer`;
DELETE FROM `PrototypeSingleInputAnswer`;
DELETE FROM `PrototypeSingleAnswer`;
DELETE FROM `PrototypeAnswer`;
DELETE FROM `PrototypeRangeAnswerOption`;
DELETE FROM `PrototypeSingleAnswerOption`;
DELETE FROM `PrototypeAnswerOption`;
DELETE FROM `PrototypeQuestionCondition`;
DELETE FROM `PrototypeQuestion`;
DELETE FROM `PostTestingRangeAnswer`;
DELETE FROM `PostTestingSingleInputAnswer`;
DELETE FROM `PostTestingSingleAnswer`;
DELETE FROM `PostTestingAnswer`;
DELETE FROM `PostTestingRangeAnswerOption`;
DELETE FROM `PostTestingSingleAnswerOption`;
DELETE FROM `PostTestingAnswerOption`;
DELETE FROM `PostTestingQuestionCondition`;
DELETE FROM `PostTestingQuestion`;
DELETE FROM `ParticipantRole`;
DELETE FROM `Participant`;
DELETE FROM `UserRole`;
DELETE FROM `Role`;
DELETE FROM `Users`;
DELETE FROM `QuickAccessSurvey`;
DELETE FROM `PrototypeCode`;
DELETE FROM `PrototypeTest`;
DELETE FROM `ParticipantSurvey`;
DELETE FROM `Survey`;

INSERT INTO  Role (Id, Name) VALUES(1,'Manager');
INSERT INTO  Role (Id, Name) VALUES(2,'Participant');
			 
INSERT INTO  Users (Id, Name, Username,  Password , UserpicPath)VALUES(1,'Topcoder Manager','topcoder','5BAA61E4C9B93F3F0682250B6CF8331B7EE68FD8','i/thumn-user.jpg');
INSERT INTO  Users (Id, Name, Username,  Password , UserpicPath)VALUES(2,'Second user','secondUser','5BAA61E4C9B93F3F0682250B6CF8331B7EE68FD8','i/thumn-user.jpg');
			 
INSERT INTO  UserRole (UserId, RoleId)VALUES(1,1);
INSERT INTO  UserRole (UserId, RoleId)VALUES(2,1);
			 
INSERT INTO  Participant (Id,Username, SurveyId, BirthDate, Weight, HeightFeet, HeightInches)VALUES(1, 'participant1',1,'1992-07-24', 67, 5, 11);
INSERT INTO  Participant (Id,Username, SurveyId, BirthDate, Weight, HeightFeet, HeightInches)VALUES(2, 'participant2',1,'1992-07-24', 67, 5, 11);
			 
INSERT INTO  ParticipantRole (ParticipantId, RoleId) VALUES(1,2);
INSERT INTO  ParticipantRole (ParticipantId, RoleId) VALUES(2,2);
			 
			 
INSERT INTO  Survey (Id,Name,CellName,ParticipantsNumber,Status,CompletedSurveysNumber,Draft,CreatedBy)VALUES(1,'Survey1','CellName1',3,'Published',0,0,1);
INSERT INTO  Survey (Id,Name,CellName,ParticipantsNumber,Status,CompletedSurveysNumber,Draft,CreatedBy)VALUES(2,'Survey2','CellName2',3,'Draft',0,1,1);
INSERT INTO  Survey (Id,Name,CellName,ParticipantsNumber,Status,CompletedSurveysNumber,Draft,CreatedBy)VALUES(3,'Survey3','CellName3',3,'Draft',0,1,1);
			 
			 
INSERT INTO  participantCredentials (Id, surveyid,Username,  password ,used)VALUES(1,1,'participant1','3EJmN7DbtlAJXiDNwLPKZQ==',1);
INSERT INTO  participantCredentials (Id, surveyid,Username,  password ,used)VALUES(2,1,'participant2','qCoFhOshU+3B4TSSD+PzBQ==',1);
INSERT INTO  participantCredentials (Id, surveyid,Username,  password ,used)VALUES(3,1,'participant3','dlya20FTyoGxt7NGHC2XzA==',0);
INSERT INTO  participantCredentials (Id, surveyid,Username,  password ,used)VALUES(4,1,'participant4','Totz77kr0oVRA78hPcwl7Q==',0);
			 
-- Profile q s  and answer options
INSERT INTO  ProfileQuestion (Id,  Text , Conditional, QuestionType) VALUES(1, 'Question1',0,'SingleAnswerQuestion');
INSERT INTO  ProfileQuestion (Id,  Text , Conditional, QuestionType) VALUES(2, 'Question2',1,'SingleAnswerQuestion');
INSERT INTO  ProfileQuestion (Id,  Text , Conditional, QuestionType) VALUES(3, 'Question3',0,'RangeQuestion');
INSERT INTO  ProfileQuestion (Id,  Text , Conditional, QuestionType) VALUES(4, 'Question4',0,'SingleAnswerQuestion');


INSERT INTO ProfileQuestionCondition(Id, QuestionId, ConditionalOnQuestionId, ConditionalOnAnswerId)VALUES(1,2,4,1);

INSERT INTO ProfileAnswerOption(Id, QuestionId, AnswerType)VALUES(1,1,'SingleAnswer');
INSERT INTO ProfileAnswerOption(Id, QuestionId, AnswerType)VALUES(2,1,'SingleAnswer');
INSERT INTO ProfileAnswerOption(Id, QuestionId, AnswerType)VALUES(3,1,'SingleAnswer');
INSERT INTO ProfileAnswerOption(Id, QuestionId, AnswerType)VALUES(4,1,'SingleAnswer');

INSERT INTO ProfileAnswerOption(Id, QuestionId, AnswerType)VALUES(5,2,'SingleAnswer');
INSERT INTO ProfileAnswerOption(Id, QuestionId, AnswerType)VALUES(6,2,'SingleAnswer');

INSERT INTO ProfileAnswerOption(Id, QuestionId, AnswerType)VALUES(7,3,'RangeAnswer');
INSERT INTO ProfileAnswerOption(Id, QuestionId, AnswerType)VALUES(8,3,'RangeAnswer');
INSERT INTO ProfileAnswerOption(Id, QuestionId, AnswerType)VALUES(9,3,'RangeAnswer');

INSERT INTO ProfileAnswerOption(Id, QuestionId, AnswerType)VALUES(10,4,'SingleAnswerInput');

INSERT INTO ProfileSingleAnswerOption(Id, Value, Label, ConditionalQuestionId)VALUES(1, 1, '4',null);
INSERT INTO ProfileSingleAnswerOption(Id, Value, Label, ConditionalQuestionId)VALUES(2, 2, '6',null);
INSERT INTO ProfileSingleAnswerOption(Id, Value, Label, ConditionalQuestionId)VALUES(3, 3, '7',null);
INSERT INTO ProfileSingleAnswerOption(Id, Value, Label, ConditionalQuestionId)VALUES(4, 4, '8',null);
INSERT INTO ProfileSingleAnswerOption(Id, Value, Label, ConditionalQuestionId)VALUES(5, 1, 'Yes',null);
INSERT INTO ProfileSingleAnswerOption(Id, Value, Label, ConditionalQuestionId)VALUES(6, 2, 'No',null);

INSERT INTO ProfileRangeAnswerOption(Id, FromValueLabel, ToValueLabel, FromValue,ToValue,Increment)VALUES(7,'Sleep At', 'Wake at',10,6,1);
INSERT INTO ProfileRangeAnswerOption(Id, FromValueLabel, ToValueLabel, FromValue,ToValue,Increment)VALUES(8,'Sleep At', 'Wake at',10,6,2);
INSERT INTO ProfileRangeAnswerOption(Id, FromValueLabel, ToValueLabel, FromValue,ToValue,Increment)VALUES(9,'Sleep At', 'Wake at',10,6,3);

INSERT INTO ProfileSingleAnswerOption(Id, Value, Label, ConditionalQuestionId)VALUES(10, 1, 'Explain Why?',null);

-- prototype questions  and answer options
INSERT INTO PrototypeQuestion (Id,  Text , Conditional, QuestionType) VALUES(1, 'Question1',0,'SingleAnswerQuestion');
INSERT INTO PrototypeQuestion (Id,  Text , Conditional, QuestionType) VALUES(2, 'Question2',1,'SingleAnswerQuestion');
INSERT INTO PrototypeQuestion (Id,  Text , Conditional, QuestionType) VALUES(3, 'Question3',0,'RangeQuestion');
INSERT INTO PrototypeQuestion (Id,  Text , Conditional, QuestionType) VALUES(4, 'Question4',0,'SingleAnswerQuestion');
																					   
INSERT INTO PrototypeQuestionCondition(Id, QuestionId, ConditionalOnQuestionId, ConditionalOnAnswerId)VALUES(1,2,4,1);

INSERT INTO PrototypeAnswerOption(Id, QuestionId, AnswerType)VALUES(1,1,'SingleAnswer');
INSERT INTO PrototypeAnswerOption(Id, QuestionId, AnswerType)VALUES(2,1,'SingleAnswer');
INSERT INTO PrototypeAnswerOption(Id, QuestionId, AnswerType)VALUES(3,1,'SingleAnswer');
INSERT INTO PrototypeAnswerOption(Id, QuestionId, AnswerType)VALUES(4,1,'SingleAnswer');

INSERT INTO PrototypeAnswerOption(Id, QuestionId, AnswerType)VALUES(5,2,'SingleAnswer');
INSERT INTO PrototypeAnswerOption(Id, QuestionId, AnswerType)VALUES(6,2,'SingleAnswer');

INSERT INTO PrototypeAnswerOption(Id, QuestionId, AnswerType)VALUES(7,3,'RangeAnswer');
INSERT INTO PrototypeAnswerOption(Id, QuestionId, AnswerType)VALUES(8,3,'RangeAnswer');
INSERT INTO PrototypeAnswerOption(Id, QuestionId, AnswerType)VALUES(9,3,'RangeAnswer');

INSERT INTO PrototypeAnswerOption(Id, QuestionId, AnswerType)VALUES(10,4,'SingleAnswerInput');

INSERT INTO PrototypeSingleAnswerOption(Id, Value, Label, ConditionalQuestionId)VALUES(1, 1, '4',null);
INSERT INTO PrototypeSingleAnswerOption(Id, Value, Label, ConditionalQuestionId)VALUES(2, 2, '6',null);
INSERT INTO PrototypeSingleAnswerOption(Id, Value, Label, ConditionalQuestionId)VALUES(3, 3, '7',null);
INSERT INTO PrototypeSingleAnswerOption(Id, Value, Label, ConditionalQuestionId)VALUES(4, 4, '8',null);
INSERT INTO PrototypeSingleAnswerOption(Id, Value, Label, ConditionalQuestionId)VALUES(5, 1, 'Yes',null);
INSERT INTO PrototypeSingleAnswerOption(Id, Value, Label, ConditionalQuestionId)VALUES(6, 2, 'No',null);

INSERT INTO PrototypeRangeAnswerOption(Id, FromValueLabel, ToValueLabel, FromValue,ToValue,Increment)VALUES(7,'Sleep At', 'Wake at',10,6,1);
INSERT INTO PrototypeRangeAnswerOption(Id, FromValueLabel, ToValueLabel, FromValue,ToValue,Increment)VALUES(8,'Sleep At', 'Wake at',10,6,2);
INSERT INTO PrototypeRangeAnswerOption(Id, FromValueLabel, ToValueLabel, FromValue,ToValue,Increment)VALUES(9,'Sleep At', 'Wake at',10,6,3);

INSERT INTO PrototypeSingleAnswerOption(Id, Value, Label, ConditionalQuestionId)VALUES(10, 1, 'Explain Why?',null);

-- PostTesting questions and answer options
INSERT INTO PostTestingQuestion (Id,  Text , Conditional, QuestionType) VALUES(1, 'Question1',0,'SingleAnswerQuestion');
INSERT INTO PostTestingQuestion (Id,  Text , Conditional, QuestionType) VALUES(2, 'Question2',1,'SingleAnswerQuestion');
INSERT INTO PostTestingQuestion (Id,  Text , Conditional, QuestionType) VALUES(3, 'Question3',0,'RangeQuestion');
INSERT INTO PostTestingQuestion (Id,  Text , Conditional, QuestionType) VALUES(4, 'Question4',0,'SingleAnswerQuestion');

INSERT INTO PostTestingQuestionCondition(Id, QuestionId, ConditionalOnQuestionId, ConditionalOnAnswerId)VALUES(1,2,4,1);

INSERT INTO PostTestingAnswerOption(Id, QuestionId, AnswerType)VALUES(1,1,'SingleAnswer');
INSERT INTO PostTestingAnswerOption(Id, QuestionId, AnswerType)VALUES(2,1,'SingleAnswer');
INSERT INTO PostTestingAnswerOption(Id, QuestionId, AnswerType)VALUES(3,1,'SingleAnswer');
INSERT INTO PostTestingAnswerOption(Id, QuestionId, AnswerType)VALUES(4,1,'SingleAnswer');

INSERT INTO PostTestingAnswerOption(Id, QuestionId, AnswerType)VALUES(5,2,'SingleAnswer');
INSERT INTO PostTestingAnswerOption(Id, QuestionId, AnswerType)VALUES(6,2,'SingleAnswer');

INSERT INTO PostTestingAnswerOption(Id, QuestionId, AnswerType)VALUES(7,3,'RangeAnswer');
INSERT INTO PostTestingAnswerOption(Id, QuestionId, AnswerType)VALUES(8,3,'RangeAnswer');
INSERT INTO PostTestingAnswerOption(Id, QuestionId, AnswerType)VALUES(9,3,'RangeAnswer');
			
INSERT INTO PostTestingAnswerOption(Id, QuestionId, AnswerType)VALUES(10,4,'SingleAnswerInput');
			
INSERT INTO PostTestingSingleAnswerOption(Id, Value, Label, ConditionalQuestionId)VALUES(1, 1, '4',null);
INSERT INTO PostTestingSingleAnswerOption(Id, Value, Label, ConditionalQuestionId)VALUES(2, 2, '6',null);
INSERT INTO PostTestingSingleAnswerOption(Id, Value, Label, ConditionalQuestionId)VALUES(3, 3, '7',null);
INSERT INTO PostTestingSingleAnswerOption(Id, Value, Label, ConditionalQuestionId)VALUES(4, 4, '8',null);
INSERT INTO PostTestingSingleAnswerOption(Id, Value, Label, ConditionalQuestionId)VALUES(5, 1, 'Yes',null);
INSERT INTO PostTestingSingleAnswerOption(Id, Value, Label, ConditionalQuestionId)VALUES(6, 2, 'No',null);
			
INSERT INTO PostTestingRangeAnswerOption(Id, FromValueLabel, ToValueLabel, FromValue,ToValue,Increment)VALUES(7,'Sleep At', 'Wake at',10,6,1);
INSERT INTO PostTestingRangeAnswerOption(Id, FromValueLabel, ToValueLabel, FromValue,ToValue,Increment)VALUES(8,'Sleep At', 'Wake at',10,6,2);
INSERT INTO PostTestingRangeAnswerOption(Id, FromValueLabel, ToValueLabel, FromValue,ToValue,Increment)VALUES(9,'Sleep At', 'Wake at',10,6,3);
			
INSERT INTO PostTestingSingleAnswerOption(Id, Value, Label, ConditionalQuestionId)VALUES(10, 1, 'Explain Why?',null);

-- Profile ANswers
INSERT INTO  ProfileAnswer (Id,QuestionId,AnswerType,ParticipantId) VALUES (1,1,'SingleAnswer'	,1);
INSERT INTO  ProfileAnswer (Id,QuestionId,AnswerType,ParticipantId) VALUES (2,3,'RangeAnswer'		,1);
INSERT INTO  ProfileAnswer (Id,QuestionId,AnswerType,ParticipantId) VALUES (3,2,'SingleAnswerInput',1);
			 
INSERT INTO  ProfileSingleAnswer (Id,AnswerOptionId) VALUES (1, 1);
INSERT INTO  ProfileSingleAnswer (Id,AnswerOptionId) VALUES (3, 2);
			 
INSERT INTO  ProfileSingleInputAnswer (Id,Input) VALUES (3,11);
INSERT INTO  ProfileRangeAnswer (Id,Value) VALUES (2,4);
			 
INSERT INTO	 QuickAccessSurvey (Id, UserId, SurveyId, Position) VALUES(1,1,1,1);
INSERT INTO	 QuickAccessSurvey (Id, UserId, SurveyId, Position) VALUES(2,1,2,2);
			 
INSERT INTO  PrototypeCode (Id, Code, PrototypesPerCode,surveyId)VALUES(1,'Code1',3,1);
INSERT INTO  PrototypeCode (Id, Code, PrototypesPerCode,surveyId)VALUES(2,'Code2',3,1);
INSERT INTO  PrototypeCode (Id, Code, PrototypesPerCode,surveyId)VALUES(3,'Code3',3,1);
INSERT INTO  PrototypeCode (Id, Code, PrototypesPerCode,surveyId)VALUES(4,'Code1',3,2);
			 
INSERT INTO  SurveyProrotypeQuestion (Id, surveyId, QuestionId,QuestionNumber)VALUES(1,1,1,1);
INSERT INTO  SurveyProrotypeQuestion (Id, surveyId, QuestionId,QuestionNumber)VALUES(2,1,2,2);
INSERT INTO  SurveyProrotypeQuestion (Id, surveyId, QuestionId,QuestionNumber)VALUES(3,1,3,3);
INSERT INTO  SurveyProrotypeQuestion (Id, surveyId, QuestionId,QuestionNumber)VALUES(4,2,1,1);
			 
INSERT INTO  SurveyPostTestingQuestion (Id, surveyId, QuestionId,QuestionNumber)VALUES(1,1,1,1);
INSERT INTO  SurveyPostTestingQuestion (Id, surveyId, QuestionId,QuestionNumber)VALUES(2,1,2,2);
INSERT INTO  SurveyPostTestingQuestion (Id, surveyId, QuestionId,QuestionNumber)VALUES(3,1,3,3);
INSERT INTO  SurveyPostTestingQuestion (Id, surveyId, QuestionId,QuestionNumber)VALUES(4,2,1,1);


INSERT INTO ParticipantSurvey(Id, ParticipantId, SurveyId, CurrentPrototypeTestId)VALUES(1,1,1,1);
INSERT INTO ParticipantSurvey(Id, ParticipantId, SurveyId, CurrentPrototypeTestId)VALUES(2,2,1,1);

INSERT INTO PrototypeTest(Id,ParticipantSurveyId,PrototypeCode,Iteration,beforeWeight,BeforePhotoLocalPath,BeforePhotoUri)
VALUES(1,1,"code1",1,12.4,"E:/tc/Csharp/tholos/sub - Copy/submission/Src/Tholos.Host/uploadFiles/download.jpg","http://localhost/tholosapi/uploadFiles/download.jpg");

INSERT INTO PrototypeAnswer(Id,QuestionId,AnswerType,PrototypeTestId) VALUES (1,1,'SingleAnswer'	,1);
INSERT INTO PrototypeAnswer(Id,QuestionId,AnswerType,PrototypeTestId) VALUES (2,3,'RangeAnswer'		,1);
INSERT INTO PrototypeAnswer(Id,QuestionId,AnswerType,PrototypeTestId) VALUES (3,2,'SingleAnswerInput',1);


INSERT INTO PrototypeSingleAnswer (Id,AnswerOptionId) VALUES (1, 1);
INSERT INTO PrototypeSingleAnswer (Id,AnswerOptionId) VALUES (3, 2);
								  
INSERT INTO PrototypeSingleInputAnswer(Id,Input) VALUES (3,11);
INSERT INTO PrototypeRangeAnswer(Id,Value) VALUES (2,4);

COMMIT;
